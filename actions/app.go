package actions

import (
	"fmt"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo-pop/pop/popmw"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/mw-csrf"
	"github.com/gobuffalo/mw-forcessl"
	"github.com/gobuffalo/mw-i18n"
	"github.com/gobuffalo/mw-paramlogger"
	"github.com/gobuffalo/packr"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/markbates/goth/gothic"
	"github.com/unrolled/secure"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App
var T *i18n.Translator

func init() {
	App().Host = envy.Get("APP_URL", App().Host)
	fmt.Printf("KP_ADMIN initialized, App.Host= %v \n", App().Host)

}

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:         ENV,
			SessionName: "_kp_admin_session",
		})
		// Automatically redirect to SSL
		app.Use(forceSSL())

		// Setup and use translations:
		app.Use(translations())

		if ENV == "development" {
			app.Use(paramlogger.ParameterLogger)
		}
		fmt.Println("ENV ---", ENV)

		if ENV == "production" {
			//Custom страница за 404 и 500
			app.ErrorHandlers[404] = ErrorPage
			app.ErrorHandlers[500] = ErrorPage
		}

		// Protect against CSRF attacks. https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
		// Remove to disable this.
		app.Use(csrf.New)

		// Wraps each request in a transaction.
		//  c.Value("tx").(*pop.PopTransaction)
		// Remove to disable this.
		app.Use(popmw.Transaction(models.DB))

		app.Use(SetCurrentUser) // Mifddleware, който проверява дали в сесията има user_id, и ако има задава user в контекста

		app.GET("/lang/toggle", ToggleLocale)

		app.GET("/home", HomeHandler)

		app.Middleware.Skip(Authorize, ToggleLocale, NotAuth(WelcomeHandler))
		app.GET("/", NotAuth(WelcomeHandler))

		//---- Аuthentication (login , signUp)--- //
		auth := app.Group("/auth")
		bah := buffalo.WrapHandlerFunc(gothic.BeginAuthHandler) //За логин с github и linkedin
		auth.Middleware.Skip(Authorize, bah, AuthCallback, UsersLoginGet)
		auth.GET("/login", NotAuth(UsersLoginGet))
		auth.PUT("/new", NotAuth(UsersCreate)).Name("signUp")
		auth.POST("/login", NotAuth(AuthAttempt)).Name("login")
		auth.GET("/{provider}", bah)
		auth.GET("/{provider}/callback", AuthCallback)
		auth.DELETE("/logout", AuthDestroy).Name("logout")

		//За възстановяване на паролата
		rp := auth.Group("/reset/password")
		rp.Use(NotAuth)
		rp.GET("/", ResetPassGet)
		rp.POST("/send/mail", ResetPassSendMail)
		rp.POST("/check/token", ResetPassCheckToken)
		rp.POST("/change/pass", ResetPass)

		app.Middleware.Skip(Authorize, LinkPage(GetLinkPage))
		app.GET("/site/join/{string}", LinkPage(GetLinkPage)).Name("getLinkPage")

		app.Use(Authorize) //  Mifddleware - трябва да си логнат

		userWithoutMiddleware := app.Group("/user")
		userWithoutMiddleware.GET("/{user_id}", GetProfile).Name("getProfilePath")
		user := app.Group("/user")
		user.Use(IsMyProfile)
		user.POST("/editMeta/{user_id}", EditMetaProfile).Name("editMetaProfilePath")
		user.POST("/editPass/{user_id}", EditPassword).Name("editPassPath")
		user.POST("/file/{user_id}", ProfileFileUpload).Name("profileFilePath")
		user.PUT("/skills/{user_id}", EditSkills).Name("editSkillsPath")

		invites := app.Group("/invites")
		invites.GET("/", GetInvitesPage).Name("getInvitesPath")
		invites.DELETE("/delete/{id}", DeleteInvite).Name("deleteInvitePath")

		app.PUT("/site/join/{site_id}", JoinSite).Name("joinSitePath")
		site := app.Group("/site")
		site.GET("/new", GetSiteForm).Name("getSiteFormPath")
		site.PUT("/new", CreateSite).Name("createSitePath")
		site.GET("/all/", GetAllSites).Name("getAllSitesPath")
		site.Use(IsInSite)
		site.PUT("/{site_id}", EditSite).Name("editSitePath")
		site.GET("/{site_id}", GetSite).Name("getSitePath")
		site.GET("/get/link/{site_id}", GenerateLink).Name("generateLinkPath")
		site.PUT("/join/send/invite/{site_id}", SendInvite).Name("sendInvitePath")
		site.PUT("/remove/user/{site_id}", RemoveUser).Name("removeUserPath")
		site.PUT("/change/rights/{site_id}", ChangeRights).Name("changeRightsPath")
		site.DELETE("/delete/{site_id}", DeleteSite).Name("deleteSitePath")
		site.POST("/photo/{site_id}", UploadPhoto).Name("sitePhotoPath")

		db := site.Group("/{site_id}/db")
		db.Use(IsInSite)
		db.POST("/get/connections/", GetConns)
		db.POST("/check/connection/", DBConnCheckConn)
		//Трябва да си admin или owner на сайта
		dbAdmin := db.Group("/")
		dbAdmin.Use(IsAdmin)
		dbAdmin.POST("/get/tables/", GetTables)
		dbAdmin.PUT("/create/", CreateConn)
		dbAdmin.PUT("/update/", UpdateConn)
		dbAdmin.DELETE("/delete/{conn_id}", DelConn)

		tbl := db.Group("/table/")
		tbl.POST("/get/columns", ColumnGetColumns)
		tbl.POST("/get/saved/columns", ColumnGetSavedColumns)
		tbl.POST("/rows/count", ColumnCountRows)
		tbl.ANY("/row/{table_id}", ColumnGetRowWS) //Работи с web socket-и
		tbl.POST("/check/conn/{table_id}", DBTableCheckConn)
		//Трябва да си admin или owner на сайта
		tblAdmin := tbl.Group("/")
		tblAdmin.Use(IsAdmin)
		tblAdmin.POST("/{table_id}/rename", TablesRenameTbl)
		tblAdmin.POST("/{table_id}/set/pk/column", TablesSetTblPKColumn)
		tblAdmin.PUT("/create/columns", ColumnCreateColumns)
		tblAdmin.PUT("/update/columns", ColumnUpdateColumns)
		tblAdmin.PUT("/update/column", ColumnUpdate)
		tblAdmin.PUT("/update/cell/data", CellsRowEditData)
		tblAdmin.POST("/get/cell/edit/log", CellGetLogs)
		tblAdmin.GET("/{table_id}/logs", TablesGetTblEditLogs)

		col := tblAdmin.Group("/column/{column_id}")
		col.PUT("/add/relation", ColumnAddRelation)
		col.DELETE("/remove/relation", ColumnDeleteRelation)
		col.PUT("/add/mutation", ColumnAddMutation)
		col.DELETE("/remove/mutation", ColumnDeleteMutation)
		col.PUT("/add/validation", ColumnAddValidation)
		col.DELETE("/remove/validation/{validation_id}", ColumnDeleteValidation)

		chart := site.Group("{site_id}/chart/")
		chart.Middleware.Skip(IsAdmin, GetDataWS, ChartsGetCharts, ChartsGetChart)
		chart.ANY("/data/{chart_id}", GetDataWS)
		chart.GET("/get/all/", ChartsGetCharts)
		chart.GET("{chart_id}", ChartsGetChart)
		chart.Use(IsAdmin)
		chart.PUT("/create/", ChartsCreateChart)
		chart.PUT("/update/{chart_id}", ChartUpdateChart)
		chart.DELETE("/delete/{chart_id}", ChartsDeleteChart)

		report := app.Group("/report")
		report.GET("/", getReportForm).Name("getReportFormPath")
		report.POST("/", postReportForm).Name("postReportFormPath")

		app.GET("/manual", getManual).Name("getManualPath")

		app.ServeFiles("/", assetsBox) // serve files from the public directory
	}

	return app
}

// translations will load locale files, set up the translator `actions.T`,
// and will return a middleware to use to load the correct locale for each
// request.
// for more information: https://gobuffalo.io/en/docs/localization
func translations() buffalo.MiddlewareFunc {
	var err error

	defLang := "en"

	if ENV == "test" {
		models.Lang = "bg"
		defLang = "bg"
	}

	if T, err = i18n.New(packr.NewBox("../locales"), defLang); err != nil {
		app.Stop(err)
	}
	return T.Middleware()
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return forcessl.Middleware(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
	})
}

package actions

func (as *ActionSuite) Test_ToggleLocale() {
	res := as.HTML("/").Get()
	as.Contains(res.Body.String(), "пробвай безплатно")

	res = as.HTML("/lang/toggle/").Get()

	res = as.HTML("/").Get()
	as.Contains(res.Body.String(), "try now for free")

	res = as.HTML("/lang/toggle/").Get()
}

var toCache = [
    "/assets/application.js",
    "/assets/application.css",
    "/assets/vuexStore.js",
    "/assets/images/charts/bar.png",
    "/assets/images/charts/doughnut.png",
    "/assets/images/charts/line.png",
    "/assets/images/charts/pie.png",
    "/assets/images/charts/polarArea.png",
    "/assets/images/charts/radar.png",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "https://use.fontawesome.com/releases/v5.5.0/css/all.css",
    "/assets/images/favicon.ico",
    "/assets/images/welcome/auth_bg.jpg",
    "/assets/images/welcome/1.png",
    "/assets/images/welcome/2.png",
    "/assets/images/welcome/3.png",
    "/assets/images/welcome/4.png",
    "/assets/images/welcome/5.png",
    "/assets/images/welcome/5_.png",
    "/assets/images/DBTable/mysql-logo.png",
    "/assets/images/DBTable/postgres-logo.png",
    "/assets/images/logo.svg",

    //от ръководството за употреба
    "/assets/images/manual/1_1.png",
    "/assets/images/manual/1_2.png",
    "/assets/images/manual/3_1.png",
    "/assets/images/manual/21_1.png",
    "/assets/images/manual/21_2.png",
    "/assets/images/manual/23_1.png",
    "/assets/images/manual/23_2.png",
    "/assets/images/manual/23_3.png",
    "/assets/images/manual/23_4.png",
    "/assets/images/manual/221_1.png",
    "/assets/images/manual/221_2.png",
    "/assets/images/manual/222_1.png",
    "/assets/images/manual/222_2.png",
    "/assets/images/manual/222_3.png",
    "/assets/images/manual/224_1.png",
    "/assets/images/manual/224_2.png",
    "/assets/images/manual/224_3.png",
    "/assets/images/manual/224_4.png",
    "/assets/images/manual/224_5.png",
    "/assets/images/manual/2231_1.png",
    "/assets/images/manual/2231_2.png",
    "/assets/images/manual/2231_3.png",
    "/assets/images/manual/2231_4.png",
    "/assets/images/manual/2232_1.png",
    "/assets/images/manual/2232_2.png",
    "/assets/images/manual/2232_3.png",

];

self.addEventListener("install", function (event) {
    event.waitUntil(
        caches.open('static').then((cache) => {
            for (let i =0; i<toCache.length; i ++){
                try{
                    cache.add(toCache[i]);
                }catch(e){
                }
            }
        })
    );
});

self.addEventListener("activated", function () {
});

self.addEventListener("fetch", function (event) {
    event.respondWith(
        caches.match(event.request).then(function (res) {
            if (res) {
                return res
            } else {
                return fetch(event.request);
            }
        })
    );
});


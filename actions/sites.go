package actions

import (
	"crypto/rand"
	"fmt"
	"strings"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

//Мидълуеър за използването на линк за присъединяване към сайт
func LinkPage(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		//ако няма логнат юзър се зарежда променлива със стринга
		//за присъединяване и юзърът бива редиректван към логин страницата
		if uid := c.Session().Get("current_user_id"); uid == nil {
			c.Session().Set("string_link", c.Param("string"))
			return c.Redirect(302, "/auth/login")
		}
		return next(c)
	}
}

//Мидълуеър, проверяващ дали потребителят е в сайта
func IsInSite(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		//Зареждане на потребител
		u := c.Value("current_user").(*models.User)
		tx := c.Value("tx").(*pop.Connection)

		if c.Param("site_id") != "" {
			s := &models.Site{}
			//Зареждане на сайт от параметъра site_id
			if err := tx.Find(s, c.Param("site_id")); err != nil {
				//Проверява дали сайтът наистина съществува
				if err.Error() == "sql: no rows in result set" {
					c.Flash().Add("danger", T.Translate(c, "site.exists"))
					return c.Redirect(302, "/site/all/")
				}
				//Проверява дали е въведен параметър с uuid.UUID формат
				if strings.Contains(err.Error(), "invalid input syntax for type uuid") {
					c.Flash().Add("danger", T.Translate(c, "site.invalidId"))
					return c.Redirect(302, "/site/all/")
				}
				return errors.WithStack(err)
			}
			//Проверка дали юзърът има достъп до сайта и може да го достъпи
			if in, err := u.IsInSite(*s, tx); err != nil {
				return errors.WithStack(err)
			} else {
				if in == false {
					c.Flash().Add("danger", T.Translate(c, "site.noAccess"))
					return c.Redirect(302, "/site/all/")
				}
			}
		}
		return next(c)
	}
}

//Middleware, дали има право да редактира изтрива и други (admin или owner)
func IsAdmin(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		//Зареждане на потребител
		u := c.Value("current_user").(*models.User)
		tx := c.Value("tx").(*pop.Connection)

		if c.Param("site_id") != "" {
			su := &models.SitesUsers{}
			//Зареждане на потребител от сайт от параметъра site_id
			//и id-то на потребителя
			if err := tx.Where("user_id=?", u.ID).Where("site_id=?", c.Param("site_id")).
				First(su); err != nil {
				//Проверява дали сайтът наистина съществува
				if err.Error() == "sql: no rows in result set" {
					c.Flash().Add("danger", T.Translate(c, "site.dontExists"))
					return c.Redirect(302, "/site/all/")
				}
				//Проверява дали е въведен параметър с uuid.UUID формат
				if strings.Contains(err.Error(), "invalid input syntax for type uuid") {
					c.Flash().Add("danger", T.Translate(c, " site.invalidId"))
					return c.Redirect(302, "/site/all/")
				}
				return errors.WithStack(err)
			}
			//Проверка дали потребителят има достъп като админ или собственик до сайта
			if !(su.Rights == "admin" || su.Rights == "owner") {
				c.Flash().Add("danger", T.Translate(c, "site.notAdmin"))
				return c.Redirect(302, "/site/all/")
			}

		}
		return next(c)
	}
}

//Зарежда страницата с форма за
//създаване на нов сайт
func GetSiteForm(c buffalo.Context) error {
	return c.Render(200, r.HTML("sites/form"))
}

//Добавя нов сайт в базата данни
func CreateSite(c buffalo.Context) error {
	s := &models.Site{}
	//Зареждане на данните от формата в променливата за сайт
	if err := c.Bind(s); err != nil {
		return errors.WithStack(err)
	}

	//Зареждане на потребител от променливата в сесията current_user_id
	uid := c.Session().Get("current_user_id")
	u := &models.User{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(u, uid); err != nil {
		return errors.WithStack(err)
	}

	//Задаване на потребителя в променливата за сайта
	s.Owner = *u

	//Създаване на сайта
	verrs, err := s.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	//Добавяне на потребителя създател като собственик в новия сайт
	if err := u.JoinSite(s, tx, "owner"); err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("site", s)
		c.Set("errors", verrs)
		c.Set("createSiteErrs", true)
		return c.Render(403, r.HTML("sites/form"))
	}

	c.Flash().Add("created_site", T.Translate(c, "site.added"))
	return c.Redirect(302, fmt.Sprintf("/site/all/"))
}

//Зарежда всички сайтове на потребител
func GetAllSites(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	u := c.Value("current_user").(*models.User)

	//owned - зареждане на всички сайтове на които е собственик
	os := models.Sites{}
	os, err := os.GetByRights(tx, u.ID, "owner")
	if err != nil {
		return errors.WithStack(err)
	}

	//admin - на които е администратор
	as := models.Sites{}
	as, err = as.GetByRights(tx, u.ID, "admin")
	if err != nil {
		return errors.WithStack(err)
	}

	//rw - в които има право да чете и редактира
	rws := models.Sites{}
	rws, err = rws.GetByRights(tx, u.ID, "rw")
	if err != nil {
		return errors.WithStack(err)
	}

	//read - в които има право само да чете
	rs := models.Sites{}
	rs, err = rs.GetByRights(tx, u.ID, "read")
	if err != nil {
		return errors.WithStack(err)
	}

	//Създаване на променливи
	c.Set("owned", os)
	c.Set("admin", as)
	c.Set("rw", rws)
	c.Set("read", rs)
	return c.Render(200, r.HTML("sites/allSites"))
}

//Зарежда страницата за определен сайт
func GetSite(c buffalo.Context) error {
	site_id := c.Param("site_id")

	//Зареждане на сайта от параметъра site_id
	s := &models.Site{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(s, site_id); err != nil {
		return errors.WithStack(err)
	}
	//Задаване на потребителите в сайта
	users, err := s.GetUsers(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	//Задаване на правата на логнатия потребител в дадения сайт
	su := &models.SitesUsers{}

	if err := su.GetRights(c.Value("current_user").(*models.User).ID, s.ID); err != nil {
		return errors.WithStack(err)
	}

	c.Set("users", users)
	c.Set("rights", su.Rights)
	c.Set("site", s)
	c.Session().Set("current_side_id", s.ID)
	return c.Render(200, r.HTML("sites/site"))
}

//Редактира определен сайт
func EditSite(c buffalo.Context) error {
	//Зареждане на данните от формата в променливата от сайт
	tx := c.Value("tx").(*pop.Connection)
	s := &models.Site{}
	if err := c.Bind(&s); err != nil {
		return errors.WithStack(err)
	}

	//Редактиране на сайта
	verrs, err := s.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.String(T.Translate(c, "site.edited")))
}

//Генериране на линк за покана
func GenerateLink(c buffalo.Context) error {
	//Генериране на стринг със случайно съдържание
	const TLength = 12
	n := TLength / 2
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		return errors.WithStack(err)
	}
	s := fmt.Sprintf("%X", b)

	//променлива за запазване на стринга
	link := &models.SiteWorkerLink{}
	link.StringLink = nulls.NewString(s)
	site_id, iderr := uuid.FromString(c.Param("site_id"))
	if iderr != nil {
		return errors.WithStack(iderr)
	}
	link.SiteId = site_id

	//запазване на линка в базата
	tx := c.Value("tx").(*pop.Connection)
	verrs, err := link.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("link", link)
		c.Set("errors", verrs)

		return c.Render(403, r.HTML("sites/site"))
	}

	return c.Render(200, r.String(s))
}

//Зареждане на страницата за покана
func GetLinkPage(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	//Зареждане на линка от параметъра string
	var links models.SiteWorkerLinks
	if err := links.GetLinkByString(nulls.NewString(c.Param("string")).String); err != nil {
		return errors.WithStack(err)
	}

	//Зареждане на сайт от линка
	s := &models.Site{}
	for _, link := range links {
		tx := c.Value("tx").(*pop.Connection)
		if err := tx.Find(s, link.SiteId); err != nil {
			return errors.WithStack(err)
		}
	}

	u := c.Value("current_user").(*models.User)

	//Проверка дали юзърът вече не е в сайта
	if in, err := u.IsInSite(*s, tx); err != nil {
		return errors.WithStack(err)
	} else {
		if in == true {
			return c.Redirect(302, "/site/%v", s.ID)
		}
	}

	c.Set("site", s)

	return c.Render(200, r.HTML("sites/linkPage"))
}

//Присъединяване към сайт
func JoinSite(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	//Зареждане на поканата от id идващо след асинхронна заявка
	c.Request().ParseForm()
	invite_id := c.Request().PostForm.Get("invite_id")
	if invite_id != "false" {
		i := &models.Invite{}
		tx := c.Value("tx").(*pop.Connection)
		if err := tx.Find(i, invite_id); err != nil {
			return c.Render(403, r.String(T.Translate(c, "site.joinNoInvite")))
		}
		//Изтриване на поканата
		i.DeleteInvite(tx)
	}

	u := c.Value("current_user").(*models.User)

	//Зараждане на сайта от параметъра site_id
	site_id := c.Param("site_id")
	s := &models.Site{}
	if err := tx.Find(s, site_id); err != nil {
		return errors.WithStack(err)
	}

	//Проверка дали юзърът вече не е в сайта
	if in, err := u.IsInSite(*s, tx); err != nil {
		return errors.WithStack(err)
	} else if in == true {
		return c.Redirect(302, "/site/%v", s.ID)
	}

	//Присъединяване към сайта
	if err := u.JoinSite(s, tx, "read"); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.String("/site/%v", site_id))
}

//Премахване на юзър от сайт
func RemoveUser(c buffalo.Context) error {
	//Конвертиране на параметъра site_id от стринг в uuid.UUID
	sid := c.Param("site_id")
	site_id, iderr := uuid.FromString(sid)
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//Конвертиране на user_id идващ след асинхронна заявка от стринг в uuid.UUID
	c.Request().ParseForm()
	uid := c.Request().PostForm.Get("user_id")
	user_id, iderr := uuid.FromString(uid)
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//Конвертиране на променливата от сесията current_user_id от стринг в uuid.UUID
	cuid := c.Session().Get("current_user_id")
	current_user_id, iderr := uuid.FromString(fmt.Sprintf("%v", cuid))
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//Зареждане на правата на сегашния юзър в сайта
	su_cu := &models.SitesUsers{}
	if err := su_cu.GetRights(current_user_id, site_id); err != nil {
		return errors.WithStack(err)
	}

	//Проверка дали сегашният юзър е собственик или админ
	if su_cu.Rights != "owner" && su_cu.Rights != "admin" {
		return c.Render(403, r.String(T.Translate(c, "site.EditAdmin")))
	}

	//Зареждане на правата в сайта на юзър според user_id идващ след асинхронната заявка
	su := &models.SitesUsers{}
	if err := su.GetRights(user_id, site_id); err != nil {
		return errors.WithStack(err)
	}

	//Премахване на юзъра от сайта
	tx := c.Value("tx").(*pop.Connection)
	su.RemoveUserFromSite(tx)

	return c.Render(200, r.String("%v", user_id))
}

//Смяна на правата на потребител
func ChangeRights(c buffalo.Context) error {
	//Конвертиране на user_id идващ след асинхронна заявка от стринг в uuid.UUID
	c.Request().ParseForm()
	uid := c.Request().PostForm.Get("user_id")
	user_id, iderr := uuid.FromString(uid)
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//Новите права идващи след асинхронна заявка
	rights := c.Request().PostForm.Get("rights")

	//Конвертиране на параметъра site_id от стринг в uuid.UUID
	sid := c.Param("site_id")
	site_id, iderr := uuid.FromString(sid)
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//Зареждане на правата на сегашния юзър
	su_cu := &models.SitesUsers{}
	if err := su_cu.GetRights(c.Value("current_user").(*models.User).ID, site_id); err != nil {
		return errors.WithStack(err)
	}

	//Проверка дали сегашният юзър е администратор или собственик
	if su_cu.Rights != "owner" && su_cu.Rights != "admin" {
		return c.Render(403, r.String(T.Translate(c, "site.EditAdmin")))
	}

	//Зареждане на правата на юзъра, който сме избрали за промяна
	su := &models.SitesUsers{}
	if err := su.GetRights(user_id, site_id); err != nil {
		return errors.WithStack(err)
	}

	//Промяна на правата
	su.Rights = rights

	//Ъпдейт на правата в базата данни
	tx := c.Value("tx").(*pop.Connection)
	su.Update(tx)

	return c.Render(200, r.String("Променен"))
}

//Изтриване на сайт
func DeleteSite(c buffalo.Context) error {
	//Зареждане на сайта от параметъра site_id
	site_id := c.Param("site_id")

	s := &models.Site{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(s, site_id); err != nil {
		return errors.WithStack(err)
	}

	//Проверка, дали сайта се притежава от потребителя
	if s.OwnerId != c.Value("current_user").(*models.User).ID {
		return c.Render(406, r.String(T.Translate(c, "site.DeleteNotOwner")))
	}

	//Конвертиране на параметъра site_id от стринг в uuid.UUID
	sid, iderr := uuid.FromString(site_id)
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//Изтриване на всички записи за потребители на този сайт от базата
	su := &models.SitesUsersMulti{}
	if err := su.DeleteUsersWithSite(sid, tx); err != nil {
		return errors.WithStack(err)
	}

	//Изтриване на сайта
	s.DeleteSite(tx)

	return c.Render(200, r.String("/site/all"))
}

//Качване на снимка на сайт
func UploadPhoto(c buffalo.Context) error {
	//Зареждане на сайта от параметъра site_id
	s := &models.Site{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(s, c.Param("site_id")); err != nil {
		return errors.WithStack(err)
	}

	//Зареждане на снимката от формата
	photo, err := c.File("photo_file")
	if err == nil {
		s.PhotoFile = photo
	}

	//Добавяне и запазване на снимката
	if err := s.SitePhotoUpload(tx); err != nil {
		return errors.WithStack(err)
	}

	//Ъпдейт на данните за сайта в базата
	verrs, err := s.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	//Връщане на страницата при грешка
	if verrs.HasAny() {
		c.Set("errors", verrs)

		users, err := s.GetUsers(tx)
		if err != nil {
			return errors.WithStack(err)
		}

		//Задаване на правата на логнатия потребител в дадения сайт
		su := &models.SitesUsers{}

		if err := su.GetRights(c.Value("current_user").(*models.User).ID, s.ID); err != nil {
			return errors.WithStack(err)
		}

		c.Set("users", users)
		c.Set("rights", su.Rights)
		c.Set("site", s)
		c.Session().Set("current_side_id", s.ID)
		return c.Render(403, r.HTML("sites/site"))
	}

	return c.Redirect(302, "/site/%v", c.Param("site_id"))
}

package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/kolioPesho/kp_admin/models"
	"net/http"
	"time"
)

// ToggleLocale променя езика на сайта (български или англиски)
func ToggleLocale(c buffalo.Context) error {

	lang := "bg"
	url := c.Request().Header.Get("Referer")

	if T.Translate(c, "current_locale") == "bg" {
		lang = "en"
	}

	models.Lang = lang

	// Set new current language using a cookie, for instance
	cookie := http.Cookie{
		Name:   "lang",
		Value:  lang,
		MaxAge: int((time.Hour * 24 * 365).Seconds()),
		Path:   "/",
	}

	http.SetCookie(c.Response(), &cookie)

	T.DefaultLanguage = lang

	// Update language for the flash message
	T.Refresh(c, lang)

	c.Flash().Add("success", T.Translate(c, "language-changed"))

	return c.Redirect(302, url)
}

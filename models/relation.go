package models

// ----------------------------------
//
//  Модел за релациите
//
// ----------------------------------

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/gobuffalo/validate/validators"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	_ "github.com/lib/pq"
)

//Стуктура за описване на релации между колони
//Пример
//SELECT visitlogs.browser, users.email FROM `visitlogs` LEFT JOIN `users` ON users.id = visitlogs.user_id

type Relation struct {
	ID        uuid.UUID `json:"id,omitempty" db:"id"`
	CreatedAt time.Time `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at,omitempty" db:"updated_at"`

	ColumnID uuid.UUID `db:"column_id"` //към коя колона е "прикачена" релацията
	//Aко се чудиш, защо пазя origin нещата, това е защото прецених, че така ще стане по просто
	//от колкот да пазя relation нещата тук, а origin да вземам от таблицата, към която е прикачно
	OriginTable    string `json:"origin_table,omitempty" db:"origin_table"`       //`visitlogs` от примера
	RelationTable  string `json:"relation_table,omitempty" db:"relation_table"`   // `users` от примера
	OriginColumn   string `json:"origin_column,omitempty" db:"origin_column"`     //`user_id` от примера
	RelationColumn string `json:"relation_column,omitempty" db:"relation_column"` //'id' от примера // обикновенно това е id(pk) колона
	ReplaceColumn  string `json:"replace_column,omitempty" db:"replace_column"`   //'email' от примера
}

// String is not required by pop and may be deleted
func (r Relation) String() string {
	jr, _ := json.Marshal(r)
	return string(jr)
}

//Проверява, дали е възможна JOIN LEFT заяка
func (r Relation) Check(dbc DBConn) (bool, error) {
	//Проверява дали може да направи SELECT ... JOIN LEFT ...

	ot := sqlEscape(r.OriginTable)
	rt := sqlEscape(r.RelationTable)
	oc := sqlEscape(r.OriginColumn)
	rc := sqlEscape(r.RelationColumn)
	repCol := sqlEscape(r.ReplaceColumn)
	q := fmt.Sprintf(`
	SELECT %s.%s, %s.%s FROM %s LEFT JOIN %s ON %s.%s=%s.%s LIMIT 1`,
		ot, oc, rt, repCol, ot, rt, ot, oc, rt, rc,
	)

	db, _ := sql.Open(dbc.Driver, dbc.getConnString())
	defer db.Close()
	rows, err := db.Query(q)
	if err != nil {
		return false, err
	}
	defer rows.Close()
	err = rows.Err()
	if err != nil {
		return false, err
	}
	return true, nil
}

// Relations is not required by pop and may be deleted
type Relations []Relation

// String is not required by pop and may be deleted
func (r Relations) String() string {
	jr, _ := json.Marshal(r)
	return string(jr)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (r *Relation) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		&validators.UUIDIsPresent{Field: r.ColumnID, Name: "ColumnId"},
		&validators.StringIsPresent{Field: r.RelationColumn, Name: "RelationColumn"},
		&validators.StringIsPresent{Field: r.RelationTable, Name: "RelationTable"},
		&validators.StringIsPresent{Field: r.OriginColumn, Name: "OriginColumn"},
		&validators.StringIsPresent{Field: r.OriginTable, Name: "OriginTable"},
		&validators.StringIsPresent{Field: r.ReplaceColumn, Name: "ReplaceColumn"},
		// check to see if the email address is already taken:
		&validators.FuncValidator{
			Field:   r.OriginColumn,
			Name:    "column",
			Message: t("relation_hasr"),
			Fn: func() bool {
				var b bool
				b, err = tx.Where("column_id = ?", r.ColumnID).Exists(&Relation{})
				if err != nil {
					return false
				}
				return !b
			},
		},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (r *Relation) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (r *Relation) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

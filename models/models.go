package models

import (
	"fmt"
	"github.com/nicksnyder/go-i18n/i18n"

	"log"

	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
)

// DB is a connection to your database to be used
// throughout your application.
var DB *pop.Connection

var Lang = "en"

func init() {

	var err error
	env := envy.Get("GO_ENV", "development")
	DB, err = pop.Connect(env)
	if err != nil {
		log.Fatal(err)
	}
	pop.Debug = env == "development"

	i18n.LoadTranslationFile(fmt.Sprintf("locales/models.bg.yaml"))
	i18n.LoadTranslationFile(fmt.Sprintf("locales/models.en.yaml"))
}

func t(translationID string, args ...interface{}) string {

	T, _ := i18n.Tfunc(Lang, fmt.Sprintf("locales/models.%s.yaml", Lang))
	return T(translationID, args...)

}

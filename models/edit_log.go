package models

import (
	"database/sql"
	"encoding/json"
	"github.com/pkg/errors"
	"time"

	"github.com/gobuffalo/validate/validators"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

// ------------ EditLog --------------
// Структура, която следи
// Промените по данните в таблиците
// Кой какво, кога е променил

type EditLog struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Column   Column    `json:"column" db:"-"`
	ColumnID uuid.UUID `json:"column_id" db:"column_id"`

	User   User      `json:"user" db:"-"`
	UserID uuid.UUID `json:"user_id" db:"user_id"`

	OldVal string `json:"old_value" db:"old_value"`
	NewVal string `json:"new_value" db:"new_value"`

	PKColumn string `json:"pk_column" db:"pk_column"`
	PKValue  string `json:"pk_value" db:"pk_value"`
}

// String is not required by pop and may be deleted
func (e EditLog) String() string {
	je, _ := json.Marshal(e)
	return string(je)
}

// EditLogs is not required by pop and may be deleted
type EditLogs []EditLog

// String is not required by pop and may be deleted
func (e EditLogs) String() string {
	je, _ := json.Marshal(e)
	return string(je)
}

//Create създава запис в базата
func (e *EditLog) Create(tx *pop.Connection) (*validate.Errors, error) {
	e.ColumnID = e.Column.ID
	e.UserID = e.User.ID
	return tx.ValidateAndCreate(e)
}

//FindLogs намира предишните редакции за някоя клетка
func (els *EditLogs) FindLogs(tx *pop.Connection, c Column, pkCol, pkVal string) error {

	err := tx.Where("column_id=?", c.ID).
		Where("pk_column=?", pkCol).
		Where("pk_value=?", pkVal).All(els)

	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//Връща всички log-ове за дадена таблица
func (els *EditLogs) TableLogs(tx *pop.Connection, t Table) error {
	err := tx.Select("edit_logs.*").
		Join("columns", "edit_logs.column_id=columns.id").
		Where("columns.table_id=?", t.ID).
		Order("created_at asc").
		All(els)

	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

//Проверява, дали PK колоната  съдържа само уникални стойности
func (e *EditLog) CheckPKColumn() (bool, error) {
	q := checkColumnUniqueQuery(e.Column.Table.DB.Driver, e.Column.Table.Name, e.PKColumn)

	//Отваря връзка към отдалечена БД
	db, _ := sql.Open(e.Column.Table.DB.Driver, e.Column.Table.DB.getConnString())
	defer db.Close()

	//Проверка, дали identifier-ите отговарят точно на един ред от базата
	r := db.QueryRow(q)
	var unique string //броя редове, които би се променил

	err := r.Scan(&unique)
	if err != nil {
		return false, errors.WithStack(err)
	}

	if unique == "not unique" {
		return false, nil
	}

	return true, nil
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (e *EditLog) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{Field: e.PKValue, Name: "PKValue"},
		&validators.StringIsPresent{Field: e.PKColumn, Name: "PKColumn"},
		&validators.UUIDIsPresent{Field: e.ColumnID, Name: "ColumnID"},
		&validators.UUIDIsPresent{Field: e.UserID, Name: "UserID"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (e *EditLog) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (e *EditLog) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// AfterFind вика се след намиране на запис
// попълва user и column
func (e *EditLog) AfterFind(tx *pop.Connection) error {
	//намира потребителя
	tx.Find(&e.User, e.UserID)
	//намира колоната
	tx.Find(&e.Column, e.ColumnID)
	return nil
}

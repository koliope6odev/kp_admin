package actions

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/gobuffalo/pop/nulls"
	"github.com/gofrs/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_Charts_CreateChart() {
	as.DB.Destroy(&models.Users{})
	as.LoadFixture("login user")
	var u models.User
	as.NoError(as.DB.Where("name = ?", "login_user").First(&u))
	as.Session.Set("current_user_id", u.ID)
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)
	u.JoinSite(&s, as.DB, "admin")
	createChart(as, s)
}

func (as *ActionSuite) Test_Charts_UpdateChart() {
	as.DB.Destroy(&models.Users{})
	as.LoadFixture("login user")
	var u models.User
	as.NoError(as.DB.Where("name = ?", "login_user").First(&u))
	as.Session.Set("current_user_id", u.ID)
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)
	u.JoinSite(&s, as.DB, "admin")

	c := createChart(as, s)

	c.Verbose = "new verbose"

	url := fmt.Sprintf("/site/%s/chart/update/%s", s.ID, c.ID)
	//Aко всичко е наред
	res := as.JSON(url).Put(&c)
	as.Equal(200, res.Code)
	//проверка дали е променило базата
	var cha models.Chart
	as.NoError(as.DB.Find(&cha, c.ID))
	as.Equal("new verbose", cha.Verbose)

	//Ако има грешка във валидността на данните (твърде дълго име)
	c.Verbose = "123456789–123456789-123456789-123456789-123456789–123456789-123456789-123456789"
	res = as.JSON(url).Put(&c)
	as.Equal(403, res.Code)
	c.Verbose = "ivan"

	//Ако е не възможно свързването или правенето на заявка
	c.Table = "nqma_takava_tablitsa"
	res = as.JSON(url).Put(&c)
	as.Equal(406, res.Code)
}

func (as *ActionSuite) Test_Charts_GetCharts() {
	as.DB.Destroy(&models.Users{})
	as.LoadFixture("login user")
	var u models.User
	as.NoError(as.DB.Where("name = ?", "login_user").First(&u))
	as.Session.Set("current_user_id", u.ID)

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	u.JoinSite(&s, as.DB, "admin")

	//Създава малко диаграми
	createChart(as, s)
	for i := 0; i < 12; i++ {
		createChart(as, s)
	}

	res := as.HTML(fmt.Sprintf("/site/%s/chart/get/all", s.ID)).Get()
	as.Equal(200, res.Code)

	var cs models.Charts
	json.Unmarshal([]byte(res.Body.String()), &cs)
	as.Equal(13, len(cs))
}

func (as *ActionSuite) Test_Charts_DeleteChart() {
	as.DB.Destroy(&models.Users{})
	as.LoadFixture("login user")
	var u models.User
	as.NoError(as.DB.Where("name = ?", "login_user").First(&u))
	as.Session.Set("current_user_id", u.ID)
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)
	u.JoinSite(&s, as.DB, "admin")

	c := models.Chart{
		Table:  "columns",
		Column: "data_type",
		Type:   "bar",
		SiteID: s.ID,
	}

	as.NoError(as.DB.Create(&c))

	url := fmt.Sprintf("/site/%s/chart/delete/%s", s.ID, c.ID)
	as.DBDelta(-1, "charts", func() {
		res := as.JSON(url).Delete()
		as.Equal(200, res.Code)
	})

}

func (as *ActionSuite) Test_Charts_GetChart() {
	as.LoadFixture("login user")
	var u models.User
	as.NoError(as.DB.Where("name = ?", "login_user").First(&u))
	as.Session.Set("current_user_id", u.ID)
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)
	u.JoinSite(&s, as.DB, "admin")

	c := models.Chart{
		Table:  "columns",
		Column: "data_type",
		Type:   "bar",
		SiteID: s.ID,
	}

	as.NoError(as.DB.Create(&c))

	res := as.HTML(fmt.Sprintf("/site/%s/chart/%s/", s.ID, c.ID)).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</chart>")
}

func createChart(as *ActionSuite, site models.Site) models.Chart {

	url := fmt.Sprintf("/site/%s/chart/create", site.ID)

	//празен PUT
	res := as.JSON(url).Put(models.Chart{})
	as.Equal(406, res.Code)

	dbc := models.DBConn{
		Site:     site,
		Driver:   "postgres",
		Host:     "127.0.0.1",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Name:     time.Now().String(),
		Port:     5432,
	}

	verrs, err := dbc.Create(models.DB)
	as.NoError(err)
	fmt.Printf("%v", verrs.Errors)
	as.False(verrs.HasAny())

	c := models.Chart{
		DBID:           dbc.ID,
		Table:          "columns",
		Column:         "data_type",
		Type:           "bar",
		Verbose:        "ime na bar-charts",
		StatisticsType: "percentage",
		SiteID:         site.ID,
	}

	//Празна колона
	c.Column = ""
	res = as.JSON(url).Put(c)
	as.Equal(406, res.Code)

	//Несъществуваща колона
	c.Column = "ivan"
	res = as.JSON(url).Put(c)
	as.Equal(406, res.Code)
	c.Column = "data_type"

	//Празна таблица
	c.Table = ""
	res = as.JSON(url).Put(c)
	as.Equal(406, res.Code)

	//Несъществуваща таблица
	c.Table = "го6о"
	res = as.JSON(url).Put(c)
	as.Equal(406, res.Code)
	c.Table = "columns"

	//Грешен тип
	c.Type = "пюре"
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	c.Type = "bar"

	//Без име
	c.Verbose = ""
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)

	//Твърде дълго име
	c.Verbose = "123456789–123456789-123456789-123456789-123456789–123456789-123456789-123456789"
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	c.Verbose = "ime na bar-charts"

	as.DBDelta(1, "charts", func() {
		res = as.JSON(url).Put(c)
		as.Equal(200, res.Code)
		idstring := strings.Replace(res.Body.String(), "\"", "", 2)[:36]
		fmt.Println(idstring)
		c.ID, err = uuid.FromString(idstring)
		as.NoError(err)
	})
	return c
}

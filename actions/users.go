package actions

//------------------------------
// Контролер, който се грижи за нещата свързани с потребителите
// Добавяне на профилна снимка, мета данни,
// Логика при login и sing up форми  др
// Middleware-и
//-----------------------------

import (
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

//Мидълуеър, който проверява дали профилът принадлежи на сегашния потребител
func IsMyProfile(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		profile_id, iderr := uuid.FromString(c.Param("user_id"))
		if iderr != nil {
			return errors.WithStack(iderr)
		}
		current_user_id, iderr := uuid.FromString(fmt.Sprintf("%v", c.Session().Get("current_user_id")))
		if iderr != nil {
			return errors.WithStack(iderr)
		}
		if profile_id != current_user_id {
			c.Flash().Add("danger", "Не може да се променят данните на други потребители!")
			return c.Redirect(302, "/user/%v", c.Param("user_id"))
		}
		return next(c)
	}
}

// UsersLoginGet показва login форма
func UsersLoginGet(c buffalo.Context) error {
	return c.Render(200, r.HTML("auth/login", "layouts/login"))
}

// UsersCreate съсдава нов потребител (при request oт sign up форма)
func UsersCreate(c buffalo.Context) error {
	u := &models.User{}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}

	tx := c.Value("tx").(*pop.Connection)
	verrs, err := u.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("user", u)
		c.Set("errors", verrs)
		c.Set("signUpErrs", true)
		return c.Render(403, r.HTML("auth/login", "layouts/login"))
	}

	c.Session().Set("current_user_id", u.ID)
	c.Flash().Add("success", "Добре дошли в Bananamin !")

	//Ако има лик за присъедиянване към сайт
	if string_link := c.Session().Get("string_link"); string_link != nil {
		c.Session().Delete("string_link")
		return c.Redirect(302, "/site/join/%v", string_link)
	}

	return c.Redirect(302, "/home")
}

//Зареждане на профил страницата
func GetProfile(c buffalo.Context) error {
	u := &models.User{}
	uid := c.Param("user_id")

	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(u, uid); err != nil {
		return errors.WithStack(err)
	}

	//owned
	os := models.Sites{}
	os, err := os.GetByRights(tx, u.ID, "owner")
	if err != nil {
		return errors.WithStack(err)
	}

	//other
	as := models.Sites{}
	as, err = as.GetByRights(tx, u.ID, "not_owner")
	if err != nil {
		return errors.WithStack(err)
	}

	if uid == fmt.Sprintf("%v", c.Session().Get("current_user_id")) {
		c.Set("myProfile", true)
	} else {
		c.Set("myProfile", false)
	}

	c.Set("user", u)
	c.Set("owned", os)
	c.Set("other", as)
	return c.Render(200, r.HTML("user/profile"))
}

//Редактиране на мета данните на профила
func EditMetaProfile(c buffalo.Context) error {
	u := &models.User{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(u, c.Param("user_id")); err != nil {
		return errors.WithStack(err)
	}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}

	verrs, err := u.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("user", u)
		c.Set("errors", verrs)
		if c.Param("user_id") == fmt.Sprintf("%v", c.Session().Get("current_user_id")) {
			c.Set("myProfile", true)
		} else {
			c.Set("myProfile", false)
		}

		os := models.Sites{}
		os, err := os.GetByRights(tx, u.ID, "owner")
		if err != nil {
			return errors.WithStack(err)
		}

		//other
		as := models.Sites{}
		as, err = as.GetByRights(tx, u.ID, "not_owner")
		if err != nil {
			return errors.WithStack(err)
		}

		c.Set("owned", os)
		c.Set("other", as)

		return c.Render(403, r.HTML("user/profile"))
	}

	return c.Redirect(302, "/user/%v", c.Param("user_id"))
}

//Редактиране на паролата
func EditPassword(c buffalo.Context) error {
	u := &models.User{}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}
	tx := c.Value("tx").(*pop.Connection)
	old_user := &models.User{}
	if err := tx.Find(old_user, c.Param("user_id")); err != nil {
		return errors.WithStack(err)
	}

	old_user.Password = u.Password
	old_user.PasswordConfirmation = u.PasswordConfirmation

	verrs, err := old_user.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("user", old_user)
		c.Set("errors", verrs)
		if c.Param("user_id") == fmt.Sprintf("%v", c.Session().Get("current_user_id")) {
			c.Set("myProfile", true)
		} else {
			c.Set("myProfile", false)
		}

		return c.Render(403, r.HTML("user/profile"))
	}

	return c.Redirect(302, "/user/%v", c.Param("user_id"))
}

//Качване на снимки за аватар и корица
func ProfileFileUpload(c buffalo.Context) error {
	u := &models.User{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(u, c.Param("user_id")); err != nil {
		return errors.WithStack(err)
	}

	avatar, err := c.File("avatar_file")
	if err == nil {
		u.AvatarFile = avatar
	}

	cover, err := c.File("cover_file")
	if err == nil {
		u.CoverFile = cover
	}

	if err := u.ProfileUpload(tx); err != nil {
		return errors.WithStack(err)
	}

	verrs, err := u.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("user", u)
		c.Set("errors", verrs)
		if c.Param("user_id") == fmt.Sprintf("%v", c.Session().Get("current_user_id")) {
			c.Set("myProfile", true)
		} else {
			c.Set("myProfile", false)
		}
		return c.Render(403, r.HTML("user/profile"))
	}

	return c.Redirect(302, "/user/%v", c.Param("user_id"))
}

//Редактиране на уменията
func EditSkills(c buffalo.Context) error {
	c.Request().ParseForm()
	skills := c.Request().PostForm.Get("skills")

	u := &models.User{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(u, c.Param("user_id")); err != nil {
		return errors.WithStack(err)
	}

	u.Skills = nulls.NewString(skills)

	verrs, err := u.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("user", u)
		c.Set("errors", verrs)
		if c.Param("user_id") == fmt.Sprintf("%v", c.Session().Get("current_user_id")) {
			c.Set("myProfile", true)
		} else {
			c.Set("myProfile", false)
		}

		return c.Render(403, r.HTML("user/profile"))
	}

	return c.Render(200, r.String("Успех"))
}

package actions

import (
	"fmt"
	"os"

	"github.com/gobuffalo/httptest"
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/ulule/deepcopier"
)

//Test_UsersLoginGet Проврява дали връща правилната страница
//Aко няма Логнат Потребител
func (as *ActionSuite) Test_UsersLoginGet_NotLogged() {
	res := as.HTML("/auth/login").Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</login>")
}

//Test_UsersLoginGet Проврява дали връща правилната страница (welcome.html)
//Aко има Логнат Потребител
func (as *ActionSuite) Test_UsersLoginGet_Logged() {

	// Взима потребител от базата
	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	res := as.HTML("/auth/login").Get()
	as.Equal(302, res.Code)
	//Трябва да редиректва към /home
	as.Equal("/home", res.Header().Get("Location"))

	// изтрива сесията (log out-ва потребителя)
	as.Session.Clear()
	res = as.HTML("/auth/login").Get()
	as.Equal(200, res.Code)

	as.Contains(res.Body.String(), "</login>")
}

//Test_UsersCreate провярява за правилното създаване(регистрация) на потребители
func (as *ActionSuite) Test_UsersCreate() {

	u := &models.User{}
	u.Email = nulls.NewString("valid@mail.ivan")
	u.Password = "pass123"
	u.PasswordConfirmation = "pass123"

	//невалиден email
	u1 := models.User{}
	deepcopier.Copy(u).To(u1)
	u1.Email = nulls.NewString("Nikola Petrov")
	res := as.HTML("/auth/new").Put(u1)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Въведете валиден E-mail адрес")
	res = nil

	//празно поле за email
	u2 := models.User{}
	deepcopier.Copy(u).To(u2)
	u2.Email = nulls.NewString("")
	res = as.HTML("/auth/new").Put(u2)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Въведете валиден E-mail адрес")
	res = nil

	//паролите не съвпадат
	var u3 models.User
	deepcopier.Copy(u).To(u3)
	u3.Email = nulls.NewString("test@gmail.com")
	u3.PasswordConfirmation = "pass1234"
	res = as.HTML("/auth/new").Put(u3)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Паролите не съвпадат")
	res = nil

	//празно поле password
	u4 := models.User{}
	deepcopier.Copy(u).To(u4)
	u4.Email = nulls.NewString("test@gmail.com")
	u4.Password = ""
	res = as.HTML("/auth/new").Put(u4)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за парола не може да бъде празно.")
	res = nil

	//твърде късо поле за парола
	u5 := models.User{}
	deepcopier.Copy(u).To(u5)
	u5.Email = nulls.NewString("test@gmail.com")
	u5.Password = "парол"
	res = as.HTML("/auth/new").Put(u5)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Паролата е твърде къса.")
	res = nil

	//празно password_confirmation
	u6 := models.User{}
	deepcopier.Copy(u).To(u6)
	u6.Email = nulls.NewString("test@gmail.com")
	u6.Password = "test123"
	u6.PasswordConfirmation = ""
	res = as.HTML("/auth/new").Put(u6)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за потвърждение на паролата не може да бъде празно.")
	res = nil

	//повече от 1 грешки във формата
	u7 := models.User{}
	u7.Email = nulls.NewString("testNotValidGmail.com")
	res = as.HTML("/auth/new").Put(u7)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за потвърждение на паролата не може да бъде празно.")
	as.Contains(res.Body.String(), "Полето за парола не може да бъде празно.")
	as.Contains(res.Body.String(), "Въведете валиден E-mail адрес")
	res = nil

	//Зает email
	user := &models.User{
		Name:  nulls.NewString(""),
		Email: nulls.NewString("test@gmail.test"),
	}
	var u8 models.User
	u8 = *user
	err := as.DB.Create(user)
	as.NoError(err)
	res = as.HTML("/auth/new").Put(u8)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Email-а \\\"test@gmail.test\\\" вече е зает.")
	res = nil

	//Валидена форма
	res = as.HTML("/auth/new").Put(u)
	as.Equal(302, res.Code)
	as.Equal("/home", res.Header().Get("Location"))
	res = nil

}

//Test_UsersCreate_OldRequest проверява дали при невалидна форма за регистрация, данните (email-a) остава
func (as *ActionSuite) Test_UsersCreate_OldRequest() {

	u := &models.User{}
	u.Email = nulls.NewString("old@mail.com")

	res := as.HTML("/auth/new").Put(u)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "old@mail.com")
	res = nil

}

//Тест за зареждане на страницата за профил
func (as *ActionSuite) Test_UsersGetProfile() {
	res := as.HTML("/user/1223").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	u1 := &models.User{
		Name: nulls.NewString("Колио1"),

		Email: nulls.NewString("test1@gmail.test"),
	}
	err := as.DB.Create(u)
	err = as.DB.Create(u1)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")
	s.OwnerId = u.ID

	err = as.DB.Create(s)

	s1 := &models.Site{}
	s1.Name = nulls.NewString("Name1")
	s1.Desc = nulls.NewString("Description1")
	s1.Link = nulls.NewString("https://link1.bg")
	s1.OwnerId = u1.ID
	err = as.DB.Create(s1)
	as.NoError(err)

	su := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u.ID,
		Rights: "owner",
	}

	su1 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u.ID,
		Rights: "read",
	}

	err = as.DB.Create(su)
	err = as.DB.Create(su1)
	as.NoError(err)

	res = as.HTML("/user/%v", u.ID).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</profile-table>")
}

//Тест за редактиране на мета данни на профил
func (as *ActionSuite) Test_UsersEditMetaProfile() {
	u1 := &models.User{}
	res := as.HTML("/user/editMeta/128421").Post(u1)
	as.Equal(302, res.Code)

	u := models.User{
		Name:         nulls.NewString("Kolio"),
		Email:        nulls.NewString("test@gmail.test"),
		PasswordHash: "PasswordHash",
	}
	err := as.DB.Create(&u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	u.Name = nulls.NewString("NameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLonmeTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNamg")
	u.Occupation = nulls.NewString("NameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLong")
	u.Phone = nulls.NewString("123456789012345")
	u.City = nulls.NewString("NameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLongNameTooLong")

	res1 := as.JSON(fmt.Sprintf("/user/editMeta/%v", u.ID)).Post(&u)
	as.Equal(403, res1.Code)
	as.Contains(res.Body.String(), "Name too long")
	as.Contains(res.Body.String(), "Occupation too long")
	as.Contains(res.Body.String(), "Phone too long")
	as.Contains(res.Body.String(), "City too long")

	u.Name = nulls.NewString("Kolio")
	u.Occupation = nulls.NewString("programmer")
	u.Phone = nulls.NewString("0881212121")
	u.City = nulls.NewString("Dobrich")

	res1 = as.JSON(fmt.Sprintf("/user/editMeta/%v", u.ID)).Post(&u)
	as.Equal(302, res1.Code)
	as.Equal(fmt.Sprintf("/user/%v", u.ID), res1.Header().Get("Location"))
	res = nil
}

//Тест за редактиране на паролата
func (as *ActionSuite) Test_UsersEditPassword() {
	u1 := &models.User{}
	res := as.HTML("/user/editPass/128421").Post(u1)
	as.Equal(302, res.Code)

	u := models.User{
		Name:         nulls.NewString("Kolio"),
		Email:        nulls.NewString("test@gmail.test"),
		PasswordHash: "PasswordHash",
	}
	err := as.DB.Create(&u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	u.Password = "123"
	u.PasswordConfirmation = "123"

	res1 := as.JSON(fmt.Sprintf("/user/editPass/%v", u.ID)).Post(&u)
	as.Equal(403, res1.Code)
	as.Contains(res.Body.String(), "Password is too short")

	u.Password = "1234567"
	u.PasswordConfirmation = "1234567"

	res1 = as.JSON(fmt.Sprintf("/user/editPass/%v", u.ID)).Post(&u)
	as.Equal(302, res1.Code)
	as.Equal(fmt.Sprintf("/user/%v", u.ID), res1.Header().Get("Location"))
	res = nil
}

//Тест за редактиране на ъплоада на аватар и корица
func (as *ActionSuite) Test_UsersProfileUpload() {
	u1 := &models.User{}
	res := as.HTML("/user/file/128421").Post(u1)
	as.Equal(302, res.Code)

	u := models.User{
		Name:         nulls.NewString("Kolio"),
		Email:        nulls.NewString("test@gmail.test"),
		PasswordHash: "PasswordHash",
	}
	err := as.DB.Create(&u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	r, err := os.Open("../assets/images/test_upload/kolio_test.jpg")
	as.NoError(err)

	avatar := httptest.File{
		// ParamName is the name of the form parameter
		ParamName: "avatar_file",
		// FileName is the name of the file being uploaded
		FileName: "AvatarTest",
		Reader:   r,
	}
	cover := httptest.File{
		// ParamName is the name of the form parameter
		ParamName: "cover_file",
		// FileName is the name of the file being uploaded
		FileName: "CoverTest",
		Reader:   r,
	}

	res1, err1 := as.HTML(fmt.Sprintf("/user/file/%v", u.ID)).MultiPartPost(u, avatar, cover)
	as.NoError(err1)
	as.Equal(302, res1.Code)
	as.Equal(fmt.Sprintf("/user/%v", u.ID), res1.Header().Get("Location"))
}

//Тест за редактиране на уменията
func (as *ActionSuite) Test_UsersEditSkills() {
	data1 := make(map[string]string)
	res := as.HTML("/user/skills/a12123123").Put(data1)
	as.Equal(302, res.Code)

	u := models.User{
		Name:         nulls.NewString("Kolio"),
		Email:        nulls.NewString("test@gmail.test"),
		PasswordHash: "PasswordHash",
	}
	err := as.DB.Create(&u)
	as.NoError(err)

	as.Session.Set("current_user_id", u.ID)

	data := make(map[string]string)
	data["skills"] = "[\"kolio\", \"pesho\"]"

	res1 := as.HTML(fmt.Sprintf("/user/skills/%v", u.ID)).Put(data)
	as.Equal(200, res1.Code)
	as.Contains(res1.Body.String(), "Успех")
	res = nil
}

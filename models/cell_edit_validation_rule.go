package models

import (
	"encoding/json"
	"reflect"
	"strconv"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/validate/validators"
	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

// ------------ CellEditValidationRule --------------
// Структура, която пази правила
// за валидация при редакция на данни в някоя клетка
// и изпълнява валидациите

type CellEditValidationRule struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	ColumnID uuid.UUID `json:"column_id" db:"column_id" ` //към коя колона е "прикачена" валидацията

	Function   string        `json:"func"    db:"function_type"` //вида на финкьията
	Params     []interface{} `json:"params"  db:"-"`
	ParamsJson string        `json:"-"       db:"params"`
	Message    nulls.String  `json:"message" db:"message"`
}

//Create - wrapper около логиката за запазване в базата
func (c *CellEditValidationRule) Create(tx *pop.Connection) (*validate.Errors, error) {
	b, err := json.Marshal(c.Params)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	c.ParamsJson = string(b)
	return tx.ValidateAndCreate(c)
}

//ValidateCell проверява, дали стринга v отговарвя на правилата за валидация
func (c CellEditValidationRule) ValidateCell(v string) error {

	validator := c.GetValidator(v)
	if validator == nil {
		return errors.New(t("cellValidation.noValidator"))
	}

	verrs := validate.Validate(validator)

	if verrs.HasAny() {
		return verrs
	}

	return nil
}

//GetValidator връща правилната функсиця за валидация
func (c CellEditValidationRule) GetValidator(v string) validate.Validator {
	//Съобщението по подразбиране
	m := c.Message.String

	intV := 0
	var err error
	//Aко функцията за валидация изисква int като стойност на параметъра
	if c.Function == "intRequired" || c.Function == "intGraterThan" || c.Function == "intLessThan" {
		intV, err = strconv.Atoi(v)
		if err != nil {
			return &notValidIntValidator{Message: stringOr(m, t("cellValidation.invalidNumber"))}
		}
	}

	var dateV time.Time
	//Aко функцията за валидация изисква date като стойност на параметъра
	if c.Function == "dateBefore" || c.Function == "dateAfter" || c.Function == "dateBeforeNow" || c.Function == "dateAfterNow" {
		dateV, err = time.Parse("2006-01-02 (15:04)", v)
		if err != nil {
			return &notValidDateValidator{Message: stringOr(m, t("cellValidation.invalidDate"))}
		}
	}

	switch c.Function {
	case "stringLengthInRange":
		return &validators.StringLengthInRange{
			Field: v,
			Message: stringOr(m, t("cellValidation.invalidLength", map[string]interface{}{
				"min": c.Params[0],
				"max": c.Params[1],
			})),
			Min: c.Params[0].(int), Max: c.Params[1].(int)}
	case "stringRequired":
		return &validators.StringIsPresent{Field: v, Message: stringOr(m, t("cellValidation.required"))}
	case "email":
		//Така позволявам въвеждането на  "" като стойност
		if v == "" {
			v = "valid@email.com"
		}
		return &validators.EmailLike{Field: v, Message: stringOr(m, t("cellValidation.invalidEmail"))}
	case "url":
		//Така позволявам въвеждането на  "" като стойност
		if v == "" {
			v = "https://valid.com"
		}
		return &validators.URLIsPresent{Field: v, Message: stringOr(m, t("cellValidation.invalidUrl"))}
	case "intRequired":
		return &validators.IntIsPresent{Field: intV, Message: stringOr(m, t("cellValidation.numberRequired"))}
	case "intGraterThan":
		return &validators.IntIsGreaterThan{Field: intV,
			Message: stringOr(m, t("cellValidation.mustBeGraterThan", map[string]interface{}{
				"min": c.Params[0],
			})),
			Compared: c.Params[0].(int)}
	case "intLessThan":
		return &validators.IntIsLessThan{Field: intV,
			Message: stringOr(m, t("cellValidation.mustBeLessThan", map[string]interface{}{
				"max": c.Params[0],
			})),
			Compared: c.Params[0].(int)}

	case "dateBefore":
		return &validators.TimeIsBeforeTime{FirstTime: dateV, SecondTime: c.Params[0].(time.Time),
			Message: stringOr(m, t("cellValidation.dateMustBeBefore", map[string]interface{}{
				"date": c.Params[0].(time.Time).Format("2006-01-02"),
			}))}

	case "dateAfter":
		return &validators.TimeAfterTime{FirstTime: dateV, SecondTime: c.Params[0].(time.Time),
			Message: stringOr(m, t("cellValidation.dateMustBeAfter", map[string]interface{}{
				"date": c.Params[0].(time.Time).Format("2006-01-02"),
			}))}

	case "dateBeforeNow":
		return &validators.TimeIsBeforeTime{FirstTime: dateV, SecondTime: time.Now(),
			Message: stringOr(m, t("cellValidation.dateMustBeBefore", map[string]interface{}{
				"date": time.Now().Format("2006-01-02"),
			}))}

	case "dateAfterNow":
		return &validators.TimeAfterTime{FirstTime: dateV, SecondTime: time.Now(),
			Message: stringOr(m, t("cellValidation.dateMustBeAfter", map[string]interface{}{
				"date": time.Now().Format("2006-01-02"),
			}))}
	}

	return nil
}

// stringOr helper функцийка
// ако подаденият string е "" -> връща default string-a
// ако подаденият string НЕ е -> връща подаденият string S
func stringOr(s, def string) string {
	if s == "" {
		return def
	}
	return s
}

//Валидатор, който връща грешка по подразбиране
//Използва се, когато GetValidator има проблем с
// преобразуването на string в int
type notValidIntValidator struct {
	Message string
}

func (n *notValidIntValidator) IsValid(errors *validate.Errors) {
	errors.Add("numeric", n.Message)
}

//Валидатор, който връща грешка по подразбиране
//Използва се, когато GetValidator има проблем с
// преобразуването на string в date
type notValidDateValidator struct {
	Message string
}

func (n *notValidDateValidator) IsValid(errors *validate.Errors) {
	errors.Add("date", n.Message)
}

// String is not required by pop and may be deleted
func (c CellEditValidationRule) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// CellEditValidationRules is not required by pop and may be deleted
type CellEditValidationRules []CellEditValidationRule

// String is not required by pop and may be deleted
func (c CellEditValidationRules) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (c *CellEditValidationRule) Validate(tx *pop.Connection) (*validate.Errors, error) {
	vals := []validate.Validator{
		&validators.StringInclusion{Field: c.Function, List: []string{
			"stringLengthInRange", "stringRequired",
			"email", "url", "intRequired",
			"intGraterThan", "intLessThan",
			"dateBefore", "dateAfter", "dateBeforeNow",
			"dateAfterNow",
		}},
		&validators.IntIsLessThan{Compared: 3, Field: len(c.Params), Message: "Max number of params is 2"},
		&validators.StringLengthInRange{Field: c.Message.String, Min: 0, Max: 200, Message: t("cellValidation.invalidMessageLength", map[string]interface{}{
			"max": 200,
		})},
		&validators.UUIDIsPresent{Field: c.ColumnID},
		&validators.StringIsPresent{Field: c.ParamsJson},
	}
	//Функции, които изискват 2 параметра
	if c.Function == "stringLengthInRange" {
		vals = append(vals, &validators.IntsAreEqual{ValueOne: 2, ValueTwo: len(c.Params), Message: "2 params are needed"})
	}

	//Функции, които изискват 1 параметр
	if c.Function == "intGraterThan" || c.Function == "intLessThan" || c.Function == "dateBefore" || c.Function == "dateAfter" {
		vals = append(vals, &validators.IntsAreEqual{ValueOne: 1, ValueTwo: len(c.Params), Message: "1 param is needed"})
	}

	//Aко параемтрите трябва да са date
	if c.Function == "dateBefore" || c.Function == "dateAfter" {
		if len(c.Params) == 1 && reflect.TypeOf(c.Params[0]).String() != "time.Time" {
			vals = append(vals, &notValidDateValidator{Message: t("cellValidation.invalidDateParam")})
		}
	} else if c.Function == "intGraterThan" || c.Function == "intLessThan" || c.Function == "stringLengthInRange" { //Aко параемтрите трябва да са int
		if len(c.Params) == 1 && reflect.TypeOf(c.Params[0]).String() != "int" {
			vals = append(vals, &notValidIntValidator{Message: t("cellValidation.invalidIntParam")})
		} else if len(c.Params) == 2 && (reflect.TypeOf(c.Params[0]).String() != "int" || reflect.TypeOf(c.Params[1]).String() != "int") {
			vals = append(vals, &notValidIntValidator{Message: t("cellValidation.invalidIntParams")})
		}
	}

	return validate.Validate(vals...), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (c *CellEditValidationRule) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (c *CellEditValidationRule) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

//AfterFind вика се след намиране на запис
func (c *CellEditValidationRule) AfterFind(tx *pop.Connection) error {

	if err := json.Unmarshal([]byte(c.ParamsJson), &c.Params); err != nil {
		return errors.WithStack(err)
	}

	//Прави float64 на int
	for k, i := range c.Params {
		if reflect.TypeOf(i).String() == "float64" {
			c.Params[k] = int(i.(float64))
		}
	}

	//Прави string на date
	for k, i := range c.Params {
		if reflect.TypeOf(i).String() == "string" {
			c.Params[k], _ = time.Parse(time.RFC3339Nano, i.(string))
		}
	}

	return nil
}

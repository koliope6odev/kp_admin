package models

// ----------------------------
//
//  Модел за колоните в таблица
//
// ----------------------------

import (
	"fmt"
	"strconv"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/validate/validators"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

type Column struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	TableID  uuid.UUID    `json:"table_id"  db:"table_id" `
	Column   string       `json:"column" db:"column_name" `
	Verbose  nulls.String `json:"verbose" db:"verbose_name"`
	ReadOnly bool         `json:"readonly" db:"read_only"`

	Table           Table                   `json:"-"  db:"-"`
	Relation        Relation                `json:"relation,omitempty"  has_one:"relation" fk_id:"column_id"`                   //ако има релации
	MutationsFunc   MutationFunc            `json:"mutation_function,omitempty"  has_one:"mutation_function" fk_id:"column_id"` //ако има модифициращи функции
	ValidationRules CellEditValidationRules `json:"validation_rules" has_many:"cell_edit_validation_rule"`                      // правила за валидация (при редакция)

	DataType         string `db:"data_type"  json:"data_type"`                   //тривиалното именование на вида данни
	DataTypeOriginal string `db:"data_type_original"  json:"data_type_original"` //вида данни, както идва от специфичната база
	DataTypeReadOnly bool   `db:"data_type_readonly" json:"data_type_readonly"`  //дали колоната е от онези типове , за които ще е трън в задника да позволим на потребителите да редактират - геометричните видове ip, mac, bytea и тн
}

// String is not required by pop and may be deleted
func (c Column) String() string {
	return fmt.Sprintf("#%v column \"%s\" from table \"%s\" datatype=%s", c.ID, c.Column, c.Table.Name, c.DataType)
}

// Columns is not required by pop and may be deleted
type Columns []Column

// String is not required by pop and may be deleted
func (c Columns) String() (s string) {
	for _, col := range c {
		s += fmt.Sprintf("column \"%s\" from table \"%s\"   (relation - %s.%s) \n ", col.Column, col.Table.Name, col.Relation.RelationTable, col.Relation.RelationColumn)
	}
	return
}

//Update wrapper около логиката за редактиране  на запис за  колона
func (c Column) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(&c)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (c *Column) Validate(tx *pop.Connection) (*validate.Errors, error) {

	var dTypes []string
	for d := range dataTypes {
		for k := range dataTypes[d] {
			dTypes = append(dTypes, k)
		}
	}

	return validate.Validate(
		&validators.StringLengthInRange{
			Field: c.Verbose.String, Name: "verbose", Max: 50, Message: t("column.nameInRange", map[string]interface{}{
				"max": 50, "min": 2,
			})},
		&validators.StringInclusion{Field: c.DataType, Name: "data type", List: dTypes},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (c *Column) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (c *Column) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	//уверява се, че не се променят типа данни, таблицата и други
	oldC := Column{}
	tx.Find(&oldC, c.ID)
	tId := c.Table.ID
	if tId == uuid.Nil {
		tId = c.TableID
	}

	return validate.Validate(
		&validators.StringsMatch{Field: tId.String(), Field2: oldC.TableID.String(), Name: "table_id", Message: t("column.tableChange")},
		&validators.StringsMatch{Field: c.DataTypeOriginal, Field2: oldC.DataTypeOriginal, Name: "data_type_original", Message: t("column.data_type_originalChange")},
		&validators.StringsMatch{Field: c.DataType, Field2: oldC.DataType, Name: "data_type", Message: t("column.data_typeChange")},
		&validators.StringsMatch{Field: strconv.FormatBool(c.DataTypeReadOnly), Field2: strconv.FormatBool(oldC.DataTypeReadOnly), Name: "data_type_readonly", Message: t("column.data_type_readonlyChange")},
		&validators.StringsMatch{Field: c.Column, Field2: oldC.Column, Name: "column", Message: t("column.columnChange")},

		//Проверява, дали колоната може да стане и за писане
		&validators.FuncValidator{
			Name:    "Readonly",
			Message: t("column.ReadonlyChange"),
			Fn: func() bool {
				if oldC.DataTypeReadOnly && !c.ReadOnly {
					return false
				}
				return true
			},
		},
	), nil
}

func (c *Column) BeforeCreate(tx *pop.Connection) error {
	if c.Table.ID != uuid.Nil {
		c.TableID = c.Table.ID
	}
	if c.DataTypeReadOnly {
		c.ReadOnly = true
	}
	return nil
}

//Връща връзката към база данни, за дадената колона
func (c Column) GetDB(tx *pop.Connection) (DBConn, error) {
	var d DBConn
	var t Table

	t = c.Table
	if c.Table.ID == uuid.Nil {
		if err := tx.Find(&t, c.TableID); err != nil {
			return DBConn{}, err
		}
	}

	d = t.DB
	if t.DB.ID == uuid.Nil {
		if err := tx.Find(&d, t.DBID); err != nil {
			return DBConn{}, err
		}
	}
	return d, nil
}

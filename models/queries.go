package models

import (
	"fmt"
	"strings"
	"time"
)

//------------------------------------------------------------------------------
// Всички неща свързани с изпълнението на SQL заявки върху отдалеяен сървър
// Съставя заяки за :
// 		* извличане данни за таблиците
//      * извличане данни за колоните в дадена таблица
//		* съставяне на комплексни SELECT заявки
//		* броене на записите в таблица
//		* редактиране на запис
//		* проверяване уникалността на записите в колона
//		* заявки свързани с визуализацията на даграми (извичането на статистика)
//
// Определя, кои видове колони са:
// 		само за четене, с забранено търсене и др
//-------------------------------------------------------------------------------

//Заявки за основните данни в базите - данните за таблици и колони
var queries = map[string]map[string]string{

	"mysql": {
		"tables": "SHOW TABLES;",
		"columns": `
    SELECT COLUMN_NAME, DATA_TYPE
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA=?
	AND TABLE_NAME=?;`,
	},

	"postgres": {
		"tables": `SELECT
		table_schema || '.' || table_name
		FROM
		information_schema.tables
		WHERE
		table_type = 'BASE TABLE'
  		AND
		table_schema NOT IN ('pg_catalog', 'information_schema');`,

		"columns": `SELECT column_name, data_type
FROM information_schema.columns
where table_catalog = $1
and table_name = $2
and table_schema = 'public' `,
	},
}

//rowsQuery връща SELECT заявка
// добавя си и left join-ове, ако някоя колона е зададена с релация
//t = postgres|mysql
func rowsQuery(t string, columns Columns, searchable []string, table, search string, limit, ofset uint) (q string) {

	//пример SELECT visitlogs.browser, users.email FROM `visitlogs` LEFT JOIN `users` ON users.id = visitlogs.user_id

	//Знака за кавички
	escChar := getEscChar(t)

	//Частта от заявката с limit и offset
	limitQ := fmt.Sprintf("LIMIT %d OFFSET %d", limit, ofset)
	if limit == 0 {
		limitQ = ""
	}

	var relationQ string // заявка с LEFT JOIN-ове (ако има релации)

	//Прави масив с имената на колоните
	// Слага кавички и името на таблицата ("колона"."таблица")
	var cs []string
	for _, c := range columns {
		colName := fmt.Sprintf("%s.%s", addEscChars(table, escChar), addEscChars(c.Column, escChar))
		//Aко таблицата си има релация
		if c.Relation.RelationColumn != "" {
			s := "%s.%s AS \"%s\""
			if t == "postgres" {
				s = "%s.%s AS %s"
			}
			colName = fmt.Sprintf(s,
				addEscChars(c.Relation.RelationTable, escChar),
				addEscChars(c.Relation.ReplaceColumn, escChar),
				c.Column,
			)
			relationQ += fmt.Sprintf(" LEFT JOIN %s ON %s.%s=%s.%s ",
				addEscChars(c.Relation.RelationTable, escChar),
				addEscChars(c.Relation.RelationTable, escChar),
				addEscChars(c.Relation.RelationColumn, escChar),
				addEscChars(c.Relation.OriginTable, escChar),
				addEscChars(c.Relation.OriginColumn, escChar),
			)
		}
		cs = append(cs, colName)
	}

	//Клауза за търсене (WHERE LIKE %% заявка)
	searchQ := ""
	if search != "" {
		searchQ = "WHERE "
		for i, c := range searchable {
			searchQ += fmt.Sprintf("%s.%s LIKE  '%%%s%%' ", addEscChars(table, escChar), addEscChars(c, escChar), sqlEscape(search))
			if i != len(searchable)-1 {
				searchQ += " OR "
			}
		}
	}

	cols := strings.Join(cs, ",")
	q = fmt.Sprintf("SELECT %s FROM %s%s%s %s %s %s;",
		cols, escChar, sqlEscape(table), escChar, relationQ, searchQ, limitQ)

	return
}

//countQuery SELECT (count)  зявка
//t = type = "postgres|mysql"
func countQuery(t, table string) (q string) {
	q = fmt.Sprintf("SELECT COUNT(*) FROM %s;", addEscChars(table, getEscChar(t)))
	return
}

//updateQuery връща UPDATE, SELECT(count) и SELECT column заявки
//SELECT заявката връща броя редове, които биха се променили при викане на UPDATE заявката
//Използва се при редакция на данни в определена клетка
func updateQuery(t, table, column, newVal string, identifiers map[string]string) (q, countQ, selectQ string) {
	escChar := getEscChar(t)

	whereClause := "WHERE "
	i := 0
	for k, v := range identifiers {
		i++
		whereClause += fmt.Sprintf("%s='%s'",
			addEscChars(k, escChar), sqlEscape(v))
		if i < len(identifiers) {
			whereClause += " AND "
		}
	}

	q = fmt.Sprintf(`UPDATE %s 
	SET %s = '%s' %s`,
		addEscChars(table, escChar),
		addEscChars(column, escChar),
		sqlEscape(newVal),
		whereClause)

	countQ = fmt.Sprintf("SELECT count(*) FROM %s  %s",
		addEscChars(table, escChar), whereClause)

	selectQ = fmt.Sprintf("SELECT %s FROM %s %s",
		addEscChars(column, escChar),
		addEscChars(table, escChar),
		whereClause)

	// примерна заявка UPDATE `users` SET `email`=`test@gmail.com` WHERE `id`=2 AND `gender`=`male`
	//                 SELECT count(*) from users WHERE `id`=2 AND `gender`=`male`
	return
}

//checkColumnUniqueQuery проверява, дали дадена колона садържа само уникални стойности
func checkColumnUniqueQuery(t, table, column string) (q string) {

	table = addEscChars(table, getEscChar(t))
	column = addEscChars(column, getEscChar(t))

	q = fmt.Sprintf(`SELECT CASE WHEN count(distinct %s) = count(%s) 
	THEN 'unique' ELSE 'not unique' END
	FROM %s;`, column, column, table)

	return
}

//groupCountQuery връща SELECT COUNT(col), browser FROM tbl GROUP BY col
//използва се при статистиките (диаграмите)
//примерно върнат резултат:
//	+----------------+-----------------------+
//	| COUNT(browser) | browser               |
//	+----------------+-----------------------+
//	|              3 | Chrome (68.0.3440.59) |
//	|           3843 | Firefox (63.0)        |
//	|              1 | Opera                 |
//	+----------------+-----------------------+
func groupCountQuery(t, table, column string) (q string) {
	escC := getEscChar(t)
	table = addEscChars(sqlEscape(table), escC)
	column = addEscChars(sqlEscape(column), escC)
	q = fmt.Sprintf(`SELECT COUNT(%s), %s FROM %s GROUP BY %s`,
		column, column, table, column)

	return
}

//countTimeQuery връща
//SELECT COUNT(*) FROM table WHERE created_at_col > firstPeriod && created_at_col< secondPeriod
//UNION ALL
//SELECT COUNT(*) FROM table WHERE created_at_col > secondPeriod && created_at_col< thirdPeriod
//UNION ALL ...
//използва се при статистиките (диаграмите, за брой записи в зависимост от времето )
//примерно върнат резултат:
//	+----------------+
//	| COUNT(*) 		 |
//	+----------------+
//	|            100 |
//	|            500 |
//	|           1340 |
//	+----------------+
func countTimeQuery(t, table, createdAtCol string, firstP time.Time, pLength time.Duration, isTimestamp bool, total bool) (q string) {
	escC := getEscChar(t)
	table = addEscChars(sqlEscape(table), escC)
	createdAtCol = addEscChars(sqlEscape(createdAtCol), escC)

	selectClause := fmt.Sprintf("UNION ALL SELECT COUNT(%s) FROM %s ",
		createdAtCol, table)

	for i := firstP; i.Before(time.Now()); i = i.Add(pLength) {
		p := i.Add(pLength)

		var whereClause string
		if isTimestamp { //timestamp
			d := p.Unix()
			whereClause = fmt.Sprintf("WHERE %s < %d ", createdAtCol, d)
			if !total {
				d0 := p.Add(-pLength).Unix()
				whereClause = fmt.Sprintf("WHERE %s < %d AND %s > %d ",
					createdAtCol, d, createdAtCol, d0)
			}

		} else if t == "postgres" { //postgres
			d := p.Format("2006-01-02") // ГГГГ-ММ-ДД
			whereClause = fmt.Sprintf("WHERE %s < '%v'::date ", createdAtCol, d)
			if !total {
				d0 := p.Add(-pLength).Format("2006-01-02")
				whereClause = fmt.Sprintf("WHERE %s < '%v'::date AND %s > '%v'::date ",
					createdAtCol, d, createdAtCol, d0)
			}
		} else { //mysql
			d := p
			whereClause = fmt.Sprintf("WHERE %s < \"%v\" ", createdAtCol, d)
			if !total {
				d0 := p.Add(-pLength)
				whereClause = fmt.Sprintf("WHERE %s < \"%v\" AND %s > \"%v\" ",
					createdAtCol, d, createdAtCol, d0)
			}
		}
		q += selectClause + whereClause + "\n"

	}
	q = strings.Replace(q, "UNION ALL", "", 1)
	return
}

//тривиални имена на типовете данни
var dataTypes = map[string]map[string][]string{
	"mysql": {
		//! id  тата имат COLUMN_KEY = PRI
		"string":      {"varchar", "char"},
		"text":        {"text", "tinytext", "mediumtext", "longtext"},
		"time":        {"timestamp", "date", "datetime", "time", "year"},
		"binary":      {"binary", "varbinary", "tinyblob", "tinyblob", "blob", "longblob", "mediumblob"},
		"int":         {"bit", "int", "smallint", "bigint", "tinyint", "serial", "mediumint"},
		"float":       {"float", "real", "double", "decimal"},
		"geometry":    {"point", "geometry", "multilinestring", "linestring", "geometrycollection", "polygon"},
		"multi-value": {"set", "enum"},
	},
	"postgres": {
		"id":       {"uuid"},
		"string":   {"character", "macaddr", "cidr", "inet", "interval", "money", "pg_lsn"},
		"text":     {"text"},
		"time":     {"timestamp"},
		"binary":   {"bytea", "tsquery", "tsvector", "txid_snapshot"},
		"int":      {"bit", "integer", "smallint", "bigint", "bigserial", "serial"},
		"float":    {"real", "double", "money", "numeric"},
		"json":     {"json", "jsonb"},
		"geometry": {"point", "circle", "line", "lseg", "path", "polygon", "box"},
		"bool":     {"boolean"},
		"xml":      {"xml"},
	},
}

//Колони, които няма да е добра идея да бъдат редактирани
var readOnly = map[string][]string{
	"mysql": {
		"timestamp", "date", "datetime", "time", "year",
		"binary", "varbinary", "tinyblob", "tinyblob", "blob", "longblob", "mediumblob",
		"point", "geometry", "multilinestring", "linestring", "geometrycollection", "polygon",
	},
	"postgres": {
		"pg_lsn", "tsvector", "txid_snapshot",
		"macaddr", "cidr", "inet",
		"bytea", "bit", "uuid", "tsquery", "interval",
		"bigserial", "serial",
		"point", "circle", "line", "lseg", "path", "polygon", "box",
	},
}

//Колони, на които не може да се изпълни LIKE заявка
var unsearchable = map[string][]string{
	"mysql": {},
	"postgres": {
		"id", "time", "geometry", "bool", "xml", "json",
		"macaddr", "cidr", "inet", "tsquery", "interval", "money", "pg_lsn", "tsquery", "tsvector", "txid_snapshot",
		"binary", "float", "int",
	},
}

//за ескейпване на mysql/postgres string
func sqlEscape(value string) string {
	replace := map[string]string{"\\": "\\\\", "'": `\'`, "\\0": "\\\\0", "\n": "\\n", "\r": "\\r", `"`: `\"`, "\x1a": "\\Z"}

	for b, a := range replace {
		value = strings.Replace(value, b, a, -1)
	}

	return value
}

//Загражда имената на колони/таблици в "" или ``
func addEscChars(value, escChar string) string {
	return escChar + sqlEscape(value) + escChar
}

//getEscChar Връща правилните кавички
//t - синтаксиса = mysql|postgres
func getEscChar(t string) (c string) {
	c = "`"
	if t == "postgres" {
		c = "\""
	}
	return
}

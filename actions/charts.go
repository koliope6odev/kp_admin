package actions

//------------------------------------------
// Контролер, който се грижи за
// нащата свързани с статистиките,
// извлеяени на база данните в някоя таблица
//------------------------------------------

import (
	"bytes"
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gofrs/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

// ChartsCreateChart запазва диаграма
func ChartsCreateChart(con buffalo.Context) error {

	c, verrs, err := chartUpdateCreate(con, uuid.Nil)
	if verrs != nil && verrs.HasAny() {
		return con.Render(403, r.JSON(verrs))
	}
	if err != nil {
		return con.Render(406, r.String(err.Error()))
	}

	return con.Render(200, r.JSON(c.ID))
}

//ChartUpdate редакрира диаграма
func ChartUpdateChart(con buffalo.Context) error {
	chartId, err := uuid.FromString(con.Param("chart_id"))
	if err != nil {
		return errors.WithStack(err)
	}
	c, verrs, err := chartUpdateCreate(con, chartId)
	if verrs != nil && verrs.HasAny() {
		return con.Render(403, r.JSON(verrs))
	}
	if err != nil {
		return con.Render(406, r.String(err.Error()))
	}
	return con.Render(200, r.JSON(c))
}

//ChartsGetCharts връща диаграмите за даден сайт
func ChartsGetCharts(c buffalo.Context) error {
	tx := models.DB //c.Value("tx").(*pop.Connection)
	var cs models.Charts
	err := tx.Eager().Where("site_id = ?", c.Param("site_id")).All(&cs)
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(cs))
}

//Премахва диаграма
func ChartsDeleteChart(con buffalo.Context) error {
	tx := con.Value("tx").(*pop.Connection)
	var c models.Chart
	if err := tx.Where("site_id=?", con.Param("site_id")).Find(&c, con.Param("chart_id")); err != nil {
		return errors.WithStack(err)
	}
	if err := tx.Destroy(&c); err != nil {
		return errors.WithStack(err)
	}

	return con.Render(200, r.String("Successfully deleted"))
}

//ChartsGetChart Връща страницата за 1 диаграма
func ChartsGetChart(con buffalo.Context) error {
	tx := con.Value("tx").(*pop.Connection)
	var c models.Chart
	if err := tx.Where("site_id=?", con.Param("site_id")).Find(&c, con.Param("chart_id")); err != nil {
		return errors.WithStack(err)
	}
	con.Set("chart", c)
	con.Set("siteID", con.Param("site_id"))
	return con.Render(200, r.HTML("charts/chart.html"))
}

//GetDataWS връща данни и label-и за диаграма
//Работи с WebSocket-и
func GetDataWS(c buffalo.Context) error {
	fmt.Println("GetDataWS")
	tx := c.Value("tx").(*pop.Connection)

	//Диаграмата, за която да върне данни
	var cha models.Chart
	err := tx.Eager().Find(&cha, c.Param("chart_id"))
	if err != nil {
		return errors.WithStack(err)
	}

	w := c.Response()
	r := c.Request()

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return errors.WithStack(err)
	}
	defer conn.Close()
	for {
		_, r, err := conn.NextReader()
		if err != nil {
			return err
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(r)
		//Съобщението, което получва от клиента
		s := buf.String()
		//Aко получава подходящо запитване формат : getChartData
		reg, err := regexp.Compile("getChartData(.*)")
		if err != nil {
			fmt.Println(err.Error())
		}
		if reg.MatchString(s) {
			fmt.Println("MATCH")

			if cha.StatisticsType == "percentage" {
				ch := make(chan map[string]interface{})
				go cha.GetDataGroupBy(ch)

				for i := range ch {
					w, err := conn.NextWriter(1)
					if err != nil {
						return err
					}
					r, err := json.Marshal(i)
					if err != nil {
						return errors.WithStack(err)
					}
					w.Write([]byte(r))
					if err := w.Close(); err != nil {
						return errors.WithStack(err)
					}
				}
			} else if cha.StatisticsType == "countTime" {

				//проверява, дали идват правилните данни във options
				reg, _ = regexp.Compile("getChartData\\(Options=start:(\\d+);period:(\\d+);total:(\"true\"|\"false\")")
				if !reg.MatchString(s) {
					return errors.New("regex do not match")
				}

				//взима си данните от webSocket съобщението
				opt := strings.Split(s[20:len(s)-1], ";")                                //options
				sTimestamp, _ := strconv.ParseInt(strings.Split(opt[0], ":")[1], 10, 64) //начало (unix timestamp)
				start := time.Unix(sTimestamp, 0)                                        //начало
				period, _ := strconv.Atoi(strings.Split(opt[1], ":")[1])                 //дължината на периодите (в дни)

				ch := make(chan uint)

				firstP := start
				pLength := time.Duration(time.Duration(period) * time.Hour * 24) //през period дни
				total := strings.Split(opt[2], ":")[1] == "\"true\""

				go cha.GetDataCount(ch, firstP, pLength, total)
				for i := range ch {
					w, err := conn.NextWriter(1)
					if err != nil {
						return err
					}
					w.Write([]byte(strconv.Itoa(int(i))))
					if err := w.Close(); err != nil {
						return errors.WithStack(err)
					}
				}
			}
			//Съобщение за край
			w, err := conn.NextWriter(1)
			if err != nil {
				return err
			}
			w.Write([]byte("--END--"))
			if err := w.Close(); err != nil {
				return errors.WithStack(err)
			}
		}
	}
}

//updateCreate Логиката за създаване/редакция на диаграма (не export-ната функция)
func chartUpdateCreate(con buffalo.Context, updateId uuid.UUID) (models.Chart, *validate.Errors, error) {
	tx := models.DB //con.Value("tx").(*pop.Connection)
	var c models.Chart
	err := con.Bind(&c)

	if err != nil {
		return models.Chart{}, nil, errors.WithStack(err)
	}
	if c.DBID == uuid.Nil {
		return models.Chart{}, nil, errors.New("Няма зададена връзка към база данни")
	}

	c.SiteID, err = uuid.FromString(con.Param("site_id"))
	if err != nil {
		return models.Chart{}, nil, errors.WithStack(err)
	}

	tx.Find(&c.DB, c.DBID)

	ok, err := c.Check() //проверка, дали е възможна връзка към БД
	if !ok {
		return models.Chart{}, nil, errors.New(
			fmt.Sprintf("Невъзможно свързване - %s", err.Error()))
	}

	//Aко става въпрос за update-ване`
	var verrs *validate.Errors
	if updateId != uuid.Nil {
		c.ID = updateId
		verrs, err = c.Update(tx)
	} else {
		verrs, err = c.Create(tx)
	}

	if err != nil {
		return models.Chart{}, nil, errors.WithStack(err)
	}
	if verrs.HasAny() {
		return models.Chart{}, verrs, errors.New("validation errors")
	}
	return c, nil, nil
}

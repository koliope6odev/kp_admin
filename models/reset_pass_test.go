package models_test

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

//Проверява, дали хвърля грешка при запазване
func (ms *ModelSuite) TestResetPass() {

	u := models.User{}
	models.DB.Save(&u)

	psr := models.ResetPass{}
	psr.Token = "1234567ASDSADCs"
	psr.UserID = u.ID

	err := models.DB.Save(&psr)
	ms.NoError(err)
}

func (ms *ModelSuite) TestResetPass_Create() {
	u := models.User{}
	u.Email = nulls.NewString("test@email11.com")
	models.DB.Save(&u)

	psr := models.ResetPass{}
	psr.Token = "Nikola123455212"
	psr.User = u

	_, err := psr.Create(models.DB)
	ms.NoError(err)
	ms.Equal(u.ID, psr.UserID)
	ms.NotEqual(psr.ID, uuid.Nil)
}

func (ms *ModelSuite) TestResetPass_MaxRequestsReached() {
	u := models.User{}
	u.Email = nulls.NewString("test@email11.com")
	models.DB.Save(&u)

	psr := models.ResetPass{User: u}
	psr.GenerateToken()

	r, err := psr.MaxRequestsReached(models.DB)
	ms.NoError(err)
	ms.False(r)

	for i := 0; i < models.MaxRequests; i++ {
		psr := models.ResetPass{User: u}
		psr.GenerateToken()

		r, err := psr.MaxRequestsReached(models.DB)
		ms.NoError(err)
		ms.False(r)

		psr.Create(models.DB)
	}

	r, err = psr.MaxRequestsReached(models.DB)
	ms.NoError(err)
	ms.True(r)

}

func (ms *ModelSuite) TestResetPass_DestroyForUser() {
	u := models.User{}
	u.Email = nulls.NewString("test@email11.com")
	models.DB.Save(&u)

	for i := 1; i <= 10; i++ {
		psr := models.ResetPass{UserID: u.ID}
		psr.GenerateToken()
		err := models.DB.Create(&psr)
		ms.NoError(err)
	}
	var psrs []models.ResetPass

	c, err := models.DB.Where("user_id = ?", u.ID).Count(&psrs)
	ms.NoError(err)
	ms.Equal(10, c)

	psr := models.ResetPass{User: u}
	psr.DestroyForUser(models.DB)

	c, err = models.DB.Where("user_id = ?", u.ID).Count(&psrs)
	ms.NoError(err)
	ms.Equal(0, c)

}

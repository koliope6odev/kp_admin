package models_test

import (
	"testing"

	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func Test_Column(t *testing.T) {
	//t.Fatal("This test needs to be implemented!")
}

func (ms *ModelSuite) TestColumn_ValidateUpdate() {
	var c models.Column
	ms.LoadFixture("a column")
	ms.DB.Where("column_name = ?", "column1").First(&c)

	//Без промени
	verrs, err := c.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	//Твърде дълго име
	c.Verbose = nulls.NewString("123456789–123456789–123456789–123456789–123456789–123456789–")
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	c.Verbose = nulls.NewString("verbose")

	//Пробва се да смени оригиналния тип данни с несъществуващ
	c.DataType = "измислен–тип"
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	c.DataType = "string"

	//Пробва се да смени оригиналния тип данни
	c.DataTypeOriginal = "varchar 253"
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("data_type_original"), "Оригиналният тип данни не може да бъде сменен!")
	c.DataTypeOriginal = "varchar 255"

	//Пробва се да смени типа данни
	c.DataType = "json"
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("data_type"), "Типа данни не може да бъде сменен!")
	c.DataType = "string"

	//Пробва се да смени оригиналното read write
	c.DataTypeReadOnly = false
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("data_type_readonly"), "Оригиналните права за четене не могат да бъдат сменени!")
	c.DataTypeReadOnly = true

	//Пробва се да смени оригиналното име на колоната
	c.Column = "column####"
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("column"), "Оригиналното име на колоната не може да буде променяно!")
	c.Column = "column1"

	//Промяна  на правата за четене
	c.ReadOnly = false
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("readonly")[0], "Не може да променяте правата за писане на тази колона!")
	c.ReadOnly = true

	//Промяна на тривиалното име
	c.Verbose = nulls.NewString("verbose name")
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	var newCol models.Column
	ms.DB.Find(&newCol, c.ID)
	ms.Equal(nulls.NewString("verbose name"), newCol.Verbose)

	//Промяна на правата за писане
	var c2 models.Column
	ms.DB.Where("column_name = ?", "column1_readOnly_false").First(&c2)
	ms.NoError(ms.DB.Save(&c2))

	c2.ReadOnly = true
	verrs, err = c.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	ms.DB.Find(&newCol, c.ID)
	ms.Equal(true, newCol.ReadOnly)
}

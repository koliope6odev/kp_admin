package models

// ----------------------------------
//
//  Модел за сайтовете
//  Създаване/Редактиране на сайт
//  Добавяне/премахване на потребители
//
// ----------------------------------

import (
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gobuffalo/envy"

	"github.com/gobuffalo/buffalo/binding"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"github.com/pkg/errors"
)

type Site struct {
	ID        uuid.UUID `db:"id" json:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	//TODO::тука може би не трябва да са null.String a string
	Name  nulls.String `db:"name" form:"name" json:"name"`        //име на сайт
	Desc  nulls.String `db:"description" form:"desc" json:"desc"` //описание на сайт
	Link  nulls.String `db:"link" form:"link" json:"link"`        //домейн на сайта
	Owner User         `db:"-" json:"owner"`                      //собственик
	Users Users        `many_to_many:"sites_users" json:"users"`  //потребители с достъп към сайта

	Photo     nulls.String `json:"photo" db:"photo"`                      //файл снимка на сайта
	PhotoFile binding.File `json:"photo_file, omitempty" form:"-" db:"-"` //път към снимката във файловата система

	Invites Invites `has_many:"invites"` //покани за присъединяване към сайта

	OwnerId uuid.UUID `db:"owner_id" ` //id на собственика
}

type Sites []Site

type SitesUsers struct {
	ID        uuid.UUID `db:"id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`

	SiteId uuid.UUID `db:"site_id"` //id на сайта
	UserId uuid.UUID `db:"user_id"` //id на потребители

	Rights string `db:"rights"` //права на потребителя
}

type SitesUsersMulti []SitesUsers

func (s Site) String() string {
	ju, _ := json.Marshal(s)
	return string(ju)
}

// Create wrapper около логиката за създаване на сайт
func (s *Site) Create(tx *pop.Connection) (*validate.Errors, error) {
	s.OwnerId = s.Owner.ID
	return tx.ValidateAndCreate(s)
}

// Update wrapper около логиката за променяне на сайт
func (s *Site) Update(tx *pop.Connection) (*validate.Errors, error) {
	//Прави невъзможна промяната на owner_id
	var oldS Site
	if err := tx.Find(&oldS, s.ID); err != nil {
		return nil, errors.WithStack(err)
	}
	s.OwnerId = oldS.OwnerId
	return tx.ValidateAndUpdate(s)
}

// Изтриване на сайт
func (s *Site) DeleteSite(tx *pop.Connection) error {
	if err := tx.Destroy(s); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//Зарежда всички сайтове притежавани от един юзър
func (s *Sites) GetOwnedBy(user *User) error {
	var sites []Site
	if err := DB.Where("owner_id = ?", user.ID).All(&sites); err != nil {
		return errors.WithStack(err)
	}

	//Временна променлива
	ss := *s
	for _, site := range sites {
		site.Owner = *user
		ss = append(ss, site)
	}
	*s = ss

	return nil
}

//Валидации
func (s *Site) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		&validators.StringIsPresent{Field: s.Name.String, Name: "Name", Message: t("site.name")},
		&validators.StringLengthInRange{Field: s.Name.String, Name: "Name", Max: 50, Message: t("site.nameLong", map[string]interface{}{"max": 50})},
		&validators.StringIsPresent{Field: s.Desc.String, Name: "Description", Message: t("site.desc")},
		&validators.StringIsPresent{Field: s.Link.String, Name: "Link", Message: t("site.link")},
		&validators.FuncValidator{
			Field:   s.Link.String,
			Name:    "Link",
			Message: t("site.linkTaken"),
			Fn: func() bool {
				var b bool
				q := tx.Where("link = ?", s.Link.String)
				if s.ID != uuid.Nil {
					q = q.Where("id != ?", s.ID)
				}
				b, err = q.Exists(s)
				if err != nil {
					return false
				}
				return !b
			},
		},
		//Проверява дали въведеният домейн е валиден
		&validators.URLIsPresent{Name: "Link", Field: s.Link.String, Message: t("site.linkInvalid")},
		&validators.FuncValidator{
			Field:   s.Link.String,
			Name:    "Link",
			Message: t("site.linkInvalidFormat"),
			Fn: func() bool {
				_, err := url.ParseRequestURI(s.Link.String)
				if err != nil {
					return false
				}
				return true
			},
		},
		//Проверява дали въведеният домейн не е забранен
		&validators.FuncValidator{
			Field:   s.Link.String,
			Name:    "Link",
			Message: t("site.linkForbidden"),
			Fn: func() bool {
				domains := []string{"localhost", "localdomain", "127.0.0.1"}
				for _, d := range domains {
					if strings.Contains(s.Link.String, d) {
						return false
					}
				}
				return true
			},
		},
	), nil
}

//Качване на снимката за сайт
func (s *Site) SitePhotoUpload(tx *pop.Connection) error {
	if s.PhotoFile.Valid() {
		ENV := envy.Get("GO_ENV", "development")
		URL := envy.Get("UPLOADS_URL", "http://127.0.0.1:3000")

		name := fmt.Sprintf("%v%v", fmt.Sprintf("%v", time.Now().Unix()), s.PhotoFile.Filename)
		s.PhotoFile.Filename = name
		dir := filepath.Join(".", "assets/images/sitePhotos")

		if ENV == "production" {
			dir = filepath.Join(envy.Get("UPLOADS_PATH", "/var/www/kp_admin/sitePhotos"))
		}
		if err := os.MkdirAll(dir, 0777); err != nil {
			return errors.WithStack(err)
		}
		f, err := os.Create(filepath.Join(dir, s.PhotoFile.Filename))
		if err != nil {
			return errors.WithStack(err)
		}
		defer f.Close()
		_, err = io.Copy(f, s.PhotoFile)

		if err := os.Chmod(filepath.Join(dir, s.PhotoFile.Filename), 0777); err != nil {
			return errors.WithStack(err)
		}

		s.Photo = nulls.NewString(URL + "/" + name)
	}
	return nil
}

// Update wrapper около логиката за променяне на Потребител към сайт
func (su *SitesUsers) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(su)
}

//Зарежда всички сайтове на определен потребител
func (su *SitesUsers) GetRights(userId, siteId uuid.UUID) error {
	var ss SitesUsers
	if err := DB.Where("user_id = ?", userId).Where("site_id = ?", siteId).Last(&ss); err != nil {
		return errors.WithStack(err)
	}

	*su = ss

	return nil
}

//Премахва потребител от сайт
func (su *SitesUsers) RemoveUserFromSite(tx *pop.Connection) error {
	if err := tx.Destroy(su); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//Зарежда всички сайтове където потребител има определени права
func (su *SitesUsersMulti) GetByRights(tx *pop.Connection, userId uuid.UUID, rights string) error {
	q := "rights = ?" //заявката за where клаузата (моце да се смени от "=" нa "<>" (примерно ако дойде rights = "not_owner"))
	var sites []SitesUsers
	if strings.Contains(rights, "not_") {
		q = "rights <> ?"
		rights = strings.Replace(rights, "not_", "", 1)
	}

	if err := DB.Where("user_id = ?", userId).Where(q, rights).All(&sites); err != nil {
		return errors.WithStack(err)
	}
	susu := *su
	for _, site := range sites {
		susu = append(susu, site)
	}
	*su = susu

	return nil
}

//Зарежда всички потребители с еднакви права от един сайт
func (su *SitesUsersMulti) GetBySite(siteId uuid.UUID, rights string) error {
	var sites []SitesUsers
	if err := DB.Where("site_id = ?", siteId).Where("rights = ?", rights).All(&sites); err != nil {
		return errors.WithStack(err)
	}

	susu := *su
	for _, site := range sites {
		susu = append(susu, site)
	}
	*su = susu

	return nil
}

//Изтрива всички потребители от сайт
func (su *SitesUsersMulti) DeleteUsersWithSite(siteId uuid.UUID, tx *pop.Connection) error {
	var sites []SitesUsers
	if err := DB.Where("site_id = ?", siteId).All(&sites); err != nil {
		return errors.WithStack(err)
	}

	for _, site := range sites {
		if derr := tx.Destroy(&site); derr != nil {
			return errors.WithStack(derr)
		}
	}

	return nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (s *Site) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (s *Site) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// AfterFind
//Вика се след получаване на site от базата
func (s *Site) AfterFind() {

}

//RemoveSite премахва сайт от масива със сайтове !!!Не trigger-ва базата
func (s *Site) RemoveUser(u User) error {
	var newUs Users
	for _, e := range s.Users {
		if e.ID != u.ID {
			newUs = append(newUs, e)
		}
	}
	s.Users = newUs
	return nil
}

//GetByRights връща сайтовете,в които определен потребител има определени права
func (ss Sites) GetByRights(tx *pop.Connection, userId uuid.UUID, rights string) (Sites, error) {
	var sus SitesUsersMulti
	if err := sus.GetByRights(tx, userId, rights); err != nil {
		return nil, errors.WithStack(err)
	}
	sites := Sites{}
	if sus != nil {
		for _, su := range sus {
			site := Site{}
			if err := tx.Find(&site, su.SiteId); err != nil {
				return nil, errors.WithStack(err)
			}
			//Задава собственика
			if err := tx.Find(&site.Owner, site.OwnerId); err != nil {
				return nil, errors.WithStack(err)
			}
			sites = append(sites, site)
		}
	}

	return sites, nil
}

type SUser struct {
	//Структура, която се връща, когато се търсят всички потребители за даден сайт + техните права
	User
	Rights string `json:"rights" db:"-"`
}

func (u SUser) TableName() string {
	return "users"
}

func (s Site) GetUsers(tx *pop.Connection) ([]SUser, error) { //Връща потребителите за даден сайт (+правата им )
	var sus SitesUsersMulti
	var users []SUser
	err := tx.Where("site_id=?", s.ID).All(&sus)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	for _, su := range sus {
		var u SUser
		err = tx.Find(&u, su.UserId)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		u.Rights = su.Rights
		users = append(users, u)
	}
	return users, nil
}

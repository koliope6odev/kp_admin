package models_test

import (
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func (ms *ModelSuite) Test_Chart_GetDataGroupByP() {

	ms.LoadFixture("lots of fictive columns")

	dbc := models.DBConn{
		Driver:   "postgres",
		Host:     "127.0.0.1",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
	}

	c := models.Chart{
		DB:     dbc,
		Table:  "columns",
		Column: "data_type",
	}
	//Лабел-string
	ch := make(chan map[string]interface{})
	go c.GetDataGroupBy(ch)

	for i := range ch {
		ms.NotEmpty(i)
		switch i["label"] {
		case "time":
			ms.Equal(uint(1), i["data"])
		case "id":
			ms.Equal(uint(2), i["data"])
		case "string":
			ms.Equal(uint(3), i["data"])
		}
	}

	//Лабел bool
	c.Column = "read_only"

	chB := make(chan map[string]interface{})
	go c.GetDataGroupBy(chB)

	for i := range chB {
		switch i["label"] {
		case "true":
			ms.Equal(uint(4), i["data"])
		case "false":
			ms.Equal(uint(2), i["data"])
		}
	}
}

func (ms *ModelSuite) Test_Chart_GetDataGroupByM() {

	dbc := models.DBConn{
		Driver:   "mysql",
		Host:     "194.135.94.89",
		User:     "kp_admin",
		Password: "nikolape6o",
		DB:       "zora",
		Port:     3306,
	}

	c := models.Chart{
		DB:     dbc,
		Table:  "users",
		Column: "email",
	}
	//Лабел-string
	ch := make(chan map[string]interface{})
	go c.GetDataGroupBy(ch)

	for i := range ch {
		ms.NotEmpty(i)
		//fmt.Println()
		//fmt.Println(i["label"])
	}

}

func (ms *ModelSuite) Test_Chart_GetDataCount() {
	ms.NoError(ms.DB.Destroy(&models.Users{}))
	ms.LoadFixture("users for date counting")

	dbc := models.DBConn{
		Driver:   "postgres",
		Host:     "127.0.0.1",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
	}

	c := models.Chart{
		DB:          dbc,
		Table:       "users",
		Column:      "created_at",
		IsTimeStamp: nulls.NewBool(false),
	}
	//Postgres
	firstP := time.Date(2018, 11, 23, 12, 0, 0, 0, time.UTC)     // от 23/11/2018
	pLength := time.Duration(time.Duration(10) * time.Hour * 24) //през 10 дни

	//TOTAL = true
	ch := make(chan uint)
	go c.GetDataCount(ch, firstP, pLength, true)

	var co uint = 0 //counter
	var n uint = 0  // броя периоди
	for i := firstP; i.Before(time.Now()); i = i.Add(pLength) {
		n++
	}

	for i := range ch {
		co++
		if co < 8 { //първите 7 записа са направени по 1 в съответния период
			ms.Equal(co, i, "Грешен брой записи")
		} else if co < n { //ако не е последен запис
			ms.Equal(uint(7), i)
		} else {
			ms.Equal(uint(8), i)
		}
	}

	//TOTAL = false
	cht := make(chan uint)
	go c.GetDataCount(cht, firstP, pLength, false)

	co = 0
	for i := range cht {
		co++
		if co < 8 { //първите 7 записа са направени по 1 в съответния период
			ms.Equal(uint(1), i, "Грешен брой записи")
		} else if co < n { //ако не е последен запис
			ms.Equal(uint(0), i)
		} else {
			ms.Equal(uint(1), i)
		}
	}

}

func (ms *ModelSuite) Test_Chart_CheckP() {
	dbc := models.DBConn{
		Driver:   "postgres",
		Host:     "127.0.0.1",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
	}

	c := models.Chart{
		DB:     dbc,
		Table:  "columns",
		Column: "data_type",
	}

	ok, err := c.Check()
	ms.True(ok)
	ms.NoError(err)

	//Грешка в dbc-то
	c.DB.User = "stfan"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.DB.User = "postgres"

	//Грешна колона
	c.Column = "wtf"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.Column = "data_type"

	//Грешна таблица
	c.Column = "wtf_tbl"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.Column = "columns"

}

func (ms *ModelSuite) Test_Chart_CheckPCountTime() {
	dbc := models.DBConn{
		Driver:   "postgres",
		Host:     "127.0.0.1",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
	}

	c := models.Chart{
		DB:          dbc,
		Table:       "users",
		Column:      "created_at",
		IsTimeStamp: nulls.NewBool(false),
	}

	ok, err := c.Check()
	ms.True(ok)
	ms.NoError(err)

	//Грешка в dbc-то
	c.DB.User = "stfan"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.DB.User = "postgres"

	//Грешна колона
	c.Column = "wtf"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.Column = "data_type"

	//Грешна таблица
	c.Column = "wtf_tbl"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.Column = "columns"

}

func (ms *ModelSuite) Test_Chart_CheckM() {
	dbc := models.DBConn{
		Driver:   "mysql",
		Host:     "194.135.94.89",
		User:     "kp_admin",
		Password: "nikolape6o",
		DB:       "zora",
		Port:     3306,
	}

	c := models.Chart{
		DB:     dbc,
		Table:  "users",
		Column: "email",
	}

	ok, err := c.Check()
	ms.NoError(err)
	ms.True(ok)

	//Грешка в dbc-то
	c.DB.User = "stfan"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.DB.User = "postgres"

	//Грешна колона
	c.Column = "wtf"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.Column = "data_type"

	//Грешна таблица
	c.Column = "wtf_tbl"
	ok, err = c.Check()
	ms.False(ok)
	ms.Error(err)
	c.Column = "columns"

}

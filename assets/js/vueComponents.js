/*-------------------------------------------------------------
Всички Vue компоненти в приложението
 --------------------------------------------------------------*/

Vue.component('login', require('./components/Login.vue').default);
Vue.component('site-form', require('./components/Site/SiteForm.vue').default);
Vue.component('site-settings-form', require('./components/Site/Settings.vue').default);
Vue.component("site-form-field", require("./components/Site/Includes/SiteFormField.vue").default);
Vue.component("site-photo", require("./components/Site/Includes/SitePhotoUpload.vue").default);
Vue.component('reset-password', require('./components/ResetPass.vue').default);
Vue.component('spinner', require('./components/Spinner.vue').default);
Vue.component('my-sites', require('./components/Site/MySites.vue').default);
Vue.component('site-panel', require('./components/Site/SitePanel.vue').default);
Vue.component('site', require("./components/Site/Site.vue").default);
Vue.component('site-dbs', require("./components/Site/SiteDBs.vue").default);
Vue.component('db-table', require("./components/Site/DBTable/DBTable.vue").default);
Vue.component('db-table-pagination', require("./components/Site/DBTable/Pagination.vue").default);
Vue.component('db-table-chose-columns', require("./components/Site/DBTable/DBTableChoseColumns.vue").default);
Vue.component('db-table-access', require("./components/Site/DBTable/DBTableAccess.vue").default);
Vue.component('db-table-rename-cols', require("./components/Site/DBTable/DbTableRenameCols.vue").default);
Vue.component('db-table-edit-cell', require("./components/Site/DBTable/DBTableCellEdit.vue").default);
Vue.component('columns-select', require("./components/Site/DBTable/ColumnsSelect.vue").default);
Vue.component('table-settings-modal', require("./components/Site/DBTable/TableSettingsModal.vue").default);
Vue.component("table-logs", require("./components/Site/DBTable/TableLogs.vue").default);
Vue.component("logs-table", require("./components/Site/Includes/LogsTable.vue").default);
Vue.component("column-relations", require("./components/Site/DBTable/ColumnRelations.vue").default);
Vue.component("delete-relation-modal", require("./components/Site/DBTable/DeleteRelation.vue").default);
Vue.component('column-add-relation', require("./components/Site/DBTable/AddRelation.vue").default);

Vue.component("column-mutations", require("./components/Site/DBTable/Mutators.vue").default);
Vue.component("delete-mutation-modal", require("./components/Site/DBTable/DeleteMutation.vue").default);
Vue.component('column-add-mutation', require("./components/Site/DBTable/AddMutator.vue").default);

Vue.component("column-validations", require("./components/Site/DBTable/ColumnValidators.vue").default);
Vue.component("delete-validator-modal", require("./components/Site/DBTable/DeleteValidation.vue").default);
Vue.component('column-add-validator', require("./components/Site/DBTable/AddValidator.vue").default);

Vue.component('site-db-modal', require("./components/Site/DBModal.vue").default);
Vue.component('site-users', require("./components/Site/Users/SiteUsers.vue").default);
Vue.component('site-users-panel', require("./components/Site/Users/SiteUsersPanel.vue").default);
Vue.component('site-users-modal', require("./components/Site/Users/SiteUsersModal.vue").default);
Vue.component('site-side-bar', require("./components/Site/SiteSideBar.vue").default);
Vue.component('join-link', require("./components/Site/JoinSiteLink.vue").default);
Vue.component('invites', require("./components/User/Invites.vue").default);
Vue.component('profile-table', require("./components/User/Profile.vue").default);
Vue.component('profile-form', require("./components/User/ProfileForm.vue").default);
Vue.component('password-form', require("./components/User/PasswordForm.vue").default);
Vue.component('file-upload', require("./components/User/FileUpload.vue").default);
Vue.component('charts', require("./components/Site/Chart/Charts.vue").default);
Vue.component('chart-modal', require("./components/Site/Chart/ChartModal.vue").default);
Vue.component('edit-skills', require("./components/User/EditSkills.vue").default);
Vue.component('chart', require("./components/Chart/Chart.vue").default);

Vue.component("email-sending", require("./components/Site/Emails/SendEmail.vue").default);

Vue.component('bug-report', require("./components/BugReport/BugReport.vue").default);

Vue.component('manual', require("./components/Manual/Manual.vue").default);


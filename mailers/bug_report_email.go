package mailers

//--------------------------------------------------
// mailer, грижест се за преащането на bug report-и
//--------------------------------------------------

import (
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

func SendBugReportEmail(br models.Report) error {
	m := mail.NewMessage()
	m.Subject = "Bug report"
	m.From = envy.Get("SMTP_SENDER", "support@bananamin.site")
	m.To = []string{"support@bananamain.site", "pepotov@gmail.com", "koliodev@protonmail.com"}

	// Data that will be used inside the templates when rendering.
	data := map[string]interface{}{
		"br": br,
	}

	err := m.AddBody(r.HTML("bug_report_email.html"), data)
	if err != nil {
		return errors.WithStack(err)
	}
	m.Bodies[0].ContentType = "text/html"

	return smtp.Send(m)
}

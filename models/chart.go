package models

// ----------------------------------
//
//  Модел за диаграмите, статистиките
//
// ----------------------------------

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/validate/validators"

	"github.com/pkg/errors"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

type Chart struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Verbose     string       `json:"name" db:"verbose_name"` //името на диаграмта
	Table       string       `json:"table" db:"table_name"`  //таблицата, от която се извличат данни
	Column      string       `json:"column" db:"column_name"`
	Column2     nulls.String `json:"column_2" db:"second_column"`
	IsTimeStamp nulls.Bool   `json:"is_timestamp" db:"is_timestamp"` //в случаите, когато вида е countTime, дали column2  е timestamp

	Type           string `json:"type" db:"type"`                       //вида на диаграмата (bar/line/radar/doughnut/pie/polar)
	StatisticsType string `json:"statistics_type" db:"statistics_type"` //вида на данните (percentage/countTime/)

	DB   DBConn    `json:"db"  belongs_to:"database"`
	DBID uuid.UUID `json:"db_id" db:"dbid"`

	SiteID uuid.UUID `json:"site_id" db:"site_id"`
}

// Create wrapper около логиката за създаване на диаграма
func (c *Chart) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(c)
}

//Update wrapper около логиката за създаване на диаграма
func (c *Chart) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(c)
}

//Check проверява, дали е възможна заявка
func (c Chart) Check() (bool, error) {
	ok, err := c.DB.CheckConnection()
	if !ok {
		return ok, err
	}
	//Проверява дали може да направи SELECT GROUP BY ИЛИ SELECT COUNT UNION
	q := groupCountQuery(c.DB.Driver, c.Table, c.Column)
	if c.StatisticsType == "countTime" {
		firstP := time.Date(2018, 11, 23, 12, 0, 0, 0, time.UTC)     // от 23/11/2018
		pLength := time.Duration(time.Duration(10) * time.Hour * 24) //през 10 дни
		countTimeQuery(c.DB.Driver, c.Table, c.Column, firstP, pLength, c.IsTimeStamp.Bool, true)
	}
	db, _ := sql.Open(c.DB.Driver, c.DB.getConnString())
	defer db.Close()
	rows, err := db.Query(q)
	if err != nil {
		return false, err
	}
	defer rows.Close()
	err = rows.Err()
	if err != nil {
		return false, err
	}

	return true, err
}

//GetDataGroupBy Връща данни за визуализация на диаграма
//с  group by клауза (percentage)
//примет : видовете браузъри във visit log-а
//праща Labels и data  във chanel-a
// {label:l,data:d}
func (c Chart) GetDataGroupBy(ch chan map[string]interface{}) error {
	q := groupCountQuery(c.DB.Driver, c.Table, c.Column)
	db, _ := sql.Open(c.DB.Driver, c.DB.getConnString())
	defer db.Close()
	rows, err := db.Query(q)
	if err != nil {
		return errors.WithStack(err)
	}

	for rows.Next() {
		var count uint
		var label string
		err = rows.Scan(&count, &label)
		if err != nil {
			fmt.Println(err)
			continue
		}
		ch <- map[string]interface{}{
			"label": label,
			"data":  count,
		}
	}

	close(ch)
	if rows.Err() != nil {
		return rows.Err()
	}
	defer rows.Close()

	return nil
}

//GetDataCount връща данните за диграмата
//която показва броя на редовете
//в зависимост от time stamp-a (created_at)
//Връща във формат [uint, uint, uint ...]
// 12  (за първи период)
// 34  (за втори период)
//  ... ...............
// firstP - кога да е началото на първия период
// pLength - по колко да продължава един период
// total - дали да върне колко записа са създадени
//  ДО определения период или В определения период
//  Пример: създадени записи от 10 до 20 февруари - 20
//		   създадени записи от 20 до 29 февруари - 10
//		   при total=true се връща : 20, 30
//		   при total=false - 20, 10
func (c Chart) GetDataCount(ch chan uint, firstP time.Time, pLength time.Duration, total bool) error {
	q := countTimeQuery(c.DB.Driver, c.Table, c.Column, firstP, pLength, c.IsTimeStamp.Bool, total)

	db, _ := sql.Open(c.DB.Driver, c.DB.getConnString())
	rows, err := db.Query(q)
	if err != nil {
		fmt.Println(err.Error())
		return errors.WithStack(err)
	}
	for rows.Next() {
		var count uint
		err = rows.Scan(&count)
		if err != nil {
			fmt.Println(err)
			continue
		}
		ch <- count
	}

	close(ch)
	if rows.Err() != nil {
		return rows.Err()
	}
	defer rows.Close()
	defer db.Close()
	return nil
}

// String is not required by pop and may be deleted
func (c Chart) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// Charts is not required by pop and may be deleted
type Charts []Chart

// String is not required by pop and may be deleted
func (c Charts) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (c *Chart) Validate(tx *pop.Connection) (*validate.Errors, error) {

	vals := []validate.Validator{
		&validators.StringIsPresent{Name: "name", Field: c.Verbose, Message: t("chart.nameRequired")},
		&validators.StringLengthInRange{Name: "name", Field: c.Verbose, Min: 2, Max: 50, Message: t("chart.nameInRange", map[string]interface{}{
			"max": 50, "min": 2,
		})},
		&validators.StringIsPresent{Name: "type", Field: c.Type},
		&validators.StringIsPresent{Name: "statistics_type", Field: c.StatisticsType},
		&validators.StringInclusion{Name: "statistics_type", Field: c.StatisticsType,
			List: []string{"percentage", "countTime"},
		},
		&validators.StringInclusion{Name: "function_name", Field: c.Type,
			List: []string{"bar", "line", "radar", "doughnut", "pie", "polarArea"},
		},
		&validators.UUIDIsPresent{Name: "site_id", Field: c.SiteID},
	}

	return validate.Validate(vals...), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (c *Chart) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (c *Chart) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

package actions

import (
	"fmt"
	"os"

	"github.com/gobuffalo/httptest"
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/ulule/deepcopier"
)

//Проверява дали страницата за формата
//за нов сайт се зарежда правилно
func (as *ActionSuite) Test_SitesGetSiteForm() {

	//Ако НЕ е логнат
	res := as.HTML("/site/new").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	res = as.HTML("/site/new").Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(200, res.Code)

	as.Contains(res.Body.String(), "</site-form>")

}

//Проверява формата за създаване на
//на нов сайт работи правилно
func (as *ActionSuite) Test_SitesCreateSite() {
	s := &models.Site{}
	//Ако НЕ е логнат
	res := as.HTML("/site/new").Put(s)
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	//празно поле за име
	s1 := models.Site{}
	deepcopier.Copy(s).To(s1)
	s1.Name = nulls.NewString("")
	res = as.HTML("/site/new").Put(&s1)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за име не може да бъде празно")

	//твърде дълго име
	s6 := models.Site{}
	deepcopier.Copy(s).To(s6)
	s6.Name = nulls.NewString("NameTooLongNameTooLongNameTooLongNameTooLongNameTooLong")
	s6.Owner = *u
	res = as.HTML("/site/new").Put(s6)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Името не може да бъде по-дълго от 50 символа")

	//празно поле за описание
	s2 := models.Site{}
	deepcopier.Copy(s).To(s2)
	s2.Name = nulls.NewString("Name")
	s2.Desc = nulls.NewString("")
	s2.Owner = *u
	res = as.HTML("/site/new").Put(s2)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за описание не може да бъде празно")

	//празно поле за линк
	s3 := models.Site{}
	deepcopier.Copy(s).To(s3)
	s3.Name = nulls.NewString("Name")
	s3.Desc = nulls.NewString("Description")
	s3.Link = nulls.NewString("")
	s3.Owner = *u
	res = as.HTML("/site/new").Put(s3)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за линк не може да бъде празно")

	//невалиден link
	s7 := models.Site{}
	deepcopier.Copy(s).To(s7)
	s7.Name = nulls.NewString("Name")
	s7.Desc = nulls.NewString("Description")
	s7.Link = nulls.NewString("link")
	s7.Owner = *u
	res = as.HTML("/site/new").Put(s7)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "трябва да е валиден адрес")

	//повече от 1 грешка
	s4 := models.Site{}
	s4.Owner = *u
	res = as.HTML("/site/new").Put(s4)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за име не може да бъде празно")
	as.Contains(res.Body.String(), "Полето за описание не може да бъде празно")
	as.Contains(res.Body.String(), "Полето за линк не може да бъде празно")

	//зает линк
	site := &models.Site{
		Name: nulls.NewString("Test_SitesCreateSite"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}
	var s5 models.Site
	s5 = *site
	err = as.DB.Create(site)
	as.NoError(err)
	s5.Owner = *u
	res = as.HTML("/site/new").Put(s5)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Сайт с този линк - Link вече съществува")
	res = nil

	s.Link = nulls.NewString("https://link.testtttt")
	res = as.HTML("/site/new").Put(s)
	//as.Fail(res.Body.String())
	as.Equal(302, res.Code)
	as.Equal(fmt.Sprintf("/site/all/"), res.Header().Get("Location"))
	res = nil
}

//Проверява дали страницата за всички сайтове
//на потребител се зарежда правилно
func (as *ActionSuite) Test_SitesGetAllSites() {
	//Ако НЕ е логнат
	res := as.HTML("/site/all/").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")
	s.OwnerId = s.Owner.ID

	as.DB.Create(s)
	res = as.HTML("/site/new").Put(s)

	var sites models.Sites

	err = sites.GetOwnedBy(u)
	as.NoError(err)

	for _, site := range sites {
		as.Equal(site.OwnerId, u.ID)
	}

	res = as.HTML(fmt.Sprintf("/site/all/")).Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</my-sites>")
}

func (as *ActionSuite) Test_SitesGetSite() {
	//Ако НЕ е логнат
	res := as.HTML("/site/ididididid").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")
	s.Owner = *u
	s.OwnerId = s.Owner.ID

	as.DB.Create(s)

	res = as.HTML("/site/new").Put(s)
	su := &models.SitesUsers{
		UserId: u.ID,
		SiteId: s.ID,
		Rights: "owner",
	}
	as.DB.Create(su)
	res = as.HTML(fmt.Sprintf("/site/%v", s.ID)).Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), s.Name.String)
}

//Проверява дали правилно се редактира
//определен сайт
func (as *ActionSuite) Test_SitesEditSite() {
	//Ако НЕ е логнат
	s := &models.Site{}
	res := as.JSON("/site/ididid").Put(s)
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)
	u.JoinSite(s, as.DB, "admin")

	site_id := s.ID

	//празно поле за име
	s1 := models.Site{}
	deepcopier.Copy(s).To(s1)
	s1.Name = nulls.NewString("")
	s1.Owner = *u
	s1.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%s/", site_id)).Put(s1)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за линк не може да бъде празно")

	//твърде дълго име
	s6 := models.Site{}
	deepcopier.Copy(s).To(s6)
	s6.Name = nulls.NewString("NameTooLongNameTooLongNameTooLongNameTooLongNameTooLong")
	s6.Owner = *u
	s6.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s6)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Името не може да бъде по-дълго от 50 символа")

	//празно поле за описание
	s2 := models.Site{}
	deepcopier.Copy(s).To(s2)
	s2.Name = nulls.NewString("Name")
	s2.Desc = nulls.NewString("")
	s2.Owner = *u
	s2.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s2)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за описание не може да бъде празно")

	//празно поле за линк
	s3 := models.Site{}
	deepcopier.Copy(s).To(s3)
	s3.Name = nulls.NewString("Name")
	s3.Desc = nulls.NewString("Description")
	s3.Link = nulls.NewString("")
	s3.Owner = *u
	s3.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s3)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за линк не може да бъде празно")

	//невалиден link
	s7 := models.Site{}
	deepcopier.Copy(s).To(s7)
	s7.Name = nulls.NewString("Name")
	s7.Desc = nulls.NewString("Description")
	s7.Link = nulls.NewString("link")
	s7.Owner = *u
	s7.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s7)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "трябва да е валиден адрес")

	//повече от 1 грешка
	s4 := models.Site{}
	s4.Owner = *u
	s4.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s4)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Полето за име не може да бъде празно")
	as.Contains(res.Body.String(), "Полето за описание не може да бъде празно")
	as.Contains(res.Body.String(), "Полето за линк не може да бъде празно")

	//зает линк
	site := &models.Site{
		Name: nulls.NewString("Name"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}
	var s5 models.Site
	s5 = *site
	err = as.DB.Create(site)
	as.NoError(err)
	s5.Owner = *u
	s5.ID = site_id
	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s5)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Сайт с този линк - Link вече съществува")
	res = nil

	//Всичко наред
	s_edited := models.Site{}
	deepcopier.Copy(s).To(s_edited)
	s_edited.Owner = *u
	s_edited.Name = nulls.NewString("Name")
	s_edited.Desc = nulls.NewString("Description")
	s_edited.Link = nulls.NewString("https://new_link.bg")
	s_edited.ID = site_id

	res = as.JSON(fmt.Sprintf("/site/%v", site_id)).Put(s_edited)
	as.Equal(200, res.Code)
	res = nil
}

//Тества генерирането на линк
func (as *ActionSuite) Test_SitesGenerateLink() {
	res := as.HTML("/site/get/link/ididid").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err := as.DB.Create(s)
	as.NoError(err)

	site_id := s.ID

	res = as.HTML(fmt.Sprintf("/site/get/link/%v", site_id)).Get()
	as.Equal(302, res.Code)
	res = nil
}

//Тества страницата за присъединяване
func (as *ActionSuite) Test_SitesGetLinkPage() {
	//Ако НЕ е логнат
	s := &models.Site{}
	res := as.HTML("/site/join/128421").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	site_id := s.ID

	link := &models.SiteWorkerLink{}
	link.StringLink = nulls.NewString("ABCDEFG")
	link.SiteId = site_id

	err = as.DB.Create(link)
	as.NoError(err)

	res = as.HTML("/site/join/ABCDEFG").Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</join-link>")

	data := make(map[string]string)
	data["invite_id"] = "false"

	res = as.HTML("/site/join/%v", site_id).Put(data)
	as.Equal(200, res.Code)

	res = as.HTML("/site/join/ABCDEFG").Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(302, res.Code)
	as.Contains(res.Body.String(), fmt.Sprintf("/site/%v", site_id))
}

//Тест за присъединяването към сайт
func (as *ActionSuite) Test_SitesJoinSite() {
	//Ако НЕ е логнат
	s := &models.Site{}
	res := as.HTML("/site/join/128421").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	site_id := s.ID

	link := &models.SiteWorkerLink{}
	link.StringLink = nulls.NewString("ABCDEFG")
	link.SiteId = site_id

	err = as.DB.Create(link)
	as.NoError(err)

	data := make(map[string]string)
	data["invite_id"] = "non-valid"

	res = as.HTML("/site/join/%v", site_id).Put(data)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Няма такава покана")

	data1 := make(map[string]string)
	data1["invite_id"] = "false"

	res1 := as.HTML("/site/join/%v", site_id).Put(data1)
	as.Equal(200, res1.Code)

	res2 := as.HTML("/site/join/%v", site_id).Put(data1)
	as.Equal(302, res2.Code)

	u1 := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.co"),
	}
	err = as.DB.Create(u1)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u1.ID)

	i := &models.Invite{
		SiteID: s.ID,
		UserID: u1.ID,
	}

	err = as.DB.Create(i)
	as.NoError(err)

	data2 := make(map[string]string)
	data2["invite_id"] = fmt.Sprintf("%v", i.ID)

	res = as.HTML("/site/join/%v", site_id).Put(data2)
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), fmt.Sprintf("/site/%v", s.ID))
	res = nil
}

//Тест за премахването на юзър
func (as *ActionSuite) Test_SitesRemoveUser() {
	//Ако НЕ е логнат
	s := &models.Site{}
	res := as.HTML("/site/join/128421").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	u1 := &models.User{
		Name: nulls.NewString("Колио1"),

		Email: nulls.NewString("test1@gmail.test"),
	}
	err := as.DB.Create(u)
	err = as.DB.Create(u1)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u1.ID)

	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	site_id := s.ID

	su := &models.SitesUsers{
		SiteId: site_id,
		UserId: u1.ID,
		Rights: "read",
	}
	su1 := &models.SitesUsers{
		SiteId: site_id,
		UserId: u.ID,
		Rights: "owner",
	}

	err = as.DB.Create(su)
	err = as.DB.Create(su1)
	as.NoError(err)

	data := make(map[string]string)
	data["user_id"] = fmt.Sprintf("%v", u1.ID)
	data["rights"] = "admin"

	res = as.HTML("/site/remove/user/%v", site_id).Put(data)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Трябва да сте")

	as.Session.Set("current_user_id", u.ID)

	res = as.HTML("/site/remove/user/%v", site_id).Put(data)
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), fmt.Sprintf("%v", u1.ID))
	res = nil
}

//Тест за смяна на правата
func (as *ActionSuite) Test_SitesChangeRights() {
	//Ако НЕ е логнат
	s := &models.Site{}
	data2 := make(map[string]string)
	res := as.HTML("/site/change/rights/128421").Put(data2)
	as.Equal(302, res.Code)

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	u1 := &models.User{
		Name: nulls.NewString("Колио1"),

		Email: nulls.NewString("test1@gmail.test"),
	}
	err := as.DB.Create(u)
	err = as.DB.Create(u1)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	site_id := s.ID

	su := &models.SitesUsers{
		SiteId: site_id,
		UserId: u.ID,
		Rights: "rw",
	}

	su1 := &models.SitesUsers{
		SiteId: site_id,
		UserId: u1.ID,
		Rights: "read",
	}

	err = as.DB.Create(su)
	err = as.DB.Create(su1)
	as.NoError(err)

	data := make(map[string]string)
	data["user_id"] = fmt.Sprintf("%v", u1.ID)
	data["rights"] = "admin"

	res = as.HTML("/site/change/rights/%v", site_id).Put(data)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Трябва да сте")

	su.Rights = "owner"
	err = as.DB.Save(su)
	as.NoError(err)

	data1 := make(map[string]string)
	data1["user_id"] = fmt.Sprintf("%v", u1.ID)
	data1["rights"] = "admin"

	res = as.HTML("/site/change/rights/%v", site_id).Put(data1)
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Променен")
}

//Тест за изтриването на сайт
func (as *ActionSuite) Test_SitesDeleteSite() {
	res := as.HTML("/site/delete/1213").Delete()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	u1 := &models.User{
		Name: nulls.NewString("Колио1"),

		Email: nulls.NewString("test1@gmail.test"),
	}
	err = as.DB.Create(u1)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")
	s.OwnerId = u.ID

	err = as.DB.Create(s)
	as.NoError(err)

	su := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u.ID,
		Rights: "owner",
	}

	su1 := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u1.ID,
		Rights: "read",
	}

	err = as.DB.Create(su)
	err = as.DB.Create(su1)
	as.NoError(err)

	res = as.HTML("/site/delete/%v", s.ID).Delete()
	as.Equal(200, res.Code)

	as.Equal(res.Body.String(), "/site/all")
}

func (as *ActionSuite) Test_SitesUploadPhoto() {
	s1 := &models.Site{}
	res := as.HTML("/user/file/128421").Post(s1)
	as.Equal(302, res.Code)

	u := models.User{
		Name:         nulls.NewString("Kolio"),
		Email:        nulls.NewString("test@gmail.test"),
		PasswordHash: "PasswordHash",
	}
	err := as.DB.Create(&u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	su := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u.ID,
		Rights: "owner",
	}

	err = as.DB.Create(su)
	as.NoError(err)

	r, err := os.Open("../assets/images/test_upload/kolio_test.jpg")
	as.NoError(err)

	photo := httptest.File{
		// ParamName is the name of the form parameter
		ParamName: "photo_file",
		// FileName is the name of the file being uploaded
		FileName: "PhotoTest",
		Reader:   r,
	}

	res1, err1 := as.HTML(fmt.Sprintf("/site/photo/%v", s.ID)).MultiPartPost(u, photo)
	as.NoError(err1)
	as.Equal(302, res1.Code)
	as.Equal(fmt.Sprintf("/site/%v", s.ID), res1.Header().Get("Location"))
}

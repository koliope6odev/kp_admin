package models_test

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (ms *ModelSuite) Test_EditLogCreate() {

	randID, _ := uuid.FromString("fb8dd5c0-b619-4903-91ca-a42fd2c19357") //случайно ID
	el := &models.EditLog{
		OldVal:   "oldVal",
		NewVal:   "newVal",
		PKColumn: "id",
		PKValue:  "2",
		Column: models.Column{
			Column: "username",
			ID:     randID,
		},
		User: models.User{
			ID: randID,
		},
	}

	//Напълно валидно
	ms.DBDelta(1, "edit_logs", func() {
		verrs, err := el.Create(models.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny())
	})

	//---Проверява валидациите--

	//Празно pk value
	el.PKValue = ""
	verrs, err := el.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	el.PKValue = "1"

	//празно pk column
	el.PKColumn = ""
	verrs, err = el.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	el.PKColumn = "id"

	//липсващ потребител
	el.User = models.User{}
	verrs, err = el.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	el.User = models.User{ID: randID}

	//липсваща колона
	el.Column = models.Column{}
	verrs, err = el.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

}

func (ms *ModelSuite) Test_EditLogCheckPKColumn() {
	ms.LoadFixture("lots of users")
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
		Site:     s,
		Tables: models.Tables{
			{Name: "users", Access: "rw"},
		},
	}

	_, err := dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)

	t := dbc.Tables[0]

	ms.Equal(dbc0.ID, t.DB.ID)

	//----------------ЕЙ ОТ ТУКА ПОЧВА ИСТИНСКИЯТ ТЕСТ ГОРНОТО БЕ ГЛАВНО ИНИЦИАЛИЗАЦИЯ НА СРЕДАТА --------------------//
	//Ще променим email-a на потребител от базата
	var user models.User
	ms.NoError(ms.DB.First(&user))
	c := models.Column{
		Table: t,
	}

	el := models.EditLog{
		Column:   c,
		PKColumn: "id",
	}

	// --- ако има грешкри в изпълнението на заявката (примерно несъществуваща колона) --- //
	el.PKColumn = "invalid_col"
	ok, err := el.CheckPKColumn()
	ms.Error(err)
	ms.False(ok)
	ms.Contains(err.Error(), "does not exist")

	// --- ако колоната има неуникални стойности --- //
	el.PKColumn = "email"
	ok, err = el.CheckPKColumn()
	ms.NoError(err)
	ms.False(ok)

	// --- ако колоната си е уникална--- //
	el.PKColumn = "id"
	ok, err = el.CheckPKColumn()
	ms.NoError(err)
	ms.True(ok)

}

func (ms *ModelSuite) Test_EditLogFindLogs() {
	ms.NoError(ms.DB.Destroy(models.EditLogs{}))
	ms.LoadFixture("lots of fictive edit logs")
	var c models.Column
	ms.NoError(ms.DB.Where("column_name=?", "editLogColumn").First(&c))

	//ID - число
	var els models.EditLogs
	err := els.FindLogs(ms.DB, c, "id", "8")
	ms.NoError(err)
	ms.Equal(4, len(els))

	//ID - uuid
	els = models.EditLogs{}
	err = els.FindLogs(ms.DB, c, "id", "4460972f-13f5-4c4a-b5e2-71d62038b47a")
	ms.NoError(err)
	ms.Equal(4, len(els))
	ms.NoError(ms.DB.Destroy(models.EditLogs{}))
}

func (ms *ModelSuite) Test_EditLogTableLogs() {
	els0 := models.EditLogs{}
	ms.NoError(ms.DB.All(&els0))
	ms.NoError(ms.DB.Destroy(&els0))
	ms.LoadFixture("lots of fictive edit logs")
	var c models.Column
	ms.NoError(ms.DB.Where("column_name=?", "editLogColumn").First(&c))

	var t models.Table
	ms.NoError(ms.DB.Where("name=?", "editLogTable").First(&t))

	var els models.EditLogs
	ms.NoError(els.TableLogs(ms.DB, t))
	ms.Equal(8, len(els))
	ms.NoError(ms.DB.Destroy(models.EditLogs{}))
}

package actions

import (
	// "fmt"
	"os"

	"github.com/gobuffalo/httptest"
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/ulule/deepcopier"
)

//Тест за зареждането на страницата за бъг репорт
func (as *ActionSuite) Test_GetReportForm() {
	//Ако НЕ е логнат
	res := as.HTML("/report/").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	res = as.HTML("/report/").Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(200, res.Code)

	as.Contains(res.Body.String(), "</bug-report>")
}

//Тест за създаването на бъг репорт
func (as *ActionSuite) Test_PostReportForm() {
	r := &models.Report{}
	//Ако НЕ е логнат
	res := as.HTML("/report").Post(r)
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	r.Os = "Ososos"
	r.OsVersion = "OsVersion"
	r.Browser = "Browser"
	r.Ip = "IP address"
	r.Email = u.Email.String
	r.Comment = "CommentCommentComment"

	file, err := os.Open("../assets/images/test_upload/kolio_test.jpg")
	as.NoError(err)

	screenshot := httptest.File{
		// ParamName is the name of the form parameter
		ParamName: "screenshot_file",
		// FileName is the name of the file being uploaded
		FileName: "ScreenshotTest",
		Reader:   file,
	}

	r1 := &models.Report{}
	deepcopier.Copy(r).To(r1)
	r1.Os = ""
	res1, _ := as.HTML("/report/").MultiPartPost(r1, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Полето за операционна система не може да бъде празно")

	r2 := &models.Report{}
	deepcopier.Copy(r).To(r2)
	r2.Os = "OsOSOSOSOSOSOSOSOSOSOSOSOSSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOOSOSOSOSOSOSOSOSOSOSOSOSSOOSSOOSOSOSOSOS"
	res1, _ = as.HTML("/report/").MultiPartPost(r2, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Текстът в полето за операционна система не може да бъде по-дълъг от 50 символа.")

	r3 := &models.Report{}
	deepcopier.Copy(r).To(r3)
	r3.OsVersion = ""
	res1, _ = as.HTML("/report/").MultiPartPost(r3, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Полето за версия не може да бъде празно")

	r4 := &models.Report{}
	deepcopier.Copy(r).To(r4)
	r4.OsVersion = "OsOSOSOSOSOSOSOSOSOSOSOSOSSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOOSOSOSOSOSOSOSOSOSOSOSOSSOOSSOOSOSOSOSOS"
	res1, _ = as.HTML("/report/").MultiPartPost(r4, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Текстът в полето за версия на ОС не може да бъде по-дълъг от 50 символа.")

	r5 := &models.Report{}
	deepcopier.Copy(r).To(r5)
	r5.Browser = ""
	res1, _ = as.HTML("/report/").MultiPartPost(r5, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Полето за браузър не може да бъде празно")

	r6 := &models.Report{}
	deepcopier.Copy(r).To(r6)
	r6.Browser = "OsOSOSOSOSOSOSOSOSOSOSOSOSSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOOSOSOSOSOSOSOSOSOSOSOSOSSOOSSOOSOSOSOSOS"
	res1, _ = as.HTML("/report/").MultiPartPost(r6, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Текстът в полето за браузър не може да бъде по-дълъг от 50 символа.")

	r7 := &models.Report{}
	deepcopier.Copy(r).To(r7)
	r7.Ip = ""
	res1, _ = as.HTML("/report/").MultiPartPost(r7, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Полето за IP не може да бъде празно")

	r8 := &models.Report{}
	deepcopier.Copy(r).To(r8)
	r8.Ip = "OsOSOSOSOSOSOSOSOSOSOSOSOSSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOOSOSOSOSOSOSOSOSOSOSOSOSSOOSSOOSOSOSOSOS"
	res1, _ = as.HTML("/report/").MultiPartPost(r8, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Текстът в полето за IP не може да бъде по-дълъг от 50 символа.")

	r9 := &models.Report{}
	deepcopier.Copy(r).To(r9)
	r9.Email = ""
	res1, _ = as.HTML("/report/").MultiPartPost(r9, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Полето за e-mail не може да бъде празно")

	r10 := &models.Report{}
	deepcopier.Copy(r).To(r10)
	r10.Email = "OsOSOSOSOSOSOSOSOSOSOSOSOSSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOSOOSOSOSOSOSOSOSOSOSOSOSOSSOOSSOOSOSOSOSOS"
	res1, _ = as.HTML("/report/").MultiPartPost(r10, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Текстът в полето за e-mail не може да бъде по-дълъг от 50 символа.")

	r11 := &models.Report{}
	deepcopier.Copy(r).To(r11)
	r11.Comment = ""
	res1, _ = as.HTML("/report/").MultiPartPost(r11, screenshot)
	as.Equal(403, res1.Code)
	as.Contains(res1.Body.String(), "Полето за описание на проблема не може да бъде празно.")

	res1, _ = as.HTML("/report/").MultiPartPost(r, screenshot)
	as.Equal(302, res1.Code)
	as.Equal("/home", res1.Header().Get("Location"))
}

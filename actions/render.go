package actions

//------------------------------------------------------
// Грижи се за компилирането на .html  plush темплейтите
//-------------------------------------------------------

import (
	"html/template"

	"github.com/gobuffalo/buffalo/render"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/packr"
)

var r *render.Engine
var assetsBox = packr.NewBox("../public")

func init() {
	r = render.New(render.Options{
		// HTML layout to be used for all HTML requests:
		HTMLLayout: "layouts/application.html",

		// Box containing all of the templates:
		TemplatesBox: packr.NewBox("../templates"),
		AssetsBox:    assetsBox,

		// Add template helpers here:
		Helpers: render.Helpers{
			//Прави input hidden поле против csdf
			"csrf": func() template.HTML {
				return template.HTML("<input name=\"authenticity_token\" value=\"<%= authenticity_token %>\" type=\"hidden\">")
			},
			//проверява, дали тойноста на string e празна
			"null": func(s nulls.String) bool {
				if s.String == "" {
					return true
				}
				return false
			},
		},
	})

}

package actions

//-------------------------------------------------------------
// Контролер, който се грижи за
// нащата свързани с колоните на таблиците
// добавяне, преименуване, изтриване на колони
// +добавяне/премахване на функции за мутация/валидация/релация
// !връщане на данните във таблицата (летят към клиента чрез WS)
// edit log-овете
//--------------------------------------------------------------

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gofrs/uuid"
	"github.com/gorilla/websocket"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// ColumnGetColumns връща списък на всички колони в дадена таблица
func ColumnGetColumns(c buffalo.Context) error {
	var cols models.Columns
	var tbl models.Table
	c.Bind(&tbl)

	//проверка дали всичко е наред с таблицата
	err := checkTbl(&tbl, models.DB)
	if err != nil {
		return c.Render(406, r.JSON(err.Error()))
	}

	cols, err = tbl.GetColumns()
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(cols))
}

//Запазва колоните за дадена таблица в базата ни данни
func ColumnCreateColumns(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	var tbl models.Table
	c.Bind(&tbl)

	//проверка дали всичко е наред с таблицата
	err := checkTbl(&tbl, models.DB)
	if err != nil {
		return c.Render(406, r.JSON(err.Error()))
	}

	//Проверка, дали чвече няма запазвни колони
	cols, err := tbl.GetSavedColumns(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if len(cols) != 0 {
		return c.Render(406, r.JSON(T.Translate(c, "site.hasColumns")))
	}

	//Проверка, дали са избрани колони
	if len(tbl.Columns) == 0 {
		return c.Render(403, r.JSON(T.Translate(c, "site.noColumns")))
	}

	if err := tbl.SaveColumns(tx); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON("Columns saved!"))
}

//Ъпдейтва колоните за дадена таблица в базата ни данни
func ColumnUpdateColumns(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	var tbl models.Table
	c.Bind(&tbl)

	//проверка дали всичко е наред с таблицата
	err := checkTbl(&tbl, models.DB)
	if err != nil {
		return c.Render(406, r.JSON(err.Error()))
	}

	//Проверка, дали са избрани колони
	if len(tbl.Columns) == 0 {
		return c.Render(403, r.JSON(T.Translate(c, "site.noColumns")))
	}

	if err := tbl.UpdateColumns(tx); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON("Columns updated!"))
}

//ColumnGetSavedColumns връща колоните избрани от даден потребител за дадена таблица
func ColumnGetSavedColumns(c buffalo.Context) error {

	tx := c.Value("tx").(*pop.Connection)
	var tbl models.Table
	c.Bind(&tbl)

	err := checkTbl(&tbl, models.DB)
	if err != nil {
		return c.Render(406, r.JSON(err.Error()))
	}

	cols, err := tbl.GetSavedColumns(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.JSON(cols))
}

//ColumnGetRowWS Връща ред от някоя таблица  (работи с web socket-и)
func ColumnGetRowWS(c buffalo.Context) error {

	tx := c.Value("tx").(*pop.Connection)

	//Таблицта, за която да върне редове
	var tbl models.Table
	err := tbl.Find(tx, c.Param("table_id"))
	if err != nil {
		return errors.WithStack(err)
	}

	//проверка дали всичко е наред с таблицата
	err = checkTbl(&tbl, tx)
	if err != nil {
		return c.Render(406, r.JSON(err.Error()))
	}

	w := c.Response()
	r := c.Request()

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return errors.WithStack(err)
	}
	defer conn.Close()

	for {
		_, r, err := conn.NextReader()
		if err != nil {
			return err
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(r)
		//Съобщението, което получва от клиента
		s := buf.String()

		//Aко получава подходящо запитване формат : rowsOffset:n;Limit:n
		reg, _ := regexp.Compile("rows=Offset:(\\d+);Limit:(\\d+);Search:\"([\\s\\S]*)\"")

		if reg.MatchString(s) {
			fmt.Println("MATCH")
			c := make(chan []string)
			//сетъпвам офсет ,лимит и клауза за търсене
			a := strings.Split(s, ";")
			offset, _ := strconv.Atoi(a[0][12:])
			limit, _ := strconv.Atoi(a[1][6:])
			searchable := a[2][8:]
			searchable = searchable[:len(searchable)-1]

			//
			go tbl.Rows(c, searchable, uint(offset), uint(limit))
			for i := range c {
				w, err := conn.NextWriter(1)
				if err != nil {
					return err
				}
				r, err := json.Marshal(i)
				if err != nil {
					return errors.WithStack(err)
				}
				w.Write([]byte(r))
				if err := w.Close(); err != nil {
					return errors.WithStack(err)
				}
			}
		}
	}

}

//ColumnCountRows Връща боря редов в някоя таблица на отдалечения сървър
func ColumnCountRows(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)

	//Таблицта, за която да върне броя редове
	var tbl models.Table
	c.Bind(&tbl)

	err := checkTbl(&tbl, models.DB)
	if err != nil {
		return c.Render(406, r.JSON(err.Error()))
	}

	count, err := tbl.Count(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.JSON(count))
}

//ColumnUpdate редактира колона
// сменя тривиалното име или правата за четене/писане
func ColumnUpdate(c buffalo.Context) error {
	var col models.Column
	c.Bind(&col)
	tx := c.Value("tx").(*pop.Connection)
	verrs, err := col.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.String("OK"))
}

//ColumnAddRelation Добавя релация към някоя колона
func ColumnAddRelation(c buffalo.Context) error {
	tx := models.DB             // c.Value("tx").(*pop.Connection)
	cID := c.Param("column_id") //id-то на колоната (взима го от url-a )
	//Проверява, дали има добро column_id
	if _, err := uuid.FromString(cID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var col models.Column
	if err := tx.Find(&col, cID); err != nil {
		return c.Render(406, r.String("wrong column"))
	}
	var rel models.Relation
	if err := c.Bind(&rel); err != nil {
		return errors.WithStack(err)
	}
	rel.ColumnID = col.ID
	//Дали всички полета са попълнени
	verrs, _ := rel.Validate(tx)
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	dbc, err := col.GetDB(tx)

	if err != nil {
		return errors.WithStack(err)
	}
	//Дали е възможна SELECT .. JOIN LEFT заяка
	ok, err := rel.Check(dbc)
	if !ok {
		return c.Render(422, r.JSON(err.Error()))
	}
	//Запазва данните в базата ни
	if err := tx.Create(&rel); err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.JSON(rel.ID))
}

//ColumnDeleteRelation премахва релацията, прикачена за някоя колона
func ColumnDeleteRelation(c buffalo.Context) error {
	tx := models.DB // c.Value("tx").(*pop.Connection)
	cID := c.Param("column_id")
	//Проверява, дали има добро column_id
	if _, err := uuid.FromString(cID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var col models.Column
	if err := tx.Find(&col, cID); err != nil {
		return c.Render(406, r.String("wrong column"))
	}
	var rel models.Relation
	if exists, _ := tx.Where("column_id= ?", cID).Exists(&rel); !exists {
		return c.Render(406, r.String("wrong column"))
	}
	tx.Where("column_id= ?", cID).First(&rel)
	if err := tx.Destroy(&rel); err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.String("successfully deleted"))
}

//ColumnAddMutation Добавя функция за мутация към някоя колона
func ColumnAddMutation(c buffalo.Context) error {
	tx := models.DB // c.Value("tx").(*pop.Connection)
	cID := c.Param("column_id")
	//Проверява, дали има добро column_id
	if _, err := uuid.FromString(cID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var col models.Column
	if err := tx.Find(&col, cID); err != nil {
		return c.Render(406, r.String("wrong column"))
	}

	var mf models.MutationFunc
	if err := c.Bind(&mf); err != nil {
		return errors.WithStack(err)
	}
	mf.ColumnID = col.ID

	//Запазва данните в базата ни
	verrs, err := mf.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}

	return c.Render(200, r.JSON(mf.ID))
}

//ColumnDeleteMutation премахва мутация, прикачена за някоя колона
func ColumnDeleteMutation(c buffalo.Context) error {
	tx := models.DB // c.Value("tx").(*pop.Connection)
	cID := c.Param("column_id")
	//Проверява, дали има добро column_id
	if _, err := uuid.FromString(cID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var col models.Column
	if err := tx.Find(&col, cID); err != nil {
		return c.Render(406, r.String("wrong column"))
	}
	var mf models.MutationFunc
	if exists, _ := tx.Where("column_id= ?", cID).Exists(&mf); !exists {
		return c.Render(406, r.String("wrong column"))
	}
	tx.Where("column_id= ?", cID).First(&mf)
	if err := tx.Destroy(&mf); err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.String("successfully deleted"))
}

//ColumnAddValidation Добавя функция за валидация към някоя колона
func ColumnAddValidation(c buffalo.Context) error {
	tx := models.DB // c.Value("tx").(*pop.Connection)
	cID := c.Param("column_id")
	//Проверява, дали има добро column_id
	if _, err := uuid.FromString(cID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var col models.Column
	if err := tx.Find(&col, cID); err != nil {
		return c.Render(406, r.String("wrong column"))
	}

	var v models.CellEditValidationRule
	if err := c.Bind(&v); err != nil {
		return errors.WithStack(err)
	}
	v.ColumnID = col.ID

	//Прави float64 параметрите на int
	for k, i := range v.Params {
		if reflect.TypeOf(i).String() == "float64" {
			v.Params[k] = int(i.(float64))
		}
	}

	//Прави timeString параметрите на time

	if len(v.Params) == 1 && reflect.TypeOf(v.Params[0]).String() == "string" {
		v.Params[0], _ = time.Parse("2006-01-02", v.Params[0].(string))
	}

	//Запазва данните в базата ни
	verrs, err := v.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}

	return c.Render(200, r.JSON(v.ID))
}

//ColumnDeleteValidation функция за валидация, прикачена за някоя колона
func ColumnDeleteValidation(c buffalo.Context) error {

	tx := models.DB // c.Value("tx").(*pop.Connection)
	cID := c.Param("column_id")
	//Проверява, дали има добро column_id
	if _, err := uuid.FromString(cID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var col models.Column
	if err := tx.Find(&col, cID); err != nil {
		return c.Render(406, r.String("wrong column"))
	}

	//Проверява, дали има добро validation_id
	vID := c.Param("validation_id")
	if _, err := uuid.FromString(vID); err != nil {
		return c.Render(406, r.String("invalid id"))
	}
	var v models.CellEditValidationRule
	if err := tx.Find(&v, vID); err != nil {
		return c.Render(406, r.String("wrong validator id"))
	}

	if err := tx.Destroy(&v); err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.String("successfully deleted"))
}

//checkTbl проверява, дали всичко е наред с таблица (дали има зададено ID и DB.ID)
func checkTbl(tbl *models.Table, tx *pop.Connection) error {
	//Aко има зададено DBID, но няма зададено DB
	if tbl.DBID != uuid.Nil && tbl.DB.ID == uuid.Nil {
		tx.Find(&tbl.DB, tbl.DBID)
	}

	//Проверка, дали е зададена таблица
	if tbl.ID == uuid.Nil || tbl.DB.ID == uuid.Nil {
		return errors.New("Wrong table")
	}
	return nil
}

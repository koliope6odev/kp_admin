package actions

//-----------------------------------------------------------
// Контролер, който се грижи за
// нащата свързани връзките за бази данни
// добавяне/изтриване/редакция
// проверяване, дали е възможна връзка към отдалечения сървър
//-----------------------------------------------------------

import (
	"fmt"
	"github.com/gobuffalo/validate"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

//DBConnCheckConn проверява, дали е възможна връзка с дадена база
func DBConnCheckConn(c buffalo.Context) error {
	_, ok, verrs, err := dbCheckConn(c, "")
	if verrs != nil && verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	if !ok {
		return c.Render(422, r.JSON(err.Error()))
	}
	return c.Render(200, r.JSON("connection established"))

}

//GetTables връща всички таблици в отдалечена БД
func GetTables(c buffalo.Context) error {
	dbc, ok, verrs, err := dbCheckConn(c, "create")
	if verrs != nil && verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	if !ok {
		return err
	}
	tables, err := dbc.GetTables()
	if err != nil {
		return c.Render(422, r.JSON(err.Error()))
	}
	return c.Render(200, r.JSON(tables))
}

//CreateConn запазва връзка към БД в нашата база
func CreateConn(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	dbc, verrs, err := dbCreateUpdate(c)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs != nil && verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	verrs, err = dbc.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	return c.Render(200, r.JSON(dbc.ID))
}

//UpdateConn променя връзка към БД в нашата база
func UpdateConn(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	dbc, verrs, err := dbCreateUpdate(c)
	if verrs != nil && verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	if err != nil {
		return errors.WithStack(err)
	}
	verrs, err = dbc.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}
	return c.Render(200, r.JSON(dbc.ID))
	return nil
}

//GetConns връща връзките за свързване към БД за определен сайт
func GetConns(c buffalo.Context) error {
	tx := models.DB
	sId, err := uuid.FromString(c.Param("site_id"))
	if err != nil {
		return errors.WithStack(err)
	}
	s := models.Site{ID: sId}

	dbcs := models.DBConns{}
	dbcs.GetConns(tx, s)
	return c.Render(200, r.JSON(dbcs))
}

//DelConn изтрива връзка към БД от нашата база
func DelConn(c buffalo.Context) error {
	tx := models.DB //c.Value("tx").(*pop.Connection)
	id, e := uuid.FromString(c.Param("conn_id"))
	if e != nil {
		return errors.WithStack(e)
	}

	siteId, e := uuid.FromString(c.Param("site_id"))
	if e != nil {
		return errors.WithStack(e)
	}

	dbc := models.DBConn{}

	if err := tx.Find(&dbc, id); err != nil {
		return errors.WithStack(e)
	}

	if siteId != dbc.SiteID {
		return c.Render(406, r.String(T.Translate(c, "deleteErr")))
	}

	e = tx.Destroy(&dbc)
	if e != nil {
		return errors.WithStack(e)
	}
	return c.Render(200, r.JSON("OK"))
}

//DBTableCheckConn Проверява, дали е възможна връзка към дадена таблица
func DBTableCheckConn(c buffalo.Context) error {
	tx := models.DB
	var t models.Table

	err := tx.Find(&t, c.Param("table_id"))
	if err != nil {
		return errors.WithStack(err)
	}
	err = tx.Find(&t.DB, t.DBID)
	if err != nil {
		return errors.WithStack(err)
	}

	ok, err := t.CheckConn(tx)
	if !ok {
		fmt.Println(err.Error())
		return c.Render(422, r.JSON(err.Error()))
	}
	return c.Render(200, r.JSON("connection established"))

}

//private функцийка, която проверява дали е възможно свързването с база данни
func dbCheckConn(c buffalo.Context, createUpdate string) (*models.DBConn, bool, *validate.Errors, error) {
	//Взима връзка към базата
	tx := c.Value("tx").(*pop.Connection)

	//намира db connection на база request-a
	dbc := &models.DBConn{}
	c.Bind(dbc)
	if dbc.ID != uuid.Nil {
		createUpdate = "update"
	}

	//id-то на сайта (от url-a)
	sId, err := uuid.FromString(c.Param("site_id"))
	if err == nil {
		dbc.Site = models.Site{ID: sId}
	}

	//Проверява, дали всичко е ОК
	verrs, err := dbc.Validate(tx)
	//Допълнителна валидация при create/update
	verrs2 := validate.NewErrors()
	if createUpdate == "create" {
		verrs2, err = dbc.ValidateCreate(tx)
	} else if createUpdate == "update" {
		verrs2, err = dbc.ValidateUpdate(tx)
	}
	if err != nil {
		return dbc, false, nil, errors.WithStack(err)
	}
	verrs.Errors = mergeMaps(verrs.Errors, verrs2.Errors)

	if err != nil {
		return dbc, false, nil, errors.WithStack(err)
	}
	if verrs.HasAny() {
		return dbc, false, verrs, nil
	}

	ok, err := dbc.CheckConnection()
	if err != nil {
		return dbc, false, nil, err
	}
	if !ok {
		return dbc, false, nil, errors.New("cannot establish connection: unknown reason")
	}
	return dbc, true, nil, nil

}

//Логиката за създаване / редакция на връзка към база данни
func dbCreateUpdate(c buffalo.Context) (*models.DBConn, *validate.Errors, error) {
	//tx := c.Value("tx").(*pop.Connection)
	tx := models.DB

	dbc, ok, verrs, err := dbCheckConn(c, "update")
	if verrs != nil && verrs.HasAny() {
		return nil, verrs, nil
	}
	if !ok || err != nil {
		return nil, nil, errors.New("error - dbCreateUpdate")
	}
	//За site_id колоната
	s := &models.Site{}
	if err := tx.Find(s, c.Param("site_id")); err != nil {
		return nil, nil, errors.WithStack(err)
	}

	dbc.Site = *s
	//За таблиците
	req := c.Request()
	req.ParseForm()
	var tables models.Tables
	var i uint = 0
	for {
		t := req.PostFormValue(fmt.Sprintf("tables[%d]", i))
		if t == "" {
			break
		}
		tables = append(tables, models.Table{Name: t, Access: "rw"})
		i++
	}
	dbc.Tables = tables
	return dbc, nil, nil
}

//mergeMaps прави два map-a в един
//ползва се там където трябва да се свържат два map-a със валидационни грешки
func mergeMaps(maps ...map[string][]string) map[string][]string {
	result := make(map[string][]string)
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

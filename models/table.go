package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/validate/validators"
	"github.com/pkg/errors"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	_ "github.com/lib/pq"
)

//---------------------------------------------------
//
//     Модел за таблиците във отдалечените БД
//
//---------------------------------------------------

type Table struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Name     string       `db:"name" json:"name"`
	Verbose  nulls.String `json:"verbose" db:"verbose_name"`
	Access   string       `db:"access" json:"access"` //read || read write
	DBID     uuid.UUID    `db:"db" json:"dbid"`
	DB       DBConn       `db:"-" json:"-"`
	Columns  Columns      `db:"-" json:"columns"`
	PkColumn nulls.String `db:"pk_column" json:"pk_column"` //privary key колоната за таблицата
}

// String is not required by pop and may be deleted
func (t Table) String() string {
	ju, _ := json.Marshal(t)
	return string(ju)
}

// Tables is not required by pop and may be deleted
type Tables []Table

//GetColumns всички имена на колони в тази таблица (прави връзка към отдалечения сървър)
func (t *Table) GetColumns() (Columns, error) {
	return t.DB.GetColumns(t.Name)
}

//GetSavedColumns връща колоните, които са запазени в нашата база
func (t *Table) GetSavedColumns(tx *pop.Connection) (Columns, error) {
	var cols Columns
	if err := tx.Where("table_id = ?", t.ID).Order("created_at DESC").All(&cols); err != nil {
		return nil, errors.WithStack(err)
	}
	return cols, nil
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (tbl *Table) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		&validators.StringLengthInRange{
			Field: tbl.Verbose.String, Name: "verbose", Min: 2, Max: 155, Message: t("table.nameInRange", map[string]interface{}{
				"min": 2,
				"max": 155,
			})},
		// check to see if the email address is already taken:
		&validators.FuncValidator{
			Field:   tbl.Verbose.String,
			Name:    "verbose",
			Message: t("table.nameUsedAsDBName"),
			Fn: func() bool {
				var b bool
				q := tx.Where("verbose_name = ?", tbl.Verbose.String).Where("db = ?", tbl.DBID)
				if tbl.ID != uuid.Nil {
					q = q.Where("id != ?", tbl.ID)
				}
				b, err = q.Exists(tbl)
				if err != nil {
					return false
				}
				return !b
			},
		},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (t *Table) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (t *Table) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(), nil
}

//SaveColumns запазва колоните в  базата ни
func (t *Table) SaveColumns(tx *pop.Connection) error {
	var cols Columns
	for _, e := range t.Columns {
		e.Table = *t
		if err := tx.Save(&e); err != nil {
			return errors.WithStack(err)
		}
		cols = append(cols, e)
	}

	t.Columns = cols
	return nil
}

//UpdateColumns добавя/изтрива колони
func (t *Table) UpdateColumns(tx *pop.Connection) error {
	tx = DB
	//таблиците, които вече са запазени
	var savedCols Columns
	tx.Where("table_id = ?", t.ID).All(&savedCols)
	//таблиците, които не са запазени
	var cols Columns
	for _, c := range t.Columns {
		//Проверява дали  е някое от savedTbls
		if !columnIn(c, savedCols) {
			cols = append(cols, c)
		}
	}

	//таблиците, които трябва да изтрие
	var toDelCols Columns
	for _, c := range savedCols {
		if !columnIn(c, t.Columns) {
			toDelCols = append(toDelCols, c)
		}
	}

	if err := tx.Destroy(&toDelCols); err != nil {
		return errors.WithStack(err)
	}

	//Запазване на relate-натите колони
	for _, c := range cols {
		c.Table = *t
		if err := tx.Create(&c); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

//Find намира таблица по id, като и задава и db connection
func (t *Table) Find(tx *pop.Connection, id interface{}) error {
	//Търси таблицата по id
	e := tx.Find(t, id)
	if e != nil {
		return errors.WithStack(e)
	}

	//Намира връзката и към БД
	var dbc DBConn
	e = tx.Find(&dbc, t.DBID)
	if e != nil {
		return errors.WithStack(e)
	}
	dbc.Tables = Tables{}
	//Задава DBConn на таблицата
	t.DB = dbc
	return nil
}

//Rename сменя verbose името
func (t *Table) Rename(tx *pop.Connection, verbose string) (*validate.Errors, error) {
	t.Verbose = nulls.NewString(verbose)
	return tx.ValidateAndUpdate(t)
}

func (t *Table) CheckConn(tx *pop.Connection) (bool, error) {
	//CheckConnection проверява, дали може да се свърже
	ok, err := t.DB.CheckConnection()
	if !ok {
		return ok, err
	}
	//Отваря връзка към отдалечна БД
	db, _ := sql.Open(t.DB.Driver, t.DB.getConnString())
	defer db.Close()

	//Проверява дали може да направи SELECT
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM %s LIMIT 1", sqlEscape(t.Name)))
	if err != nil {
		return false, err
	}
	defer rows.Close()
	err = rows.Err()
	if err != nil {
		return false, err
	}
	return true, nil
}

//Rows връща редовете в  таблицата
func (t *Table) Rows(c chan []string, search string, offsets ...uint) error {

	//Offset и limit за paginate-ването
	var offset uint = 0
	var limit uint = 0

	//Aко реално има зададени offset-и (за paginate-ването)
	if len(offsets) == 2 {
		offset = offsets[0]
		limit = offsets[1]
	}

	//Отваря връзка към отдалечена БД
	db, _ := sql.Open(t.DB.Driver, t.DB.getConnString())
	defer db.Close()

	//Имената на Колоните, за които МОЖЕ да се извика SQL оператор LIKE
	var searchableCols []string

	for _, c := range t.Columns {
		nonS := false //дали е nonSearchable
		for _, e := range unsearchable[t.DB.Driver] {
			if e == c.DataType || strings.Contains(c.DataTypeOriginal, e) {
				nonS = true
				break
			}
		}
		if !nonS {
			searchableCols = append(searchableCols, c.Column)
		}
	}

	//Изпълнява SELECT заявка
	rows, err := db.Query(rowsQuery(t.DB.Driver, t.Columns, searchableCols, t.Name, search, limit, offset))

	if err != nil {
		return errors.WithStack(err)
	}

	//Празен Интерфейс за стойностите, които ще дойдат (защото не знаем какви типове и колко са на брой колоните)
	vals := make([]interface{}, len(t.Columns))
	//vals = {
	//  колона1 : {[]red1{колона1, колона2},[]red2{колона1, колона2}  }
	// }
	for i := 0; i < len(t.Columns); i++ {
		vals[i] = new(interface{})
	}

	//Върти през редовете, получени от SELECT заявката
	for rows.Next() {
		err = rows.Scan(vals...)
		if err != nil {
			fmt.Println(err)
			continue
		}
		//Данните в този ред
		var vs []string

		//Върти през колоните, за да попълни данните в този ред
		for k, v := range vals {
			//Стринговата репрезентация
			vString := getValue(v.(*interface{}))
			//Aко има функция за стрингова промяна
			if t.Columns[k].MutationsFunc.ID != uuid.Nil {
				vString = t.Columns[k].MutationsFunc.Mutate(vString)
			}

			//попълва реда
			vs = append(vs, vString)

		}
		//Праща в chanel-a
		c <- vs
	}
	//Затваря chanel-a
	close(c)
	if rows.Err() != nil {
		return rows.Err()
	}
	defer rows.Close()

	return nil
}

//GetCellValue Връща данните oт дадена клетка
func (t *Table) GetCellValue(column Column, identifiers map[string]string) (string, error) {
	//Ако няма identifier-и за WHERE частта
	if len(identifiers) == 0 {
		return "", errors.New("no identifiers specified")
	}

	//SELECT заяката
	_, countQ, q := updateQuery(t.DB.Driver, t.Name, column.Column, "", identifiers)

	fmt.Println(countQ)

	//Отваря връзка към отдалечена БД
	db, _ := sql.Open(t.DB.Driver, t.DB.getConnString())
	defer db.Close()

	//Проверка, дали identifier-ите отговарят точно на един ред от базата
	r := db.QueryRow(countQ)
	var affectedRows uint8 //броя редове, които би се променил

	err := r.Scan(&affectedRows)
	if err != nil {
		return "", errors.WithStack(err)
	}

	//Ако няма да промени точно един ред
	if affectedRows != 1 {
		return "", errors.New("invalid identifiers")
	}

	//изпълнява SELECT заяката
	r = db.QueryRow(q)
	var val string //данните в клетката
	err = r.Scan(&val)
	if err != nil {
		return "", errors.WithStack(err)
	}

	//Aко има функция за мутация
	if column.MutationsFunc.ID != uuid.Nil {
		val = column.MutationsFunc.Mutate(val)
	}

	return val, nil
}

//EditCell Променя данните в някоя клетка
//identifier - колони и стойности, по които да направи where claus-a
//пример {id:1, email:"Nikola"}
func (t *Table) EditCell(column Column, newVal string, identifiers map[string]string) error {

	//проверка, дали колоната е readonly
	if column.ReadOnly || column.DataTypeReadOnly {
		return errors.New("cannot edit readonly column")
	}

	//проверка, ако колоната има зададени валидации
	for _, v := range column.ValidationRules {
		if err := v.ValidateCell(newVal); err != nil {
			return err
		}
	}

	//Проверка, дали има функция за промяна, ако има я обръща
	if column.MutationsFunc.ID != uuid.Nil {
		newVal = column.MutationsFunc.MutateReverse(newVal)
	}

	//Проверка дали вида на клетка не е bool (трябва да направи "Bool(False)"->0 и "Bool(True)"->1)
	if column.DataType == "bool" {
		column.MutationsFunc.FuncName = "int2bool"
		newVal = column.MutationsFunc.MutateReverse(newVal)
	}

	//Ако няма identifier-и за WHERE частта
	if len(identifiers) == 0 {
		return errors.New("no identifiers specified")
	}

	//Отваря връзка към отдалечена БД
	db, _ := sql.Open(t.DB.Driver, t.DB.getConnString())
	defer db.Close()

	//UPDATE и  SELECT count(*) заявки
	q, countQ, _ := updateQuery(t.DB.Driver, t.Name, column.Column, newVal, identifiers)

	//Проверка дали ще affect-не само 1 ред
	r := db.QueryRow(countQ)
	var affectedRows uint8 //броя редове, които би се променил

	err := r.Scan(&affectedRows)
	if err != nil {
		return errors.WithStack(err)
	}

	//Ако няма да промени точно един ред
	if affectedRows != 1 {
		if affectedRows == 0 {
			return errors.New("query will affect no rows")
		}
		return errors.New("query will affect more than one row")
	}

	//Изпълнява UPDATE заявката
	_, err = db.Exec(q)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//Count връща колко записи има в някоя таблица
func (t *Table) Count(tx *pop.Connection) (uint, error) {
	db, _ := sql.Open(t.DB.Driver, t.DB.getConnString())
	defer db.Close()
	row := db.QueryRow(countQuery(t.DB.Driver, t.Name))

	var count uint
	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

//BeforeCreate вика се преди да създаде запис
func (t *Table) BeforeCreate(tx *pop.Connection) error {
	t.DBID = t.DB.ID
	return nil
}

//AfterDestroy вика се след изтриване на запис
func (t *Table) AfterDestroy(tx *pop.Connection) error {
	tx.Destroy(&t.Columns)
	return nil
}

//AfterFind вика се след намиране на запис
func (t *Table) AfterFind(tx *pop.Connection) error {
	var cols Columns
	e := tx.Eager().Where("table_id = ? ", t.ID).Order("created_at ASC").All(&cols)
	if e != nil {
		return errors.WithStack(e)
	}
	t.Columns = cols
	return nil
}

//Въща стринговата репрезентация на данни от клетка от някоя таблица
func getValue(pval *interface{}) string {
	switch v := (*pval).(type) {
	case nil:
		return "NULL"
	case bool:
		if v {
			return "Bool(True)"
		} else {
			return "Bool(False)"
		}
	case []byte:
		return string(v)
	case time.Time:
		return v.Format("2006-01-02 (15:04)")
	default:
		return fmt.Sprintf("%v", v)
	}
}

//columnIn проверява, дали някоя колона е част от масив с колони (сравнява по id или  column_name)
func columnIn(c Column, cs Columns) bool {
	for _, i := range cs {
		if i.ID == c.ID || c.Column == i.Column {
			return true
		}
	}
	return false
}

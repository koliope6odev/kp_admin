package actions

//-----------------------------------------------------
// Контролер, който се грижи за нещата
// сврързани с въстановяване на паролата на потребител
// праща email, проверява token-a, такива работи
//-----------------------------------------------------

import (
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/mailers"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

//ResetPassGet връща страницата за промянва на паролата (въвеждане на потребител)
func ResetPassGet(c buffalo.Context) error {
	return c.Render(200, r.HTML("auth/resetPass", "layouts/login"))
}

//ResetPassSendMail праща email с код за промяна на паролата
func ResetPassSendMail(c buffalo.Context) error {
	tx := models.DB

	u := &models.User{}
	c.Bind(u)

	exists, err := u.FindByEmail(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	//Проверка, дали съществува такъв проверител
	if !exists || u.Email == nulls.NewString("") {
		return c.Render(403, r.JSON(T.Translate(c, "resetPass.noEmail")))
	}

	//Проверка дали е използвал social auth
	if u.Provider.String != "" {
		p := "GitHub"
		if u.Provider.String == "linkedin" {
			p = "LinkedIn"
		}

		return c.Render(403, r.JSON(T.Translate(c, "resetPass.socialAuth", map[string]interface{}{
			"type": p,
		})))
	}

	pr := models.ResetPass{User: *u}

	//проверка, дали скоро не пратен email
	if b, _ := pr.MaxRequestsReached(tx); b {
		return c.Render(403, r.JSON(
			T.Translate(c, "resetPass.tooMany")))
	}

	err = pr.GenerateToken()
	if err != nil {
		fmt.Println("54: --  " + err.Error())
		return errors.WithStack(err)
	}

	pr.Create(models.DB)
	//go
	if err := mailers.SendResetPassEmail(pr); err != nil {
		fmt.Println("61: --  " + err.Error())
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON("success"))
}

//ResetPassCheckToken проверява дали кода е правилния
func ResetPassCheckToken(c buffalo.Context) error {
	tx := models.DB
	c.Request().ParseForm()
	//Token
	t := c.Request().PostForm.Get("token")

	//Потребитя, който праща запитване (поне email-a му)
	u := &models.User{}
	c.Bind(u)

	exists, err := u.FindByEmail(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if !exists || u.Email == nulls.NewString("") {
		return c.Render(403, r.JSON(T.Translate(c, "resetPass.noEmail")))
	}

	prs := &models.ResetPass{}
	prs.User = *u
	if err := prs.GetToken(tx); err != nil {
		return errors.WithStack(err)
	}

	if prs.Token != t {
		return c.Render(403, r.JSON(T.Translate(c, "resetPass.invalidCode")))
	}

	return c.Render(200, r.JSON("success"))
}

//ResetPass обслужва последната стъпка от формата за промяна на паролата
func ResetPass(c buffalo.Context) error {
	tx := models.DB
	c.Request().ParseForm()
	//Token
	t := c.Request().PostForm.Get("token")

	//Потребитя, който праща запитване
	u := &models.User{}
	c.Bind(u)

	exists, err := u.FindByEmail(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	//Проверка, дали съществува такъв проверител
	if !exists || u.Email == nulls.NewString("") {
		return c.Render(403, r.JSON(T.Translate(c, "resetPass.noEmail")))
	}

	//Проверка дали е използвал social auth
	if u.Provider.String != "" {
		p := "GitHub"
		if u.Provider.String == "linkedin" {
			p = "LinkedIn"
		}
		return c.Render(403, r.JSON(T.Translate(c, "resetPass.socialAuth", map[string]interface{}{
			"type": p,
		})))
	}

	//Проверява, дали кода е валиден
	prs := &models.ResetPass{}
	prs.User = *u
	if err := prs.GetToken(tx); err != nil {
		if err.Error() == "sql: no rows in result set" {
			return c.Render(403, r.JSON(T.Translate(c, "resetPass.invalidCode")))
		}
		return errors.WithStack(err)
	}

	if prs.Token != t {
		return c.Render(403, r.JSON(T.Translate(c, "resetPass.invalidCode")))
	}

	verrs, err := u.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}

	//Изтрива всички заявки пратени от този потребител
	if err := prs.DestroyForUser(tx); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON("success"))
}

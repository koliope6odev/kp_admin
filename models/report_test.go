package models_test

import (
	"github.com/gobuffalo/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

//Проверява дали запазва бъг репорт с save метода без да хвърля грешки
func (ms *ModelSuite) Test_Report_SaveMethod() {

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	r := &models.Report{}
	r.Os = "Ososos"
	r.OsVersion = "OsVersion"
	r.Browser = "Browser"
	r.Ip = "IP address"
	r.Email = u.Email.String
	r.Comment = "CommentCommentComment"

	err := models.DB.Save(r)
	ms.NoError(err)
}

//Проверява дали запазва репорт с create метода
func (ms *ModelSuite) Test_Report_Create() {

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	r := &models.Report{}
	r.Os = "Ososos"
	r.OsVersion = "OsVersion"
	r.Browser = "Browser"
	r.Ip = "IP address"
	r.Email = u.Email.String
	r.Comment = "CommentCommentComment"

	_, err := r.Create(models.DB)
	ms.NoError(err)
}

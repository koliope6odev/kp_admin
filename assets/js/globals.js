/*
    Глобални променливи и функции, които ползваме тук там
 */

//Функция за вземане на get параметър
window.getUrlParam = function (name) {
    // search =
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(window.location.search))
        return decodeURIComponent(name[1]);
    return ""
};

window.setUrlParam = function (key, value) {
    let  url = new URL(window.location.href);
    url.hash = "";

    url.searchParams.set(key, value);
    if (!value){
        url.searchParams.delete(key);
    }

    window.history.pushState(key, key, url.href);
};


//Иконките за различните типове данни
window.dataTypesIcons = {
    "id": "key",
    "string": "comment",
    "text": "align-justify ",
    "time": "clock",
    "binary": "bold",
    "int": "dice-three",
    "float": "ship",
    "json": "code",
    "geometry": "shapes",
    "bool": "check",
    "xml": "code",
    "multi-value": "list-ul",
};


//Имената на различните типове функции за промяна
window.mutation_funcs = [
    {
        name: "timestamp2timestring", verbose: {
            bg: "timestamp към timestring",
            en: "timestamp to timestring",
        }
    },
    {
        name: "int2bool", verbose: {
            bg: "1/0 към true/false",
            en: "1/0 to true/false",
        }
    },
    {
        name: "stringF", verbose: {
            bg: "+ текст",
            en: "+ text",
        }
    },
    {
        name: "dict", verbose: {
            bg: "речник",
            en: "dictionary",
        }
    },

];

// Имената на различните типове функции за валидация
window.validation_funcs = [
    {
        name: "stringRequired", verbose: {
            bg: "задължително въвеждане",
            en: "input required",
        }
    },
    {
        name: "intRequired", verbose: {
            bg: "задължително въвеждане на число",
            en: "integer required",
        }
    },
    {
        name: "stringLengthInRange", verbose: {
            bg: "дължина на текст в границите",
            en: "string length in range",
        }
    },
    {
        name: "email", verbose: {
            bg: "валиден E-mail",
            en: "valid E-mail",
        }
    },
    {
        name: "url", verbose: {
            bg: "валиден URL адрес",
            en: "valid URL",
        }
    },
    {
        name: "intGraterThan", verbose: {
            bg: "число по-голямо от",
            en: "integer grater than",
        }
    },
    {
        name: "intLessThan", verbose: {
            bg: "число по-малко от",
            en: "integer less than",
        }
    },
    {
        name: "dateBefore", verbose: {
            bg: "дата преди",
            en: "date before",
        }
    },
    {
        name: "dateAfter", verbose: {
            bg: "дата след",
            en: "date after",
        }
    },
    {
        name: "dateBeforeNow", verbose: {
            bg: "дата преди датата на въвеждане",
            en: "date before today",
        }
    },
    {
        name: "dateAfterNow", verbose: {
            bg: "дата след датата на въвеждане",
            en: "date after today",
        }
    },
];
package actions

//--------------------------------------------------------------------
// Контролер, който се грижи за нещата свързани с таблиците в някоя БД
// Преименуване, задаване на PK колона, и намиране на edit-logo-вете
//---------------------------------------------------------------------

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
	"strings"
)

//RenameTbl преименува таблица (сменя и verbose името )
func TablesRenameTbl(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)

	//Взима id-то на таблицата
	id, e := uuid.FromString(c.Param("table_id"))
	if e != nil {
		if strings.Contains(e.Error(), "uuid: incorrect UUID length") {
			return c.Render(406, r.JSON("invalid table id"))
		}
		return errors.WithStack(e)
	}

	//Ако id-то е NIL
	if id == uuid.Nil {
		return c.Render(406, r.JSON("invalid table id"))
	}

	//Взима новото име от  request-a
	verb := c.Request().PostForm.Get("verbose")

	//намира таблицата
	var tbl models.Table
	err := tx.Find(&tbl, id)
	if err != nil {
		return errors.WithStack(err)
	}

	//Преименува я
	verrs, err := tbl.Rename(tx, verb)
	if err != nil {
		return errors.WithStack(err)
	}

	//Ако има валидационни грешки (твърде дълго/късо име и тн.)
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs))
	}

	//Всичко 6
	return c.Render(200, r.JSON("OK"))
}

//TableGetEditLogs връща всички edit-logo-ве за дадена таблица
func TablesGetTblEditLogs(c buffalo.Context) error {

	tx := models.DB // c.Value("tx").(*pop.Connection)

	//намира таблицата на база id-то
	t, err := tablesGetTable(c, tx)
	if err != nil {
		return c.Render(406, r.String(err.Error()))
	}

	//Намира log-овете за таблицата
	var logs models.EditLogs
	if err := logs.TableLogs(tx, t); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(logs))
}

//TableChangePKColumn запазва/променя primary key колона за таблицата
func TablesSetTblPKColumn(c buffalo.Context) error {
	tx := models.DB // c.Value("tx").(*pop.Connection)

	//намира таблицата на база id-то
	t, err := tablesGetTable(c, tx)
	if err != nil {
		return c.Render(406, r.String(err.Error()))
	}

	//взима pk_column от request-a
	pkColumn := c.Request().PostForm.Get("pk_column")

	//проверява, дали колоната садържа САМО уникални стойности
	el := models.EditLog{
		PKColumn: pkColumn,
		Column:   models.Column{Table: t},
	}
	ok, err := el.CheckPKColumn()

	if err != nil {
		return c.Render(406, r.String("invalid column"))
	}

	if !ok {
		return c.Render(403, r.JSON("Колоната трябва да садържа само уникални стойности"))
	}

	t.PkColumn = nulls.NewString(pkColumn)

	//запазва промените в базата
	if err := tx.Save(&t); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.String("OK"))
}

//tablesGetTable намира таблицата на база параметъра от url-a
//прави и нужните валидации
func tablesGetTable(c buffalo.Context, tx *pop.Connection) (models.Table, error) {

	//Взима id-то на таблицата
	id, e := uuid.FromString(c.Param("table_id"))
	if e != nil {
		if strings.Contains(e.Error(), "uuid: incorrect UUID length") {
			return models.Table{}, errors.New("invalid table id")
		}
		return models.Table{}, errors.WithStack(e)
	}

	//намира таблицата на база id-то
	var t models.Table
	if err := tx.Find(&t, id); err != nil {
		return models.Table{}, errors.New("cannot find table with such id")

	}
	if err := tx.Find(&t.DB, t.DBID); err != nil {
		return models.Table{}, errors.New("invalid table")
	}
	return t, nil
}

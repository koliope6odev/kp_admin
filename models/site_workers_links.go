package models

// ----------------------------------
//
//  Модел за линковете за присъединяване
//
// ----------------------------------

import (
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/pkg/errors"
)

type SiteWorkerLink struct {
	ID        uuid.UUID `db:"id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`

	SiteId     uuid.UUID    `db:"site_id"`     //id на сайта
	StringLink nulls.String `db:"string_link"` //рандом стринг за присъединяване
}

type SiteWorkerLinks []SiteWorkerLink

//Create wrapper за създаване на линк за присъединяване
func (link *SiteWorkerLink) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(link)
}

//Update wrapper за редактиране на линк за присъединяване
func (link *SiteWorkerLink) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(link)
}

//Зареждане на линк чрез рандом стринга
func (l *SiteWorkerLinks) GetLinkByString(stringLink string) error {
	var links []SiteWorkerLink
	if err := DB.Where("string_link = ?", stringLink).All(&links); err != nil {
		return errors.WithStack(err)
	}

	ll := *l
	for _, link := range links {
		ll = append(ll, link)
	}
	*l = ll

	return nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (link *SiteWorkerLink) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (link *SiteWorkerLink) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

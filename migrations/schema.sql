--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cell_edit_validation_rules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cell_edit_validation_rules (
    id uuid NOT NULL,
    column_id uuid NOT NULL,
    function_type character varying(255) NOT NULL,
    params character varying(255) NOT NULL,
    message character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.cell_edit_validation_rules OWNER TO postgres;

--
-- Name: charts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.charts (
    id uuid NOT NULL,
    dbid uuid NOT NULL,
    site_id uuid NOT NULL,
    table_name character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    verbose_name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    statistics_type character varying(255) NOT NULL,
    is_timestamp boolean,
    second_column character varying(255)
);


ALTER TABLE public.charts OWNER TO postgres;

--
-- Name: columns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.columns (
    id uuid NOT NULL,
    table_id uuid NOT NULL,
    column_name character varying(255) NOT NULL,
    verbose_name character varying(255),
    data_type character varying(255) NOT NULL,
    data_type_original character varying(255) NOT NULL,
    data_type_readonly boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    read_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.columns OWNER TO postgres;

--
-- Name: databases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databases (
    id uuid NOT NULL,
    driver character varying(255) NOT NULL,
    host bytea NOT NULL,
    port character varying(255) NOT NULL,
    username bytea NOT NULL,
    password bytea NOT NULL,
    db bytea NOT NULL,
    site uuid NOT NULL,
    connection_name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.databases OWNER TO postgres;

--
-- Name: edit_logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.edit_logs (
    id uuid NOT NULL,
    column_id uuid NOT NULL,
    user_id uuid NOT NULL,
    old_value text NOT NULL,
    new_value text NOT NULL,
    pk_column character varying(255) NOT NULL,
    pk_value character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.edit_logs OWNER TO postgres;

--
-- Name: invites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invites (
    id uuid NOT NULL,
    site_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.invites OWNER TO postgres;

--
-- Name: manual_pages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.manual_pages (
    id uuid NOT NULL,
    text text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title character varying(255) NOT NULL,
    screenshots jsonb
);


ALTER TABLE public.manual_pages OWNER TO postgres;

--
-- Name: mutation_functions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mutation_functions (
    id uuid NOT NULL,
    column_id uuid NOT NULL,
    mutation_function character varying(255) NOT NULL,
    dictionary json,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    string_f character varying(255)
);


ALTER TABLE public.mutation_functions OWNER TO postgres;

--
-- Name: password_reset; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_reset (
    id uuid NOT NULL,
    token character varying(255) NOT NULL,
    user_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.password_reset OWNER TO postgres;

--
-- Name: relations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.relations (
    id uuid NOT NULL,
    column_id uuid NOT NULL,
    origin_table character varying(255) NOT NULL,
    relation_table character varying(255) NOT NULL,
    origin_column character varying(255) NOT NULL,
    relation_column character varying(255) NOT NULL,
    replace_column character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.relations OWNER TO postgres;

--
-- Name: reports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reports (
    id uuid NOT NULL,
    os character varying(255) NOT NULL,
    os_version character varying(255) NOT NULL,
    browser character varying(255) NOT NULL,
    ip_address character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    comment character varying(255) NOT NULL,
    screenshots character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.reports OWNER TO postgres;

--
-- Name: schema_migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migration (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migration OWNER TO postgres;

--
-- Name: site_worker_links; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.site_worker_links (
    id uuid NOT NULL,
    site_id uuid NOT NULL,
    string_link character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.site_worker_links OWNER TO postgres;

--
-- Name: sites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sites (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    link character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    owner_id uuid NOT NULL,
    photo character varying(255)
);


ALTER TABLE public.sites OWNER TO postgres;

--
-- Name: sites_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sites_users (
    id uuid NOT NULL,
    site_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    rights character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.sites_users OWNER TO postgres;

--
-- Name: tables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tables (
    id uuid NOT NULL,
    db uuid NOT NULL,
    name character varying(255) NOT NULL,
    access character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    verbose_name character varying(255),
    pk_column character varying(255)
);


ALTER TABLE public.tables OWNER TO postgres;

--
-- Name: testing_datatypes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.testing_datatypes (
    id uuid NOT NULL,
    string character varying(255) NOT NULL,
    text text NOT NULL,
    "int" integer NOT NULL,
    bool boolean NOT NULL,
    "time" timestamp without time zone NOT NULL,
    int8 bigint NOT NULL,
    "bit" bit(1) NOT NULL,
    "bigint" bigint NOT NULL,
    box box NOT NULL,
    bytea bytea NOT NULL,
    cidr cidr NOT NULL,
    circle circle NOT NULL,
    float8 double precision NOT NULL,
    inet inet NOT NULL,
    "integer" integer NOT NULL,
    "interval" interval NOT NULL,
    json json NOT NULL,
    jsonb jsonb NOT NULL,
    line line NOT NULL,
    lseg lseg NOT NULL,
    macaddr macaddr NOT NULL,
    money money NOT NULL,
    "numeric" numeric NOT NULL,
    path path NOT NULL,
    pg_lsn pg_lsn NOT NULL,
    point point NOT NULL,
    polygon polygon NOT NULL,
    "real" real NOT NULL,
    "smallint" smallint NOT NULL,
    smallserial smallint NOT NULL,
    serial integer NOT NULL,
    tsquery tsquery NOT NULL,
    tsvector tsvector NOT NULL,
    txid_snapshot txid_snapshot NOT NULL,
    xml xml NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.testing_datatypes OWNER TO postgres;

--
-- Name: testing_datatypes_serial_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.testing_datatypes_serial_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.testing_datatypes_serial_seq OWNER TO postgres;

--
-- Name: testing_datatypes_serial_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.testing_datatypes_serial_seq OWNED BY public.testing_datatypes.serial;


--
-- Name: testing_datatypes_smallserial_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.testing_datatypes_smallserial_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.testing_datatypes_smallserial_seq OWNER TO postgres;

--
-- Name: testing_datatypes_smallserial_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.testing_datatypes_smallserial_seq OWNED BY public.testing_datatypes.smallserial;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id uuid NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    provider character varying(255),
    provider_id character varying(255),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    password_hash character varying(255) DEFAULT ''::character varying NOT NULL,
    avatar character varying(255),
    cover character varying(255) DEFAULT ''::character varying,
    phone character varying(255) DEFAULT ''::character varying,
    birthday timestamp without time zone,
    city character varying(255) DEFAULT ''::character varying,
    occupation character varying(255) DEFAULT ''::character varying,
    skills json
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: testing_datatypes smallserial; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.testing_datatypes ALTER COLUMN smallserial SET DEFAULT nextval('public.testing_datatypes_smallserial_seq'::regclass);


--
-- Name: testing_datatypes serial; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.testing_datatypes ALTER COLUMN serial SET DEFAULT nextval('public.testing_datatypes_serial_seq'::regclass);


--
-- Name: cell_edit_validation_rules cell_edit_validation_rules_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cell_edit_validation_rules
    ADD CONSTRAINT cell_edit_validation_rules_pkey PRIMARY KEY (id);


--
-- Name: charts charts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.charts
    ADD CONSTRAINT charts_pkey PRIMARY KEY (id);


--
-- Name: columns columns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.columns
    ADD CONSTRAINT columns_pkey PRIMARY KEY (id);


--
-- Name: databases databases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databases
    ADD CONSTRAINT databases_pkey PRIMARY KEY (id);


--
-- Name: edit_logs edit_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.edit_logs
    ADD CONSTRAINT edit_logs_pkey PRIMARY KEY (id);


--
-- Name: invites invites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invites
    ADD CONSTRAINT invites_pkey PRIMARY KEY (id);


--
-- Name: manual_pages manual_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manual_pages
    ADD CONSTRAINT manual_pages_pkey PRIMARY KEY (id);


--
-- Name: mutation_functions mutation_functions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mutation_functions
    ADD CONSTRAINT mutation_functions_pkey PRIMARY KEY (id);


--
-- Name: password_reset password_reset_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.password_reset
    ADD CONSTRAINT password_reset_pkey PRIMARY KEY (id);


--
-- Name: relations relations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_pkey PRIMARY KEY (id);


--
-- Name: reports reports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (id);


--
-- Name: site_worker_links site_worker_links_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.site_worker_links
    ADD CONSTRAINT site_worker_links_pkey PRIMARY KEY (id);


--
-- Name: sites sites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (id);


--
-- Name: sites_users sites_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sites_users
    ADD CONSTRAINT sites_users_pkey PRIMARY KEY (id);


--
-- Name: tables tables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tables
    ADD CONSTRAINT tables_pkey PRIMARY KEY (id);


--
-- Name: testing_datatypes testing_datatypes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.testing_datatypes
    ADD CONSTRAINT testing_datatypes_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: schema_migration_version_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX schema_migration_version_idx ON public.schema_migration USING btree (version);


--
-- PostgreSQL database dump complete
--


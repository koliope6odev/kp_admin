let locale =  document.querySelector("meta[name='language']").getAttribute("content");
console.info("locale set to ", locale);
require("materialize-css/dist/js/materialize.min.js");
require("expose-loader?$!expose-loader?jQuery!jquery");
require("axios/dist/axios.js");
require("./globals");

if (locale === "bg"){
    require('moment/locale/bg');
}
window.locale = locale;

window.axios = require('axios');
window.qs = require('qs');
window._ = require('lodash');
window.moment = require('moment');
window.noUiSlider = require("materialize-css/extras/noUiSlider/nouislider.js");

let token = document.head.querySelector('meta[name="csrf-token"]');
window.csrf_token = token.content;

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';


$(() => {
    //Регистира serviceWorker-и (за кеширане на js и css )
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/sw.js').then(d => {
            console.log(d)
        }).catch(e => console.log(e))
    }
});


import Vue from "vue";
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';

Vue.use(Vuex);
Vue.use(VueI18n);

window.VuexStore = require('./vuexStore.js').default;
window.Vue = Vue;


require("./vueComponents");
let {messages} = require("./i18n");

const i18n = new VueI18n({
    locale,
    messages, // set locale messages
});



const vueApp = new Vue({
    el: '#app',
    store: VuexStore,
    i18n
});


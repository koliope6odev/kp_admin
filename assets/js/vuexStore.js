import Vuex from "vuex";

export default new Vuex.Store({
    state: {
        urls: {},
        site: {link: "", desc: "", name: ""},
        csrfToken: "",
        siteUsers: {}, //потребителите за даден сайт
        access: "", //нивото на достъп до сайта admin | owner | rw | read
        connections: [],
        connection: null, //избраната връзка
        table: {}, //избраната таблица
        column: {},//избраната колона
        charts: {}, //диаграмите за дадения сайт

        validation_func: {}//функцията за валидация на данни в някоя колона, която е ативна
    },
    mutations: {
        addUrls(state, urls) {
            $.each(urls, (i, e) => {
                state.urls[i] = e
            });
        },
        setSite(state, site) {
            state.site = site;
        },
        setSiteUsers(state, users) {
            state.siteUsers = users;
        },
        setCsrfToken(state, token) {
            state.csrfToken = token
        },
        setUserAccess(state, access) {
            state.access = access;
        },
        setConnections(state, connections) {
            state.connections = connections;
        },
        setTable(state, table) {
            state.table = table;
        },
        setConnection(state, connection) {
            state.connection = connection;
        },
        setColumn(state, col) {
            state.column = col;
        },
        setCharts(state, charts) {
            state.charts = charts;
        },
        setValidationFunc(state, func) {
            state.validation_func = func;
        }
    },
    actions: {
        setConnections({commit}, connections) {
            commit("setConnections", connections)
        },
        addConnection(state, connection) {
            let cs = state.getters.connections;
            cs.unshift(connection);
            state.commit("setConnections", cs)
        },
        updateConnection(state, connection) {
            let cs = state.getters.connections;
            for (let i = 0; i < cs.length; i++) {
                if (cs[i].id == connection.id) {
                    cs[i] = connection;
                    break;
                }
            }
            state.commit("setConnections", cs)
        },
        setTable(state, table) {
            state.commit("setTable", table);
            let tbls = state.getters.connection.tables;
            tbls = _.map(tbls, (e) => {
                if (e.id !== table.id) return e;
                return table
            });
            state.dispatch("connectionUpdateTables", tbls)
        },
        setConnection({commit}, connection) {
            commit("setConnection", connection)
        },
        connectionUpdateTables(state, tables) {
            let con = state.getters.connection;
            con.tables = tables;
            state.commit("setConnection", con);
        },
        addRelation(state, {relation, column}) {
            let t = state.getters.table;
            t.columns = _.map(t.columns, c => {
                if (c.id === column.id) c.relation = relation;
                return c
            });
            state.commit("setTable", t);
        },
        removeRelation(state, column) {
            let t = state.getters.table;
            t.columns = _.map(t.columns, c => {
                if (c.id == column.id) {
                    c.relation = {}
                }
                return c
            });
            state.commit("setTable", t);
        },
        addMutationFunc(state, {mutation_func, column}) {
            let t = state.getters.table;
            t.columns = _.map(t.columns, c => {
                if (c.id == column.id) c.mutation_function = mutation_func;
                return c
            });
            t.triggerWatcher = true;
            state.commit("setTable", t);
        },
        removeMutation(state, column) {
            let t = state.getters.table;
            t.columns = _.map(t.columns, c => {
                if (c.id === column.id) {
                    c.mutation_function = {}
                }
                return c
            });
            state.commit("setTable", t);
        },
        addValidationFunc(state, {validation_func, column}) {
            let t = state.getters.table;
            console.log(validation_func);
            t.columns = _.map(t.columns, c => {
                if (c.id === column.id) c.validation_rules.push(validation_func);
                return c
            });
            state.commit("setTable", t);
        },
        removeValidation(state, {validation_func, column}) {
            let t = state.getters.table;
            t.columns = _.map(t.columns, c => {
                if (c.id === column.id) {
                    c.validation_rules = _.reject(c.validation_rules, (r) => {
                        return r.id === validation_func.id
                    })
                }
                return c
            });
            state.commit("setTable", t);
        },
        changeRights(state, {user, rights}) {
            let users = state.getters.siteUsers;
            users = _.map(users, u => {
                if (u.id === user.id) u.rights = rights;
                return u;
            });
            state.commit("setSiteUsers", users)
        },
        removeUser(state, user) {
            let users = state.getters.siteUsers;
            users = _.reject(users, u => {
                return u.id === user.id
            });
            state.commit("setSiteUsers", users)
        },
        addChart(state, chart) {
            let charts = state.getters.charts;
            if (!charts) {
                charts = []
            }
            charts.push(chart);
            state.commit("setCharts", charts);
        },
        editChart(state, chart) {
            let charts = state.getters.charts;
            console.log(chart);
            charts = _.map(charts, c => {
                if (c.id === chart.id) {
                    return chart
                }
                return c
            });

            state.commit("setCharts", charts);
        },
        removeChart(state, chart) {
            let charts = state.getters.charts;
            charts = _.reject(charts, c => {
                return c.id === chart.id
            });
            state.commit("setCharts", charts);

        },
    },
    getters: {
        connections: state => state.connections,
        connection: state => state.connection,
        table: state => state.table,
        siteUsers: state => state.siteUsers,
        charts: state => state.charts,
    }

});

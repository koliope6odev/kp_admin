package models_test

import (
	"fmt"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"time"
)

func (ms *ModelSuite) Test_CellEditValidationRule_ValidateCell() {
	v := models.CellEditValidationRule{
		Params:   []interface{}{4, 8},
		Function: "stringLengthInRange",
	}

	v.Function = "qq"
	err1 := v.ValidateCell("Ivan")
	ms.Error(err1)
	ms.Equal("Няма открит валидатор", err1.Error())
	v.Function = "stringLengthInRange"

	// ---- stringLengthInRange ----
	err := v.ValidateCell("Ivan")
	ms.NoError(err)

	err = v.ValidateCell("IvanToLong")
	ms.Error(err)
	ms.Equal("Дължината на въведения текст трябва да е между 4 и 8 символа", err.Error())

	// ---- stringRequired ----
	v.Function = "stringRequired"
	ms.NoError(v.ValidateCell("Ivan"))

	err = v.ValidateCell("")
	ms.Error(err)
	ms.Equal("Въвеждането на стойност е задължително", err.Error())

	//-------- email ------------
	v.Function = "email"
	ms.NoError(v.ValidateCell("emal@gmail.com"))
	ms.NoError(v.ValidateCell(""))

	err = v.ValidateCell("email_not_valid")
	ms.Error(err)
	ms.Equal("Трябва да е валиден Email адрес", err.Error())

	//-------- url -------------
	v.Function = "url"
	ms.NoError(v.ValidateCell("https://domain.info"))
	ms.NoError(v.ValidateCell(""))

	err = v.ValidateCell("url_not_valid")
	ms.Error(err)
	ms.Equal("Трябва да е валиден URL адрес", err.Error())

	//------ intRequired -----------
	v.Function = "intRequired"
	ms.NoError(v.ValidateCell("12"))
	ms.NoError(v.ValidateCell("-12"))

	err = v.ValidateCell("qwerty")
	ms.Error(err)
	ms.Equal("Невалидно число", err.Error())

	err = v.ValidateCell("")
	ms.Error(err)
	ms.Equal("Невалидно число", err.Error())

	err = v.ValidateCell("0")
	ms.Error(err)
	ms.Equal("Въвеждането на число е задължително", err.Error())

	//---- intGraterThan ----
	v.Function = "intGraterThan"
	v.Params = []interface{}{4}
	ms.NoError(v.ValidateCell("12"))

	err = v.ValidateCell("qwerty")
	ms.Error(err)
	ms.Equal("Невалидно число", err.Error())

	err = v.ValidateCell("")
	ms.Error(err)
	ms.Equal("Невалидно число", err.Error())

	err = v.ValidateCell("2")
	ms.Error(err)
	ms.Equal("Въведената стойност трябва да е по-голяма от 4", err.Error())

	err = v.ValidateCell("-22")
	ms.Error(err)
	ms.Equal("Въведената стойност трябва да е по-голяма от 4", err.Error())

	// ----- intLessThan ----
	v.Function = "intLessThan"
	ms.NoError(v.ValidateCell("-12"))
	v.Params = []interface{}{-4}

	err = v.ValidateCell("qwerty")
	ms.Error(err)
	ms.Equal("Невалидно число", err.Error())

	err = v.ValidateCell("")
	ms.Error(err)
	ms.Equal("Невалидно число", err.Error())

	err = v.ValidateCell("-2")
	ms.Error(err)
	ms.Equal("Въведената стойност трябва да е по-малка от -4", err.Error())

	err = v.ValidateCell("22")
	ms.Error(err)
	ms.Equal("Въведената стойност трябва да е по-малка от -4", err.Error())

	// ----- dateBefore ----
	v.Function = "dateBefore"
	v.Params = []interface{}{time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)}
	ms.NoError(v.ValidateCell("2017-02-10 (12:45)"))

	err = v.ValidateCell("invalid date")
	ms.Error(err)
	ms.Equal("Невалидна дата (изискаван формат: \"2000-01-01 (12:00)\")", err.Error())

	err = v.ValidateCell("2019-02-10 (12:46)")
	ms.Error(err)
	ms.Equal("Трябва да е дата преди 2018-01-01", err.Error())

	// ----- dateBeforeNow ----
	v.Function = "dateBeforeNow"
	ms.NoError(v.ValidateCell("2017-02-10 (12:45)"))

	err = v.ValidateCell("invalid date")
	ms.Error(err)
	ms.Equal("Невалидна дата (изискаван формат: \"2000-01-01 (12:00)\")", err.Error())

	err = v.ValidateCell("2059-02-10 (12:46)")
	ms.Error(err)
	ms.Equal(fmt.Sprintf("Трябва да е дата преди %s", time.Now().Format("2006-01-02")), err.Error())

	// ----- dateAfter ----
	v.Function = "dateAfter"
	v.Params = []interface{}{time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)}
	ms.NoError(v.ValidateCell("2019-02-10 (12:45)"))

	err = v.ValidateCell("invalid date")
	ms.Error(err)
	ms.Equal("Невалидна дата (изискаван формат: \"2000-01-01 (12:00)\")", err.Error())

	err = v.ValidateCell("2017-02-10 (12:46)")
	ms.Error(err)
	ms.Equal("Трябва да е дата след 2018-01-01", err.Error())

	// ----- dateAfterNow ----
	v.Function = "dateAfterNow"
	ms.NoError(v.ValidateCell("2067-02-10 (12:45)"))

	err = v.ValidateCell("invalid date")
	ms.Error(err)
	ms.Equal("Невалидна дата (изискаван формат: \"2000-01-01 (12:00)\")", err.Error())

	err = v.ValidateCell("2019-02-10 (12:46)")
	ms.Error(err)
	ms.Equal(fmt.Sprintf("Трябва да е дата след %s", time.Now().Format("2006-01-02")), err.Error())

	//тествам, дали показва правилното съобщение, ако е зададено v.Message
	v.Message = nulls.NewString("Къстом мес")
	v.Params = []interface{}{1, 2}

	allFs := []string{
		"stringLengthInRange",
		"stringRequired",
		"email",
		"url",
		"intRequired",
		"intGraterThan",
		"intLessThan",
		"dateBefore",
		"dateAfter",
		"dateBeforeNow",
		"dateAfterNow",
	}

	for _, f := range allFs {
		v.Function = f
		val := "not_valid"
		if f == "stringRequired" {
			val = ""
		}
		err = v.ValidateCell(val)
		ms.Error(err)
		ms.Equal("Къстом мес", err.Error())
	}

}

func (ms *ModelSuite) Test_CellEditValidationRule_Create() {

	//Случайно id за колоната
	col, err := uuid.FromString("fa43aaa4-1a11-4fd1-a4d4-5c2e90f67287")
	ms.NoError(err)

	v := models.CellEditValidationRule{
		Params:   []interface{}{4, 8},
		Function: "stringLengthInRange",
		ColumnID: col,
	}

	//Всичко ок
	ms.DBDelta(1, "cell_edit_validation_rules", func() {
		verrs, err := v.Create(models.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny())
	})

	//Aко съобщението е твърде дълго
	v.Message = nulls.NewString("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_")
	ms.DBDelta(0, "cell_edit_validation_rules", func() {
		verrs, err := v.Create(models.DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
	})
	v.Message = nulls.NewString("normal length")

	//Липсващо ColumnID
	v.ColumnID = uuid.Nil
	ms.DBDelta(0, "cell_edit_validation_rules", func() {
		verrs, err := v.Create(models.DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
	})
	v.ColumnID = col

	//несъществуваща функция за валидация
	v.Function = "non_existing"
	verrs, err := v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Фукция с 2 параметъра
	v.Function = "stringLengthInRange"
	v.Params = []interface{}{1, 2, 3}
	verrs, err = v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	v.Params = []interface{}{1}
	verrs, err = v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Фукция с 1 параметър
	v.Function = "intGraterThan"
	v.Params = []interface{}{1, 2}
	verrs, err = v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	v.Params = []interface{}{}
	verrs, err = v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Aко параметъра не отговаря  на формата
	v.Params = []interface{}{"not int"}
	verrs, err = v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	v.Function = "dateBefore"
	v.Params = []interface{}{"not date"}
	verrs, err = v.Create(models.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

}

func (ms *ModelSuite) Test_CellEditValidationRule_AfterFind() {

	//Случайно id за колоната
	col, err := uuid.FromString("fa43aaa4-1a11-4fd1-a4d4-5c2e90f67287")
	ms.NoError(err)

	//----- Числа като параметри  -------------//
	vInt := models.CellEditValidationRule{
		Params:   []interface{}{4, 8},
		Function: "stringLengthInRange",
		ColumnID: col,
	}

	verrs, err := vInt.Create(models.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	var vFromDB models.CellEditValidationRule

	ms.NoError(ms.DB.Find(&vFromDB, vInt.ID))

	ms.Equal(vInt.Params, vFromDB.Params)

	//----- Дати като параметри  -------------//
	vDate := models.CellEditValidationRule{
		Params:   []interface{}{time.Date(2006, 2, 4, 5, 6, 0, 0, time.Local)},
		Function: "dateBefore",
		ColumnID: col,
	}

	verrs, err = vDate.Create(models.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	ms.NoError(ms.DB.Find(&vFromDB, vDate.ID))

	ms.Equal(vDate.Params, vFromDB.Params)
}

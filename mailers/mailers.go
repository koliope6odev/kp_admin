package mailers

import (
	"fmt"
	"log"
	gosmtp "net/smtp"

	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/buffalo/render"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/packr"
)

var smtp mail.Sender
var r *render.Engine

func init() {

	// Pulling config from the env.
	port := envy.Get("SMTP_PORT", "1025")
	host := envy.Get("SMTP_HOST", "localhost")
	user := envy.Get("SMTP_USER", "")
	password := envy.Get("SMTP_PASSWORD", "")

	var err error
	sender, err := mail.NewSMTPSender(host, port, user, password)

	//Ей заради този ш***н ред изгубих 4 часа от живота си
	//Не го трий !
	sender.Dialer.Auth = gosmtp.PlainAuth("", user, password, host)

	if err != nil {
		log.Fatal(err)
		fmt.Printf("MAIL ERROR - %v \n", err.Error())
	}

	smtp = sender

	r = render.New(render.Options{
		HTMLLayout:   "layout.html",
		TemplatesBox: packr.NewBox("../templates/mail"),
		Helpers:      render.Helpers{},
	})
}

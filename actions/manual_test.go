package actions

import (
	"fmt"
	
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_SitesGetManual() {
	//Ако НЕ е логнат
	res := as.HTML("/manual").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	m := &models.ManualPage{}
	m.Title = "Title"
	m.Text = "text text text text text"

	err = as.DB.Create(m)
	as.NoError(err)

	res = as.HTML(fmt.Sprintf("/manual")).Get()
	//Очаква се 200 при get request към страницата с формата
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</manual>")
}

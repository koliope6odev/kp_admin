package models_test

import (
	"fmt"

	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func (ms *ModelSuite) TestRelation_CheckP() {
	c := testRelation_getColumn(ms, "columns", "table_id", "id", "_TestRelation_CheckP")

	r := models.Relation{
		ColumnID:       c.ID,
		ReplaceColumn:  "name",
		OriginTable:    "columns",
		RelationTable:  "tables",
		OriginColumn:   "table_id",
		RelationColumn: "id",
	}
	ok, err := r.Check(c.Table.DB)
	ms.NoError(err)
	ms.True(ok)

	r = models.Relation{
		ColumnID:       c.ID,
		ReplaceColumn:  "name",
		OriginTable:    "columns",
		RelationTable:  "tables",
		OriginColumn:   "table_id",
		RelationColumn: "id1",
	}
	ok, err = r.Check(c.Table.DB)
	ms.False(ok)

	r = models.Relation{
		ColumnID:       c.ID,
		ReplaceColumn:  "name",
		OriginTable:    "columns",
		RelationTable:  "tables",
		OriginColumn:   "table_id1",
		RelationColumn: "id",
	}
	ok, err = r.Check(c.Table.DB)
	ms.False(ok)

	r = models.Relation{
		ColumnID:       c.ID,
		ReplaceColumn:  "name",
		OriginTable:    "columns",
		RelationTable:  "tables1",
		OriginColumn:   "table_id",
		RelationColumn: "id",
	}
	ok, err = r.Check(c.Table.DB)
	ms.False(ok)

	r = models.Relation{
		ColumnID:       c.ID,
		ReplaceColumn:  "name",
		OriginTable:    "columns1",
		RelationTable:  "tables",
		OriginColumn:   "table_id",
		RelationColumn: "id",
	}
	ok, err = r.Check(c.Table.DB)
	ms.False(ok)

	r = models.Relation{
		ColumnID:       c.ID,
		ReplaceColumn:  "name1",
		OriginTable:    "columns",
		RelationTable:  "tables",
		OriginColumn:   "table_id",
		RelationColumn: "id",
	}
	ok, err = r.Check(c.Table.DB)
	ms.False(ok)
}

func testRelation_getColumn(ms *ModelSuite, table, column, dataType, unique string) models.Column {
	s := models.Site{
		Name: nulls.NewString("Name"),
		Desc: nulls.NewString("desk"),
	}
	ms.DB.Save(&s)

	ts := models.Tables{
		{Name: table, Verbose: nulls.NewString(table + unique), Access: "rw"},
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Password: "postgres",
		Port:     5432,
		Host:     "127.0.0.1",
		DB:       "kp_admin_test",
		Tables:   ts,
		Name:     "TestRelation_CheckP",
		Site:     s,
	}
	verrs, err := dbc.Create(ms.DB)
	ms.False(verrs.HasAny(), fmt.Sprintf("има валидационни грешки %v", verrs.Errors))
	ms.NoError(err)

	var t models.Table
	err = ms.DB.Where("verbose_name =?", table+unique).First(&t)
	ms.NoError(err)
	t.DB = dbc

	t.Columns = models.Columns{
		{Column: column, Verbose: nulls.NewString(column + unique), DataType: dataType},
		{Column: "data_type"}, {Column: "data_type_readonly"},
	}
	err = t.SaveColumns(models.DB)
	ms.NoError(err)

	var c models.Column
	err = ms.DB.Where("verbose_name =?", column+unique).First(&c)
	ms.NoError(err)
	c.Table = t
	return c
}

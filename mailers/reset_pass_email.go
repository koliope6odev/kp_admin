package mailers

//---------------------------------------------------------------------
// mailer, грижест се за преащането email-и за въстановяване на парола
//----------------------------------------------------------------------

import (
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

func SendResetPassEmail(prs models.ResetPass) error {
	m := mail.NewMessage()
	m.Subject = "Забравена парола"
	m.From = envy.Get("SMTP_SENDER", "support@bananamin.site")
	m.To = []string{prs.User.Email.String}

	// Data that will be used inside the templates when rendering.
	data := map[string]interface{}{
		"prs": prs,
	}

	err := m.AddBody(r.HTML("reset_pass_email.html"), data)
	if err != nil {
		return errors.WithStack(err)
	}
	m.Bodies[0].ContentType = "text/html"

	return smtp.Send(m)
}

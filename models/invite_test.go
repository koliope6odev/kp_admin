package models_test

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

//Проверява дали запазва покана с save метода без да хвърля грешки
func (ms *ModelSuite) Test_Invite_SaveMethod() {

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:    nulls.NewString("Name"),
		Desc:    nulls.NewString("Description"),
		Link:    nulls.NewString("Link"),
		OwnerId: u.ID,
	}
	models.DB.Save(s)

	i := &models.Invite{
		SiteID: s.ID,
		UserID: u.ID,
	}

	ms.NoError(models.DB.Save(i), "Cannot save a invite")
}

//Проверява дали запазва покана с create метода
func (ms *ModelSuite) Test_Invite_Create() {

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:  nulls.NewString("Name"),
		Desc:  nulls.NewString("Description"),
		Link:  nulls.NewString("Link"),
		Owner: *u,
	}
	models.DB.Save(s)

	i := &models.Invite{
		SiteID: s.ID,
		UserID: u.ID,
	}

	_, err := i.Create(models.DB)
	ms.NoError(err, "Cannot save an invite")
}

func (ms *ModelSuite) Test_Invite_GetByUser() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	u1 := &models.User{
		Name:  nulls.NewString("Ivan1"),
		Email: nulls.NewString("ivan1@example.com"),
	}
	models.DB.Save(u1)

	u2 := &models.User{
		Name:  nulls.NewString("Ivan2"),
		Email: nulls.NewString("ivan2@example.com"),
	}
	models.DB.Save(u2)

	s := &models.Site{
		Name:  nulls.NewString("Name"),
		Desc:  nulls.NewString("Description"),
		Link:  nulls.NewString("Link"),
		Owner: *u2,
	}
	models.DB.Save(s)
	s1 := &models.Site{
		Name:  nulls.NewString("Name1"),
		Desc:  nulls.NewString("Description1"),
		Link:  nulls.NewString("Link1"),
		Owner: *u2,
	}
	models.DB.Save(s1)

	i1 := &models.Invite{
		SiteID: s.ID,
		UserID: u.ID,
	}

	i2 := &models.Invite{
		SiteID: s.ID,
		UserID: u1.ID,
	}

	i3 := &models.Invite{
		SiteID: s1.ID,
		UserID: u.ID,
	}
	models.DB.Save(i1)
	models.DB.Save(i2)
	models.DB.Save(i3)

	var invites models.Invites
	ms.NoError(invites.GetByUser(u.ID), "Хвърля грешка при викане не models.Invites.GetByUser()")

	ms.False(len(invites) > 2, "Върнало е твърде много покани")

	ms.False(len(invites) < 2, "Върнало е твърде малко покани")

	for _, invite := range invites {
		ms.Equal(u.ID, invite.UserID, "Връща покана, която не е до този потребител")
	}
}

func (ms *ModelSuite) Test_Invite_DeleteInvite() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:  nulls.NewString("Name"),
		Desc:  nulls.NewString("Description"),
		Link:  nulls.NewString("Link"),
		Owner: *u,
	}

	models.DB.Save(s)

	i := &models.Invite{
		SiteID: s.ID,
		UserID: u.ID,
	}
	models.DB.Save(i)

	ms.NoError(models.DB.Destroy(i), "Хвърля грешка при викане")

	var invites models.Invites
	ms.NoError(invites.GetByUser(u.ID), "Хвърля грешка при викане не models.Invites.GetByUser()")

	ms.Equal(0, len(invites))
}

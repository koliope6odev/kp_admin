package models_test

import (
	"fmt"
	"strconv"

	"github.com/gobuffalo/pop/nulls"
	"github.com/gofrs/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (ms *ModelSuite) TestTable() {

}

func (ms *ModelSuite) TestTable_Find() {

	tables := models.Tables{
		{
			Access: "TestTable_Find_Connection",
			Name:   "users",
		},
	}

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc := &models.DBConn{}
	dbc.Driver = "postgres"
	dbc.Host = "localhost"
	dbc.User = "postgres"
	dbc.Password = "postgres"
	dbc.DB = "kp_admin_development"
	dbc.Port = 5432
	dbc.Site = s
	dbc.Tables = tables

	verrs, err := dbc.Create(models.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	tbl := models.Table{}
	models.DB.Where("access=?", "TestTable_Find_Connection").First(&tbl)
	tbl.DB = *dbc
	cs, err := tbl.GetColumns()
	ms.NoError(err)
	tbl.Columns = cs
	err = tbl.SaveColumns(models.DB)
	ms.NoError(err)

	table_id := tbl.ID
	//Ей от тука почва исстинския тест
	tbl = models.Table{}
	err = tbl.Find(models.DB, table_id)
	ms.NoError(err)
	ms.NotEqual(tbl.ID, uuid.Nil)
	ms.NotEqual(tbl.DBID, uuid.Nil)
	ms.NotEqual(tbl.DB.ID, uuid.Nil)
	ms.NotEmpty(tbl.Columns)
	ms.NotEqual(tbl.Columns[0].ID, uuid.Nil)
}

func (ms *ModelSuite) TestTable_GetColumnsM() {

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//MySQL
	dbc0 := &models.DBConn{}
	dbc0.Driver = "mysql"
	dbc0.Host = "194.135.94.89"
	dbc0.User = "kp_admin"
	dbc0.Password = "nikolape6o"
	dbc0.DB = "zora"
	dbc0.Port = 3306
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	ms.Len(tables, 10)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "users" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	var columns []string
	for _, e := range cs {
		ms.NotNil(e.DataType)
		ms.NotNil(e.Column)
		columns = append(columns, e.Column)
	}
	ms.NoError(err)
	ms.NotEmpty(columns)
	ms.Contains(columns, "username")
	ms.Contains(columns, "id")
	ms.Contains(columns, "password")
}

func (ms *ModelSuite) TestTable_GetColumnsP() {

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{}
	dbc0.Driver = "postgres"
	dbc0.Host = "localhost"
	dbc0.User = "postgres"
	dbc0.Password = "postgres"
	dbc0.DB = "kp_admin_development"
	dbc0.Port = 5432
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "users" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	var columns []string
	for _, e := range cs {
		ms.NotNil(e.DataType)
		ms.NotNil(e.Column)
		columns = append(columns, e.Column)
	}
	ms.NoError(err)
	ms.NotEmpty(columns)
	ms.Contains(columns, "created_at")
	ms.Contains(columns, "id")
	ms.Contains(columns, "password_hash")
}

func (ms *ModelSuite) TestTable_SaveColumns() {

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{}
	dbc0.Driver = "postgres"
	dbc0.Host = "localhost"
	dbc0.User = "postgres"
	dbc0.Password = "postgres"
	dbc0.DB = "kp_admin_test"
	dbc0.Port = 5432
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "databases" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	t.Columns = cs
	err = t.SaveColumns(models.DB)
	ms.NoError(err)

	var columns models.Columns
	count, err := models.DB.Where("table_id = ?", t.ID).Count(columns)
	ms.Equal(11, count)
}

func (ms *ModelSuite) TestTable_UpdateColumns() {
	t := models.Table{
		Name:   "TestTable_UpdateColumns_TABLE",
		Access: "rw",
	}
	err := models.DB.Create(&t)
	ms.NoError(err)

	cols := models.Columns{
		{Column: "col1", Verbose: nulls.NewString("Col1"), DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col2", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col3", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col4", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col5", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
	}

	t.Columns = cols

	ms.DBDelta(5, "columns", func() {
		err = t.SaveColumns(models.DB)
		ms.NoError(err)
	})
	count, err := models.DB.Where("table_id = ?", t.ID).Count(&models.Columns{})
	ms.NoError(err)
	ms.Equal(5, count)

	cols = models.Columns{
		{Column: "col2", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col4", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col5", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col6", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col7", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col8", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
		{Column: "col89", DataType: "string", DataTypeOriginal: "varchar", DataTypeReadOnly: false},
	}
	t.Columns = cols
	ms.DBDelta(2, "columns", func() {
		err = t.UpdateColumns(models.DB)
		ms.NoError(err)
	})

	count, err = models.DB.Where("table_id = ?", t.ID).Count(&models.Columns{})
	ms.NoError(err)
	ms.Equal(7, count)

}

func (ms *ModelSuite) TestTable_CheckConnP() {

	//Сайта, към който е базата
	site := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&site)

	//postgres
	dbc := models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_development",
		Port:     5432,
		Site:     site,
		Tables: models.Tables{
			{Name: "users", Verbose: nulls.NewString("testing_connection_users")},
			{Name: "not_existing", Verbose: nulls.NewString("testing_connection_not_existing")},
		},
	}

	verrs, err := dbc.Create(ms.DB)
	ms.False(verrs.HasAny(), fmt.Sprintf("Има грешки %v", verrs.Errors))
	ms.NoError(err)

	var t models.Table
	err = ms.DB.Where("verbose_name = ?", "testing_connection_users").First(&t)
	t.DB = dbc
	ms.NoError(err)
	ms.False(t.ID == uuid.Nil)

	//----- Правилни данни за свързване ------ //
	ok, err := t.CheckConn(ms.DB)
	ms.True(ok)
	ms.NoError(err)

	//----- Невалидни данни за свързване ------ //
	t.DB.User = "invalid_user"
	ok, err = t.CheckConn(ms.DB)
	ms.False(ok)
	ms.NotNil(err)
	t.DB.User = "postgres"

	//------ Невалидно име на таблица ------- //
	err = ms.DB.Where("verbose_name = ?", "testing_connection_not_existing").First(&t)
	t.DB = dbc
	ms.NoError(err)
	ms.False(t.ID == uuid.Nil)

	ok, err = t.CheckConn(ms.DB)
	ms.False(ok)
	ms.NotNil(err)
}

func (ms *ModelSuite) TestTable_CheckConnM() {

	//Сайта, към който е базата
	site := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&site)

	//mysql
	dbc := models.DBConn{
		Driver:   "mysql",
		Host:     "194.135.94.89",
		User:     "kp_admin",
		Password: "nikolape6o",
		DB:       "zora",
		Port:     3306,
		Site:     site,
		Tables: models.Tables{
			{Name: "users", Verbose: nulls.NewString("testing_connection_users")},
			{Name: "not_existing", Verbose: nulls.NewString("testing_connection_not_existing")},
		},
	}

	verrs, err := dbc.Create(ms.DB)
	ms.False(verrs.HasAny(), fmt.Sprintf("Има грешки %v", verrs.Errors))
	ms.NoError(err)

	var t models.Table
	err = ms.DB.Where("verbose_name = ?", "testing_connection_users").First(&t)
	t.DB = dbc
	ms.NoError(err)
	ms.False(t.ID == uuid.Nil)

	//----- Правилни данни за свързване ------ //
	ok, err := t.CheckConn(ms.DB)
	ms.True(ok)
	ms.NoError(err)

	//----- Невалидни данни за свързване ------ //
	t.DB.User = "invalid_user"
	ok, err = t.CheckConn(ms.DB)
	ms.False(ok)
	ms.NotNil(err)
	t.DB.User = "nikolape6o"

	//------ Невалидно име на таблица ------- //
	err = ms.DB.Where("verbose_name = ?", "testing_connection_not_existing").First(&t)
	t.DB = dbc
	ms.NoError(err)
	ms.False(t.ID == uuid.Nil)

	ok, err = t.CheckConn(ms.DB)
	ms.False(ok)
	ms.NotNil(err)
}

func (ms *ModelSuite) TestTable_RowsM() {
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Mysql
	dbc0 := &models.DBConn{}
	dbc0.Driver = "mysql"
	dbc0.Host = "194.135.94.89"
	dbc0.User = "kp_admin"
	dbc0.Password = "nikolape6o"
	dbc0.DB = "zora"
	dbc0.Port = 3306
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "users" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	t.Columns = cs
	err = t.SaveColumns(models.DB)
	ms.NoError(err)

	//----------------ЕЙ ОТ ТУКА ПОЧВА ИСТИНСКИЯТ ТЕСТ ГОРНОТО БЕ COPY PASTE
	c := make(chan []string)
	go t.Rows(c, "")
	var rows [][]string
	for i := range c {
		ms.NotEmpty(i)
		rows = append(rows, i)
	}
	ms.NotEmpty(rows)
	ms.NotEmpty(rows[0])
	ms.NoError(err)

	//Ако има дума за търсене
	cS := make(chan []string)
	var rowsSearchable [][]string
	go t.Rows(cS, "test")
	for i := range cS {
		ms.NotEmpty(i)
		rowsSearchable = append(rowsSearchable, i)
	}
	ms.NoError(err)
	ms.NotEmpty(rowsSearchable)
	ms.NotEmpty(rowsSearchable[0])
	ms.True(len(rowsSearchable) < len(rows), "Трябва елеменетите намерени с клауза за търсене да са по-малко")

}

func (ms *ModelSuite) TestTable_RowsP() {

	ms.LoadFixture("lots of users")

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{}
	dbc0.Driver = "postgres"
	dbc0.Host = "localhost"
	dbc0.User = "postgres"
	dbc0.Password = "postgres"
	dbc0.DB = "kp_admin_test"
	dbc0.Port = 5432
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "columns" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	t.Columns = cs
	err = t.SaveColumns(models.DB)
	ms.NoError(err)

	//ЕЙ ОТ ТУКА ПОЧВА ИСТИНСКИЯТ ТЕСТ ГОРНОТО БЕ COPY PASTE
	c := make(chan []string)
	go t.Rows(c, "")
	var rows [][]string
	for i := range c {
		ms.NotEmpty(i)
		rows = append(rows, i)
	}
	ms.NotEmpty(rows)
	ms.NotEmpty(rows[0])
	ms.NoError(err)

	//Aко има колона с релация
	t.Columns[1].Relation = models.Relation{
		OriginTable:    "columns",
		OriginColumn:   "table_id",
		RelationColumn: "id",
		RelationTable:  "tables",
		ReplaceColumn:  "name",
	} //table_id
	cR := make(chan []string)
	var rowsRel [][]string
	go t.Rows(cR, "")
	for i := range cR {
		ms.NotEmpty(i)
		rowsRel = append(rowsRel, i)
	}

	//Проверява, дали е сменило table_id-то
	_, uuidErr := uuid.FromString(rowsRel[0][1])
	ms.NotNil(uuidErr, "Не е сменило table_id-то")
	ms.Equal("columns", rowsRel[0][1], "Релацията не бачка")
	ms.NoError(err)
	ms.NotEmpty(rowsRel)
	ms.NotEmpty(rowsRel[0])
	ms.Equal(len(rowsRel), len(rows), "Трябва елеменетите намерени с LEFT JOIN да са същия брой")

	//Ако има дума за търсене
	cS := make(chan []string)
	var rowsSearchable [][]string
	go t.Rows(cS, "read_only")
	for i := range cS {
		ms.NotEmpty(i)
		rowsSearchable = append(rowsSearchable, i)
	}
	ms.NoError(err)
	ms.NotEmpty(rowsSearchable)
	ms.NotEmpty(rowsSearchable[0])
	ms.True(len(rowsSearchable) < len(rows), "Трябва елеменетите намерени с клауза за търсене да са по-малко")

}

func (ms *ModelSuite) TestTable_CountM() {
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Mysql
	dbc0 := &models.DBConn{}
	dbc0.Driver = "mysql"
	dbc0.Host = "194.135.94.89"
	dbc0.User = "kp_admin"
	dbc0.Password = "nikolape6o"
	dbc0.DB = "zora"
	dbc0.Port = 3306
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "users" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	t.Columns = cs
	err = t.SaveColumns(models.DB)
	ms.NoError(err)

	//Ей от тука почва теста
	count, err := t.Count(models.DB)
	ms.NoError(err)
	ms.NotZero(count)
}

func (ms *ModelSuite) TestTable_CountP() {
	ms.LoadFixture("lots of users")
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{}
	dbc0.Driver = "postgres"
	dbc0.Host = "localhost"
	dbc0.User = "postgres"
	dbc0.Password = "postgres"
	dbc0.DB = "kp_admin_test"
	dbc0.Port = 5432
	dbc0.Site = s

	tables, err := dbc0.GetTables()
	ms.NoError(err)
	ms.NotEmpty(tables)
	for _, t := range tables {
		dbc0.Tables = append(dbc0.Tables, models.Table{Name: t, Access: "rw"})
	}
	_, err = dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)
	var t models.Table

	for _, e := range dbc.Tables {
		if e.Name == "users" {
			t = e
		}
	}
	ms.Equal(dbc0.ID, t.DB.ID)

	cs, err := t.GetColumns()
	t.Columns = cs
	err = t.SaveColumns(models.DB)
	ms.NoError(err)

	//Ей от тука почва теста
	count, err := t.Count(models.DB)
	ms.NoError(err)
	ms.NotZero(count)
}

func (ms *ModelSuite) TestTable_EditCell() {

	ms.LoadFixture("lots of users")
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
		Site:     s,
		Tables: models.Tables{
			{Name: "users", Access: "rw"},
		},
	}

	_, err := dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)

	t := dbc.Tables[0]

	ms.Equal(dbc0.ID, t.DB.ID)

	//----------------ЕЙ ОТ ТУКА ПОЧВА ИСТИНСКИЯТ ТЕСТ ГОРНОТО БЕ ГЛАВНО ИНИЦИАЛИЗАЦИЯ НА СРЕДАТА --------------------//
	//Ще променим email-a на потребител от базата
	var user models.User
	ms.NoError(ms.DB.First(&user))
	c := models.Column{
		Column: "email",
	}

	//ако няма identifiers
	err = t.EditCell(c, "newEmail@fresh.recent", map[string]string{})

	ms.Error(err)
	ms.Equal("no identifiers specified", err.Error())

	//Ако identifier-ите "хващат" повече от 1 ред
	err = t.EditCell(c, "newEmail@fresh.recent", map[string]string{
		"provider": "linkedin",
	})

	ms.Error(err)
	ms.Equal("query will affect more than one row", err.Error())

	//Ако identifier-ите "хващат" 0 редове
	err = t.EditCell(c, "newEmail@fresh.recent", map[string]string{
		"provider": "ivan",
	})

	ms.Error(err)
	ms.Equal("query will affect no rows", err.Error())

	//Aко въведените данни не отговарят на типа на колоната
	// (въвеждаме string по-дълъг от 255)
	hugeS := "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789"
	err = t.EditCell(c, hugeS, map[string]string{
		"id":    user.ID.String(),
		"email": user.Email.String,
	})
	ms.Error(err)
	ms.Equal("pq: value too long for type character varying(255)", err.Error())

	//Опит за промяна на readonly колона
	c.ReadOnly = true
	err = t.EditCell(c, "newEmail@fresh.recent", map[string]string{
		"id":    user.ID.String(),
		"email": user.Email.String,
	})
	ms.Error(err)
	ms.Equal("cannot edit readonly column", err.Error())

	//Опит за промяна на readonly колона v2
	c.ReadOnly = false
	c.DataTypeReadOnly = true
	err = t.EditCell(c, "newEmail@fresh.recent", map[string]string{
		"id":    user.ID.String(),
		"email": user.Email.String,
	})
	ms.Error(err)
	ms.Equal("cannot edit readonly column", err.Error())
	c.DataTypeReadOnly = false

	//Реално update-ва
	err = t.EditCell(c, "newEmail@fresh.recent", map[string]string{
		"id":    user.ID.String(),
		"email": user.Email.String,
	})

	ms.NoError(err)

	//Проверка, дали реално е променило базата
	var updatedUser models.User
	ms.DB.Find(&updatedUser, user.ID)
	ms.Equal("newEmail@fresh.recent", updatedUser.Email.String)

	// !!!----------------- Aко има функция за промяна към колоната ---------------------!!!
	c.MutationsFunc = models.MutationFunc{
		FuncName: "timestamp2timestring",
		ID:       t.ID, //някакво ID, няма значение, какво е
	}

	err = t.EditCell(c, "2018-01-14 (09:26)", map[string]string{
		"id": user.ID.String(),
	})

	ms.NoError(err)

	//Проверка, дали реално е променило базата както трябва (с обратната функция за промяна)
	ms.DB.Find(&updatedUser, user.ID)
	timestampt, err := strconv.Atoi(updatedUser.Email.String)
	ms.NoError(err)
	ms.InDelta(1515965160, timestampt, 43200)

	// !!!----------------- Ако има правила за валидация ---------------------!!!
	c.MutationsFunc.ID = uuid.Nil
	c.ValidationRules = models.CellEditValidationRules{
		{
			Function: "email",
		},
		{
			Function: "stringRequired",
		},
		{
			Function: "stringLengthInRange",
			Params:   []interface{}{2, 30},
		},
	}

	err = t.EditCell(c, "not_valid_email", map[string]string{
		"id": user.ID.String(),
	})

	ms.Error(err)
	ms.Equal("Трябва да е валиден Email адрес", err.Error())

	err = t.EditCell(c, "", map[string]string{
		"id": user.ID.String(),
	})

	ms.Error(err)

	err = t.EditCell(c, "1", map[string]string{
		"id": user.ID.String(),
	})

	ms.Error(err)

	err = t.EditCell(c, "123456789_123456789_123456789_123456789_123456789_", map[string]string{
		"id": user.ID.String(),
	})

	ms.Error(err)

	err = t.EditCell(c, "valid@email.address", map[string]string{
		"id": user.ID.String(),
	})

	ms.NoError(err)

}

func (ms *ModelSuite) TestTable_GetCellValue() {
	ms.LoadFixture("lots of users")
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	//Postgres
	dbc0 := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
		Site:     s,
		Tables: models.Tables{
			{Name: "users", Access: "rw"},
		},
	}

	_, err := dbc0.Create(models.DB)
	ms.NoError(err)

	var dbc models.DBConn
	models.DB.Find(&dbc, dbc0.ID)

	t := dbc.Tables[0]

	ms.Equal(dbc0.ID, t.DB.ID)

	//----------------ЕЙ ОТ ТУКА ПОЧВА ИСТИНСКИЯТ ТЕСТ ГОРНОТО БЕ ГЛАВНО ИНИЦИАЛИЗАЦИЯ НА СРЕДАТА --------------------//
	//Ще търсим  email-a на потребител от базата
	var user models.User
	ms.NoError(ms.DB.First(&user))
	c := models.Column{
		Column: "email",
	}

	//празни identifier-и
	_, err = t.GetCellValue(c, map[string]string{})
	ms.Error(err)
	ms.Equal("no identifiers specified", err.Error())

	//невалидни identifier-и
	_, err = t.GetCellValue(c, map[string]string{
		"provider": "go6o123id",
	})
	ms.Error(err)
	ms.Equal("invalid identifiers", err.Error())

	//всичко 6
	val, err := t.GetCellValue(c, map[string]string{
		"id": user.ID.String(),
	})
	ms.NoError(err)
	ms.Equal("test@gmail.com", val)
}

package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
	"strings"
)

//Изпращане на покана
func SendInvite(c buffalo.Context) error {
	//Взима имейл
	c.Request().ParseForm()
	mail := c.Request().PostForm.Get("email")
	//mail да стане с малки букви
	mail = strings.ToLower(mail)

	u := &models.User{}
	u.Email = nulls.NewString(mail)

	//намира юзъра според имейла
	tx := c.Value("tx").(*pop.Connection)
	exists, err := u.FindByEmail(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if !exists || mail == "" {
		return c.Render(403, r.String(T.Translate(c, "invites.noEmail")))
	}

	//намира сайт по параметъра site_id
	s := &models.Site{}
	if err := tx.Find(s, c.Param("site_id")); err != nil {
		return errors.WithStack(err)
	}

	//Проверява дали юзърът вече не е в сайта
	if in, err := u.IsInSite(*s, tx); err != nil {
		return errors.WithStack(err)
	} else {
		if in == true {
			return c.Render(406, r.String(T.Translate(c, "invites.AlreadyIn")))
		}
	}

	//конвертиране на параметъра site_id от string в uuid.UUID
	siteId, iderr := uuid.FromString(c.Param("site_id"))
	if iderr != nil {
		return errors.WithStack(iderr)
	}

	//създаване на поканата
	i := &models.Invite{}
	i.SiteID = siteId
	i.UserID = u.ID

	//Запазване на поканата
	verrs, err := i.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.String(T.Translate(c, "invites.Sent")))
}

//Зареждане на страницата с поканите
func GetInvitesPage(c buffalo.Context) error {
	//Зарежда нов юзър по променливата current_user
	u := c.Value("current_user").(*models.User)

	//Зарежда всички покана на юзъра по неговото id
	var invites models.Invites
	if err := invites.GetByUser(u.ID); err != nil {
		return errors.WithStack(err)
	}
	//Създаване на променлива за поканите
	c.Set("invites", invites)
	return c.Render(200, r.HTML("user/invites"))
}

//Изтриване/отказване на поканата
func DeleteInvite(c buffalo.Context) error {
	//Зареждане на поканата според параметъра id
	invite_id := c.Param("id")

	i := &models.Invite{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(i, invite_id); err != nil {
		return errors.WithStack(err)
	}

	//Изтриване на поканата
	i.DeleteInvite(tx)

	return c.Render(200, r.String("/invites"))
}

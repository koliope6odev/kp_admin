package actions

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_ResetPassGet() {
	//При не логнат потребител
	res := as.HTML("/auth/reset/password").Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</reset-password>")

	//При логнат потребител
	// Взима потребител от базата
	u := &models.User{
		Name:       nulls.NewString("Колио"),
		Provider:   nulls.NewString("github"),
		ProviderID: nulls.NewString("123"),
		Email:      nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	res = as.HTML("/auth/reset/password").Get()
	as.Equal(302, res.Code)
	//Трябва да редиректва към /home
	as.Equal("/home", res.Header().Get("Location"))
}

func (as *ActionSuite) Test_ResetPassSendMail() {

	as.LoadFixture("lots of users")

	//Несъществуващ email
	res := as.HTML("/auth/reset/password/send/mail").Post(
		&models.User{Email: nulls.NewString("notExisting@mail.com")})

	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Не съществува потребител с такъв Email.")

	//Празен  email
	res = as.HTML("/auth/reset/password/send/mail").Post(
		&models.User{Email: nulls.NewString("notExisting@mail.com")})

	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Не съществува потребител с такъв Email.")

	//Акаунт с github
	u := &models.User{
		Name:       nulls.NewString("Колио"),
		Email:      nulls.NewString("forgotten@mail.com"),
		Provider:   nulls.NewString("github"),
		ProviderID: nulls.NewString("1234reqq"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	res = as.HTML("/auth/reset/password/send/mail").Post(
		&models.User{Email: nulls.NewString("forgotten@mail.com")})

	as.Equal(403, res.Code)
	as.Contains(res.Body.String(),
		"Акаунтът е създаден чрез GitHub акаунт, моля използвайте своя GitHub акаунт за да влезете.")

	err = as.DB.Destroy(u)
	as.NoError(err)
	u = nil

	//Акаунт с linkedIn
	u = &models.User{
		Name:       nulls.NewString("Колио"),
		Email:      nulls.NewString("forgotten@mail.com"),
		Provider:   nulls.NewString("linkedin"),
		ProviderID: nulls.NewString("1234linkedin"),
	}
	err = as.DB.Create(u)
	as.NoError(err)

	res = as.HTML("/auth/reset/password/send/mail").Post(
		&models.User{Email: nulls.NewString("forgotten@mail.com")})

	as.Equal(403, res.Code)
	as.Contains(res.Body.String(),
		"Акаунтът е създаден чрез LinkedIn акаунт, моля използвайте своя LinkedIn акаунт за да влезете.")

	err = as.DB.Destroy(u)
	as.NoError(err)
	u = nil

	//Правилен email и всичко
	u = &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("forgotten@mail.com"),
	}
	err = as.DB.Create(u)
	as.NoError(err)

	res = as.HTML("/auth/reset/password/send/mail").Post(
		&models.User{Email: nulls.NewString("forgotten@mail.com")})

	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "success")

	err = as.DB.Destroy(u)
	as.NoError(err)
	u = nil

	//Твърде много пратени запитвания

	n := 3

	u = &models.User{
		Name:  nulls.NewString("КолиоТвърдеМногоЗаявки"),
		Email: nulls.NewString("forgotten@mail.com"),
	}
	err = as.DB.Create(u)
	as.NoError(err)

	//n пъти трябва да върне 200
	for i := 0; i <= n; i++ {
		res = as.HTML("/auth/reset/password/send/mail").Post(
			&models.User{Email: nulls.NewString("forgotten@mail.com")})

		as.Equal(200, res.Code)
		as.Contains(res.Body.String(), "success")
	}

	//На n+1 път трябва да върне 403
	res = as.HTML("/auth/reset/password/send/mail").Post(
		&models.User{Email: nulls.NewString("forgotten@mail.com")})

	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Твърде много заявки за възстановяване на паролата.")

	err = as.DB.Destroy(u)
	as.NoError(err)
	u = nil

}

func (as *ActionSuite) Test_ResetPassCheckToken() {
	u := &models.User{
		Name:       nulls.NewString("Колио"),
		Email:      nulls.NewString("res@pass.mail"),
		Provider:   nulls.NewString("github"),
		ProviderID: nulls.NewString("1234reqq"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	psr := &models.ResetPass{
		UserID: u.ID,
		Token:  "token123",
	}
	err = as.DB.Create(psr)
	as.NoError(err)

	data := make(map[string]string)
	data["email"] = "res@pass.mail"

	data["token"] = "token123"
	res := as.HTML("/auth/reset/password/check/token").Post(data)
	as.Equal(200, res.Code)

	data["token"] = "notValid"
	res = as.HTML("/auth/reset/password/check/token").Post(data)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Невалиден код.")

}

//Последната стъпка във формата
func (as *ActionSuite) Test_ResetPass() {
	u := &models.User{
		Name:                 nulls.NewString("Колио"),
		Email:                nulls.NewString("res@passq.mail"),
		Password:             "ivan1234321",
		PasswordConfirmation: "ivan1234321",
	}
	_, err := u.Create(models.DB)
	as.NoError(err)

	psr := &models.ResetPass{
		UserID: u.ID,
		Token:  "token123",
	}
	err = as.DB.Create(psr)
	as.NoError(err)

	data := make(map[string]string)
	data["email"] = "res@passq.mail"
	data["password"] = "password"
	data["password_confirmation"] = "password"

	//невалиден код
	data["token"] = "notValid"
	res := as.HTML("/auth/reset/password/change/pass").Post(data)
	as.Equal(403, res.Code)

	//Твърде къда парола
	data["token"] = "token123"
	data["password"] = "pass"
	data["password_confirmation"] = "pass"
	res = as.HTML("/auth/reset/password/change/pass").Post(data)
	as.Equal(403, res.Code)
	data["password"] = "password"
	data["password_confirmation"] = "password"

	//Несъвпадащи пароли
	data["password_confirmation"] = "password––"
	res = as.HTML("/auth/reset/password/change/pass").Post(data)
	as.Equal(403, res.Code)
	data["password_confirmation"] = "password"

	//валиден код
	data["token"] = "token123"
	res = as.HTML("/auth/reset/password/change/pass").Post(data)
	as.Equal(200, res.Code)

	//Повторна промяна с един и същи код
	data["token"] = "token123"
	res = as.HTML("/auth/reset/password/change/pass").Post(data)
	as.Equal(403, res.Code)

}

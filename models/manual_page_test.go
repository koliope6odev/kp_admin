package models_test

import (
	// "fmt"

	// "github.com/gobuffalo/pop/nulls"
	// "github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

//Проверява дали запазва страница с create метода без да хвърля грешки
func (ms *ModelSuite) Test_Manual_Create() {

	m := &models.ManualPage{
		Title:       "Go6o123",
		Text:        "text text text",
		Screenshots: []string{"pesho", "gosho", "kolio"},
	}
	ms.DBDelta(1, "manual_pages", func() {
		_, err := m.Create(models.DB)
		ms.NoError(err)
	})
	m1 := &models.ManualPage{}
	ms.DB.Where("title=?", m.Title).First(m1)
	ms.Equal(m.Screenshots, m1.Screenshots)
}

func (ms *ModelSuite) Test_Manual_GetAllPages() {
	m1 := &models.ManualPage{
		Title: "Title",
		Text:  "text text text",
	}
	m2 := &models.ManualPage{
		Title: "Title2",
		Text:  "text text text2",
	}

	m3 := &models.ManualPage{
		Title: "Title3",
		Text:  "text text text3",
	}

	m4 := &models.ManualPage{
		Title: "Title4",
		Text:  "text text text4",
	}

	models.DB.Save(m1)
	models.DB.Save(m2)
	models.DB.Save(m3)
	models.DB.Save(m4)

	var pages models.ManualPages
	ms.NoError(pages.GetAllPages(), "Хвърля грешка при викане не models.ManualPages.GetAllPages()")

	ms.True(len(pages) >= 4)
}

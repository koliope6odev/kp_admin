package actions

import (
	"encoding/json"
	"fmt"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_TablesGetTblEditLogs() {
	els := models.EditLogs{}
	as.NoError(as.DB.All(&els))
	as.NoError(as.DB.Destroy(&els))
	as.LoadFixture("lots of fictive edit logs")

	var u models.User
	var t models.Table
	var s models.Site

	as.NoError(as.DB.Where("name=?", "editLogTable").First(&t))
	as.NoError(as.DB.Where("name=?", "editLogUserName").First(&u))
	as.NoError(as.DB.Where("name=?", "editLogsSite").First(&s))

	dbc := &models.DBConn{
		ID:       t.DBID,
		Driver:   "postgres",
		Port:     1234,
		Site:     s,
		Name:     "editLogsDBConnName",
		User:     "u",
		Password: "p",
		Host:     "h",
		DB:       "d",
		Tables: models.Tables{
			t,
		},
	}

	verrs, err := dbc.Update(models.DB)
	as.False(verrs.HasAny())
	as.NoError(err)
	as.NoError(as.DB.Update(dbc))

	as.NoError(as.DB.Where("connection_name = ? ", "editLogsDBConnName").First(dbc))

	//логва потребителя
	as.Session.Set("current_user_id", u.ID)

	//Aко не е част от сайт
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/%s/logs/", s.ID, "-")).Get()
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/%s/logs/", s.ID, "-")).Get()
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	//С празно table_id
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/%s/logs/", s.ID, "-")).Get()
	as.Equal(406, res.Code)

	//С невалидно table_id
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/%s/logs/", s.ID, "никола")).Get()
	as.Equal(406, res.Code)

	//С неправилно table_id
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/%s/logs/", s.ID, uuid.Nil)).Get()
	as.Equal(406, res.Code)

	res = as.JSON(fmt.Sprintf("/site/%s/db/table/%s/logs/", s.ID, t.ID)).Get()
	as.Equal(200, res.Code)
	var logs models.EditLogs
	as.NoError(json.Unmarshal(res.Body.Bytes(), &logs))
	as.Equal(8, len(logs))
}

func (as *ActionSuite) Test_TablesRenameTbl() {

	as.DB.Destroy(&models.Users{})
	as.DB.Destroy(&models.Sites{})
	as.DB.Destroy(&models.DBConns{})

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	//логва се
	as.Session.Set("current_user_id", u.ID)

	//Сайта, към който е базата
	site := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("https://localhost:3000")}
	models.DB.Create(&site)

	//postgres
	dbc := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_development",
		Port:     5432,
		Site:     site,
		Name:     "Връзка 123",
		Tables: models.Tables{
			models.Table{Name: "users", Access: "rw", Verbose: nulls.NewString("users_table_to_rename")},
		},
	}

	verrs, err := dbc.Create(models.DB)
	as.NoError(err)
	as.False(verrs.HasAny())

	var t models.Table
	err = models.DB.Where("verbose_name = ?", "users_table_to_rename").First(&t)
	as.NoError(err)

	//Aко не е част от сайт
	res := as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, "-")).Post(0)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&site, as.DB, "rw")
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, "-")).Post(0)
	as.Equal(302, res.Code)
	u.LeaveSite(&site, as.DB)
	u.JoinSite(&site, as.DB, "admin")

	//С празно table_id
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, "-")).Post(0)
	as.Equal(406, res.Code)

	//С невалидно table_id
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, "никола")).Post(0)
	as.Equal(406, res.Code)

	//С неправилно table_id
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, uuid.Nil)).Post(0)
	as.Equal(406, res.Code)

	//Правилната таблица, но твърде късо име
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, t.ID)).Post(map[string]string{
		"verbose": "u",
	})
	as.Equal(403, res.Code)

	//Правилната таблица, но твърде дълго име
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, t.ID)).Post(map[string]string{
		"verbose": "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456",
	})
	as.Equal(403, res.Code)

	//Правилната таблица, но твърде без име
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, t.ID)).Post(map[string]string{
		"verbose": "",
	})
	as.Equal(403, res.Code)

	//Правилната таблица, всичко 6
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/rename/", site.ID, t.ID)).Post(map[string]string{
		"verbose": "users_table_to_rename_EDITED",
	})
	as.Contains(res.Body.String(), "OK")
	as.Equal(200, res.Code)

	//За таблица, която няма право
	//implementirano чрез middleware

}

func (as *ActionSuite) Test_TablesSetTblPKColumn() {

	as.DB.Destroy(&models.Users{})
	as.DB.Destroy(&models.Sites{})
	as.DB.Destroy(&models.DBConns{})
	as.LoadFixture("lots of users")

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	//логва се
	as.Session.Set("current_user_id", u.ID)

	//Сайта, към който е базата
	site := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("https://localhost:3000")}
	models.DB.Create(&site)

	//postgres връзка
	dbc := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
		Site:     site,
		Name:     "Връзка 123456",
		Tables: models.Tables{
			models.Table{Name: "users", Access: "rw", Verbose: nulls.NewString("users_table_to_change_pk")},
		},
	}

	verrs, err := dbc.Create(models.DB)
	as.NoError(err)
	as.False(verrs.HasAny())

	var t models.Table
	err = models.DB.Where("verbose_name = ?", "users_table_to_change_pk").First(&t)
	as.NoError(err)

	// -------  OT ТУК ПОЧВА СЪЩИНСКИЯ ТЕСТ -------- //

	//Aко не е част от сайт
	res := as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, "-")).Post(0)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))

	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&site, as.DB, "rw")
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, "-")).Post(0)
	as.Equal(302, res.Code)
	u.LeaveSite(&site, as.DB)
	u.JoinSite(&site, as.DB, "admin")

	//С празно table_id
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, "-")).Post(0)
	as.Equal(406, res.Code)

	//С невалидно table_id
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, "никола")).Post(0)
	as.Equal(406, res.Code)

	//С неправилно table_id
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, uuid.Nil)).Post(0)
	//as.Equal(404, res.Code)

	//Пробва да зададе колона, която не съществува
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, t.ID)).Post(map[string]string{
		"pk_column": "invalid_column",
	})
	as.Equal(406, res.Code)

	//Пробва да зададе колона, която не е уникална
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, t.ID)).Post(map[string]string{
		"pk_column": "email",
	})
	as.Equal(403, res.Code)

	//всичко 6
	res = as.HTML(fmt.Sprintf("/site/%s/db/table/%s/set/pk/column", site.ID, t.ID)).Post(map[string]string{
		"pk_column": "id",
	})
	as.Equal(200, res.Code)
	as.NoError(models.DB.Where("verbose_name = ?", "users_table_to_change_pk").First(&t))
	as.Equal("id", t.PkColumn.String)

}

package actions

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

//Тест за изпращане на покана
func (as *ActionSuite) Test_InvitesSend() {
	data := make(map[string]string)
	res := as.HTML("/site/join/send/invite/uids").Put(data)
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	//Aко е логнат
	// Взима потребител от базата
	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)
	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	//Aко не е част от сайта
	res = as.HTML("/site/join/send/invite/%v", s.ID).Put(data)
	as.Equal(302, res.Code)

	u.JoinSite(s, as.DB, "admin")

	data["email"] = "invalid@not.valid"

	res = as.HTML("/site/join/send/invite/%v", s.ID).Put(data)
	as.Equal(403, res.Code)
	as.Equal("Не съществува потребител с подобен E-mail.", res.Body.String())

	data1 := make(map[string]string)
	data1["invite_id"] = "false"

	res = as.HTML("/site/join/%v", s.ID).Put(data1)
	as.Equal(302, res.Code)

	data2 := make(map[string]string)
	data2["email"] = "test@gmail.test"

	res = as.HTML("/site/join/send/invite/%v", s.ID).Put(data2)
	as.Equal(406, res.Code)
	as.Equal("Потребителят вече е част от сайта.", res.Body.String())

	u1 := &models.User{
		Name: nulls.NewString("Колио1"),

		Email: nulls.NewString("test1@gmail.test"),
	}
	err = as.DB.Create(u1)
	as.NoError(err)

	data3 := make(map[string]string)
	data3["email"] = "test1@gmail.test"

	res = as.HTML("/site/join/send/invite/%v", s.ID).Put(data3)

	as.Equal(200, res.Code)
	as.Equal(res.Body.String(), "Поканата е успешно изпратена!")
}

//Тест на зареждането на страницата за покани
func (as *ActionSuite) Test_InvitesGetPage() {
	res := as.HTML("/user/invites/").Get()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	res = as.HTML("/invites/").Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "</invites>")
}

//Тест за изтриване на поканата
func (as *ActionSuite) Test_InvitesDelete() {
	res := as.HTML("/invites/delete/1213").Delete()
	as.Equal(302, res.Code)
	as.Equal("/auth/login", res.Header().Get("Location"))

	u := &models.User{
		Name: nulls.NewString("Колио"),

		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	s := &models.Site{}
	s.Name = nulls.NewString("Name")
	s.Desc = nulls.NewString("Description")
	s.Link = nulls.NewString("https://link.bg")

	err = as.DB.Create(s)
	as.NoError(err)

	i := &models.Invite{
		SiteID: s.ID,
		UserID: u.ID,
	}

	err = as.DB.Create(i)
	as.NoError(err)

	res = as.HTML("/invites/delete/%v", i.ID).Delete()
	as.Equal(200, res.Code)
	as.Equal(res.Body.String(), "/invites")
}

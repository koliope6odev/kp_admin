package models_test

// КОМЕНТИРАНО Е, ЗАЩОТО
// КОГАТО СЕ ПУСКАТ ВСИЧКИ ТЕСТОВЕ НА ПРИЛОЖЕНИЕТО
// ОТНЕМА ТВЪРДЕ МНОГО ВРЕМЕ
// ЗА ИЗПЪЛНЕНИЕ
// ЗА ДА ТЕСТВАШ НЕЩА
// СВРЪЗАНИ С ВРЪЗКИ ТЕ ЗА БАЗИ ДАННИ
// ОТКОМЕНТИРАЙ !

//import (
//	"database/sql"
//	"fmt"
//	"github.com/gobuffalo/pop/nulls"
//	"github.com/gobuffalo/uuid"
//	"github.com/kolioPesho/kp_admin/models"
//	"testing"
//)
//
//import _ "github.com/go-sql-driver/mysql"
//
//func (ms *ModelSuite) TestDBConnM() {
//
//	dbc := models.DBConn{}
//	dbc.Driver = "mysql"
//	dbc.Host = "194.135.94.89"
//	dbc.User = "kp_admin"
//	dbc.Password = "nikolape6o"
//	dbc.DB = "zora"
//	dbc.Port = 3306
//
//	//[username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
//	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@(%s:%d)/%s",
//		dbc.User, dbc.Password, dbc.Host, dbc.Port, dbc.DB))
//	defer db.Close()
//
//	err = db.Ping()
//	ms.NoError(err)
//
//	err = models.DB.Create(&dbc)
//	ms.NoError(err)
//
//	//CREATE USER 'kp_admin'@'localhost' IDENTIFIED BY 'nikolape6o';
//	//CREATE USER 'kp_admin'@'%' IDENTIFIED BY 'nikolape6o';
//	//GRANT ALL ON *.* TO 'kp_admin'@'localhost';
//	//GRANT ALL ON *.* TO 'kp_admin'@'%';
//
//	//GRANT ALL PRIVILEGES ON *.* TO 'kp_admin'@'%' IDENTIFIED BY 'nikolape6o' WITH GRANT OPTION; FLUSH PRIVILEGES;
//}
//
//func (ms *ModelSuite) TestDBConnP() {
//
//	dbc := models.DBConn{}
//	dbc.Driver = "postgres"
//	dbc.Host = "127.0.0.1"
//	dbc.User = "postgres"
//	dbc.Password = "postgres"
//	dbc.DB = "kp_admin_development"
//	dbc.Port = 5432
//
//	//[username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
//	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s "+
//		"password=%s dbname=%s sslmode=disable",
//		dbc.Host, dbc.Port, dbc.User, dbc.Password, dbc.DB))
//	defer db.Close()
//
//	err = db.Ping()
//	ms.NoError(err)
//
//	err = models.DB.Create(&dbc)
//	ms.NoError(err)
//
//}
//
//func (ms *ModelSuite) TestDBConn_CheckConnection() {
//	//Валидно
//	dbc0 := models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//	}
//
//	//Невалидно
//	dbc1 := models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin123",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//	}
//
//	ok, _ := dbc0.CheckConnection()
//	ms.True(ok)
//
//	ok, _ = dbc1.CheckConnection()
//	ms.False(ok)
//}
//
//func (ms *ModelSuite) TestDBConn_GetTables() {
//	//MySQL
//	dbc := models.DBConn{}
//	dbc.Driver = "mysql"
//	dbc.Host = "194.135.94.89"
//	dbc.User = "kp_admin"
//	dbc.Password = "nikolape6o"
//	dbc.DB = "zora"
//	dbc.Port = 3306
//
//	tables, err := dbc.GetTables()
//	ms.NoError(err)
//	ms.NotEmpty(tables)
//	ms.Len(tables, 10)
//
//	//PostgreSQL
//	dbc.Driver = "postgres"
//	dbc.Host = "localhost"
//	dbc.User = "postgres"
//	dbc.Password = "postgres"
//	dbc.DB = "kp_admin_development"
//	dbc.Port = 5432
//
//	tables, err = dbc.GetTables()
//	ms.NoError(err)
//	ms.NotEmpty(tables)
//
//}
//
//func (ms *ModelSuite) TestDBConn_GetColumns() {
//	dbc := models.DBConn{}
//	dbc.Driver = "mysql"
//	dbc.Host = "194.135.94.89"
//	dbc.User = "kp_admin"
//	dbc.Password = "nikolape6o"
//	dbc.DB = "zora"
//	dbc.Port = 3306
//
//	columns, err := dbc.GetColumns("users")
//
//	ms.NoError(err)
//	ms.NotEmpty(columns)
//	ms.Len(columns, 15)
//	var colStrings []string
//	for _, e := range columns {
//		ms.NotNil(e.DataType)
//		colStrings = append(colStrings, e.Column)
//	}
//	ms.Contains(colStrings, "id")
//	ms.Contains(colStrings, "username")
//	ms.Contains(colStrings, "password")
//
//	dbc = models.DBConn{}
//	dbc.Driver = "postgres"
//	dbc.Host = "localhost"
//	dbc.User = "postgres"
//	dbc.Password = "postgres"
//	dbc.DB = "kp_admin_development"
//	dbc.Port = 5432
//
//	columns, err = dbc.GetColumns("databases")
//
//	ms.NoError(err)
//	ms.NotEmpty(columns)
//	colStrings = []string{}
//	for _, e := range columns {
//		ms.NotNil(e.DataType)
//		colStrings = append(colStrings, e.Column)
//	}
//	ms.Contains(colStrings, "id")
//	ms.Contains(colStrings, "host")
//	ms.Contains(colStrings, "port")
//	ms.Contains(colStrings, "connection_name")
//}
//
//func (ms *ModelSuite) TestDBConn_Create() {
//
//	tablesM := append(models.Tables{},
//		models.Table{Name: "TableMySQL1", Access: "rw"},
//		models.Table{Name: "TableMySQL2", Access: "rw"},
//		models.Table{Name: "TableMySQL3", Access: "rw"},
//		models.Table{Name: "TableMySQL4", Access: "rw"},
//	)
//	tablesP := append(models.Tables{},
//		models.Table{Name: "TablePostgreSQL1", Access: "rw"},
//		models.Table{Name: "TablePostgreSQL2", Access: "rw"},
//		models.Table{Name: "TablePostgreSQL3", Access: "rw"},
//	)
//
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&site)
//
//	//mysql
//	dbcM := &models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//		Tables:   tablesM,
//		Site:     site,
//		Name:     "conn mysql",
//	}
//
//	//postgres
//	dbcP := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Tables:   tablesP,
//		Site:     site,
//		Name:     "conn postgres",
//	}
//
//	//mySQL
//	_, err := dbcM.Create(models.DB)
//	ms.NoError(err)
//
//	tables := models.Tables{}
//	n, _ := models.DB.Where("db=?", dbcM.ID).Count(tables)
//	ms.Equal(4, n)
//	ms.NotNil(dbcM.SiteID)
//
//	//PostgreSQL
//	_, err = dbcP.Create(models.DB)
//
//	ms.NoError(err)
//	n, _ = models.DB.Where("db=?", dbcP.ID).Count(tables)
//	ms.Equal(3, n)
//	ms.NotEqual(dbcP.SiteID, uuid.Nil)
//	fmt.Println(dbcP.UserEnc)
//}
//
//func (ms *ModelSuite) TestDBConn_Update() {
//
//	tablesM := append(models.Tables{},
//		models.Table{Name: "SavedTableMySQL1", Access: "rw"},
//		models.Table{Name: "SavedTableMySQL2", Access: "rw"},
//		models.Table{Name: "SavedTableMySQL3", Access: "rw"},
//		models.Table{Name: "SavedTableMySQL4", Access: "rw"},
//	)
//	tablesP := append(models.Tables{},
//		models.Table{Name: "SavedTablePostgreSQL1", Access: "rw"},
//		models.Table{Name: "SavedTablePostgreSQL2", Access: "rw"},
//		models.Table{Name: "SavedTablePostgreSQL3", Access: "rw"},
//	)
//
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&site)
//
//	//mysql
//	dbcM := &models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//		Tables:   tablesM,
//		Site:     site,
//		Name:     "conn mysql",
//	}
//
//	//postgres
//	dbcP := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Tables:   tablesP,
//		Site:     site,
//		Name:     "conn postgres",
//	}
//
//	//mySQL
//	_, err := dbcM.Create(models.DB)
//	ms.NoError(err)
//	tables := models.Tables{}
//	n, _ := models.DB.Where("db=?", dbcM.ID).Count(tables)
//	ms.Equal(4, n)
//	ms.NotNil(dbcM.SiteID)
//
//	//PostgreSQL
//	_, err = dbcP.Create(models.DB)
//
//	ms.NoError(err)
//	n, _ = models.DB.Where("db=?", dbcP.ID).Count(tables)
//	ms.Equal(3, n)
//	ms.NotEqual(dbcP.SiteID, uuid.Nil)
//
//	//Ей от тука почва истинския тест
//	dbcM.Tables = models.Tables{
//		models.Table{Name: "SavedTableMySQL1", Access: "rw"},
//		models.Table{Name: "SavedTableMySQL3", Access: "rw"},
//		models.Table{Name: "SavedTableMySQL4", Access: "rw"},
//		models.Table{Name: "NewTableMySQL1", Access: "rw"},
//		models.Table{Name: "NewTableMySQL2", Access: "rw"},
//	}
//	_, err = dbcM.Update(models.DB)
//	ms.NoError(err)
//	n, _ = models.DB.Where("db=?", dbcM.ID).Count(tables)
//	ms.Equal(5, n)
//
//	dbcP.Tables = models.Tables{
//		models.Table{Name: "SavedTablePostgreSQL1", Access: "rw"},
//		models.Table{Name: "NewTablePostgreSQL0", Access: "rw"},
//		models.Table{Name: "NewTablePostgreSQL2", Access: "rw"},
//		models.Table{Name: "NewTablePostgreSQL3", Access: "rw"},
//		models.Table{Name: "NewTablePostgreSQL4", Access: "rw"},
//		models.Table{Name: "NewTablePostgreSQL5", Access: "rw"},
//		models.Table{Name: "NewTablePostgreSQL6", Access: "rw"},
//	}
//	_, err = dbcP.Update(models.DB)
//	ms.NoError(err)
//	n, _ = models.DB.Where("db=?", dbcP.ID).Count(tables)
//	ms.Equal(7, n)
//
//}
//
//func (ms *ModelSuite) TestDBConn_AfterFind() {
//	//Запазва запис, който да дръпне после
//	tables := append(models.Tables{},
//		models.Table{Name: "Table01", Access: "rw"},
//		models.Table{Name: "Table02", Access: "rw"},
//		models.Table{Name: "Table03", Access: "rw"},
//		models.Table{Name: "Table04", Access: "rw"},
//		models.Table{Name: "Table05", Access: "rw"},
//		models.Table{Name: "Table06", Access: "rw"},
//	)
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&site)
//
//	dbc0 := &models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//		Tables:   tables,
//		Site:     site,
//	}
//
//	_, err := dbc0.Create(models.DB)
//
//	ms.NoError(err)
//
//	var dbc models.DBConn
//	models.DB.Find(&dbc, dbc0.ID)
//
//	ms.Equal("kp_admin", dbc.User)
//	ms.Equal("194.135.94.89", dbc.Host)
//	ms.Equal("nikolape6o", dbc.Password)
//	ms.Equal("zora", dbc.DB)
//	ms.NotEmpty(dbc.Tables)
//	ms.Len(dbc.Tables, 6)
//
//}
//
//func (ms *ModelSuite) TestDBConn_BeforeDestroy() {
//	//Уверява се че трие таблиците и диаграмите заедно с връзката към база данни
//
//	ms.DB.Destroy(&models.Tables{})
//
//	tbls := models.Tables{
//		{Name: "Table01", Access: "rw"},
//		{Name: "Table02", Access: "rw"},
//		{Name: "Table03", Access: "rw"},
//	}
//	dbc := models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//		Tables:   tbls,
//	}
//	ms.DBDelta(3, "tables", func() {
//		ms.NoError(ms.DB.Save(&dbc))
//	})
//
//	ms.NoError(ms.DB.Save(&models.Chart{
//		Verbose:        "Chart1",
//		Table:          "Table11",
//		Column:         "Kolumn1",
//		Type:           "bar",
//		StatisticsType: "timeCount",
//		DB:             dbc,
//		SiteID:         uuid.UUID{},
//	}))
//
//	ms.NoError(ms.DB.Save(&models.Chart{
//		Verbose:        "Chart2",
//		Table:          "Table11",
//		Column:         "Kolumn1",
//		Type:           "bar",
//		StatisticsType: "timeCount",
//		DB:             dbc,
//		SiteID:         uuid.UUID{},
//	}))
//
//	ms.DBDelta(-2, "charts", func() {
//		ms.DB.Destroy(&dbc)
//	})
//
//	tbls = models.Tables{}
//	ms.DB.Where("db=?", dbc.ID).All(&tbls)
//	ms.Len(tbls, 0)
//
//}
//
//func (ms *ModelSuite) TestDBConn_GetConns() {
//	s := models.Site{
//		Name: nulls.NewString("site_with_multiple_conns"),
//		Desc: nulls.NewString("site_with_multiple_conns desc"),
//		Link: nulls.NewString("site_with_multiple_conns desc"),
//	}
//	models.DB.Create(&s)
//	for i := 1; i <= 15; i++ {
//		d := "postgres"
//		if i%2 == 0 {
//			d = "mysql"
//		}
//		dbc := models.DBConn{
//			Site:     s,
//			Name:     fmt.Sprintf("db_conn#%d", i),
//			Password: fmt.Sprintf("pass#%d", i),
//			User:     fmt.Sprintf("user#%d", i),
//			Host:     fmt.Sprintf("host#%d", i),
//			DB:       fmt.Sprintf("database#%d", i),
//			Driver:   d,
//			Port:     5432,
//		}
//		dbc.Create(models.DB)
//	}
//
//	var dbcs models.DBConns
//	err := dbcs.GetConns(models.DB, s)
//	ms.NoError(err)
//	ms.Len(dbcs, 15)
//	ms.Contains(fmt.Sprintf("%v", dbcs), "postgres")
//	ms.Contains(fmt.Sprintf("%v", dbcs), "mysql")
//	ms.Contains(fmt.Sprintf("%v", dbcs), "database#1")
//	ms.Contains(fmt.Sprintf("%v", dbcs), "database#15")
//
//}
//
////BENCHMARK-ОВЕ ЗА DBConn_Create
////buffalo test ./models/ -bench=. -cover
//func BenchmarkDBConn_Create3(b *testing.B) {
//	benchmarkCreate(3, b)
//}
//func BenchmarkDBConn_Create10(b *testing.B) {
//	benchmarkCreate(10, b)
//}
//func BenchmarkDBConn_Create30(b *testing.B) {
//	benchmarkCreate(30, b)
//}
//func BenchmarkDBConn_Create100(b *testing.B) {
//	benchmarkCreate(100, b)
//}
//
//func BenchmarkDBConn_AfterUpdate3_9(b *testing.B) {
//	benchmarkUpdate(3, 9, b)
//}
//
//func BenchmarkDBConn_AfterUpdate10_90(b *testing.B) {
//	benchmarkUpdate(10, 90, b)
//}
//
//func BenchmarkDBConn_AfterUpdate10_11(b *testing.B) {
//	benchmarkUpdate(10, 11, b)
//}
//
////benchmarkCreate ntbls - брой таблици
//func benchmarkCreate(ntbls int, b *testing.B) {
//	s := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&s)
//
//	for n := 0; n < b.N; n++ {
//
//		tbls := models.Tables{}
//
//		for k := 0; k < ntbls; k++ {
//			tbls = append(tbls,
//				models.Table{Name: fmt.Sprintf("Benchmarktable_%d_ot_%d", k, ntbls), Access: "rw"},
//			)
//		}
//
//		dbc := &models.DBConn{
//			Driver:   "mysql",
//			Host:     "benchmarkCreate",
//			User:     "benchmarkCreateU",
//			Password: "benchmarkCreateP",
//			DB:       "zora",
//			Port:     3306,
//			Tables:   tbls,
//			Site:     s,
//			Name:     fmt.Sprintf("benchmark conn %d", n),
//		}
//
//		_, err := dbc.Create(models.DB)
//
//		if err != nil {
//			b.Fatal(fmt.Sprintf("dbc.Create() хвърля грешка %v", err))
//		}
//
//		tables := models.Tables{}
//		n, _ := models.DB.Where("db=?", dbc.ID).Count(tables)
//		if n != ntbls {
//			b.Fatal(fmt.Sprintf("Не е записало правилния брой таблици в таблица \"tables\" - %d", n))
//		}
//		if dbc.SiteID == uuid.Nil {
//			b.Fatal("Не е задало siteID")
//		}
//
//	}
//}
//
////benchmarkCreate ntbls - брой таблици
//func benchmarkUpdate(ntbls, ntblsUpdate int, b *testing.B) {
//	s := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&s)
//
//	for n := 0; n < b.N; n++ {
//
//		tbls := models.Tables{}
//
//		for k := 0; k < ntbls; k++ {
//			tbls = append(tbls,
//				models.Table{Name: fmt.Sprintf("Benchmarktable_%d_ot_%d", k, ntbls), Access: "rw"},
//			)
//		}
//
//		dbc := &models.DBConn{
//			Driver:   "mysql",
//			Host:     "benchmarkCreate",
//			User:     "benchmarkCreateU",
//			Password: "benchmarkCreateP",
//			DB:       "zora",
//			Port:     3306,
//			Tables:   tbls,
//			Site:     s,
//			Name:     fmt.Sprintf("benchmark update conn %d", n),
//		}
//
//		_, err := dbc.Create(models.DB)
//
//		if err != nil {
//			b.Fatal(fmt.Sprintf("dbc.Create() хвърля грешка %v", err))
//		}
//
//		tables := models.Tables{}
//		n, _ := models.DB.Where("db=?", dbc.ID).Count(tables)
//		if n != ntbls {
//			b.Fatal(fmt.Sprintf("Не е записало правилния брой таблици в таблица \"tables\" - %d", n))
//		}
//		if dbc.SiteID == uuid.Nil {
//			b.Fatal("Не е задало siteID")
//		}
//
//		//Таблици за промяна
//		tbls = models.Tables{}
//		for k := 0; k < ntblsUpdate; k++ {
//			tbls = append(tbls,
//				models.Table{Name: fmt.Sprintf("Benchmarktable_%d_ot_%d", k, ntbls), Access: "rw"},
//			)
//		}
//
//		dbc.Tables = tbls
//		verrs, err := dbc.Update(models.DB)
//		if verrs.HasAny() {
//			b.Fatal(fmt.Sprintf("Има verrs %v", verrs))
//		}
//		if err != nil {
//			b.Fatal(fmt.Sprintf("dbc.Update() хвърля грешка %v", err))
//		}
//
//	}
//}

package actions

//-------------------------------------------------------
// Връща най-първата (welcome) страницата на приложението
//-------------------------------------------------------

import (
	"github.com/gobuffalo/buffalo"
)

// WelcomeWelcomeHandler връща welcome/welcome.html
func WelcomeHandler(c buffalo.Context) error {
	return c.Render(200, r.HTML("welcome/welcome.html", "layouts/welcome.html"))
}

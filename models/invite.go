package models

// ----------------------------------
//
//  Модел за поканите за присъединяване
//
// ----------------------------------

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

type Invite struct {
	ID        uuid.UUID `db:"id" json:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	SiteID uuid.UUID `db:"site_id"`      //id на сайта към който е поканата
	Site   Site      `belongs_to:"site"` //сайт към който е поканата
	UserID uuid.UUID `db:"user_id"`      //id на потребител към който е поканата
	User   User      `belongs_to:"user"` //потребител към който е поканата
}

type Invites []Invite

func (i Invite) String() string {
	ju, _ := json.Marshal(i)
	return string(ju)
}

//Create wrapper за създаване на покани
func (i *Invite) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(i)
}

//Update wrapper за редактиране на покани
func (i *Invite) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(i)
}

//Зареждане на всички покани към един потребител
func (i *Invites) GetByUser(user_id uuid.UUID) error {
	var invites []Invite
	if err := DB.Eager().Where("user_id = ?", user_id).All(&invites); err != nil {
		return errors.WithStack(err)
	}

	//Временна променлива
	ii := *i
	for _, invite := range invites {
		ii = append(ii, invite)
	}
	*i = ii

	return nil
}

//Изтриване на покана
func (i *Invite) DeleteInvite(tx *pop.Connection) error {
	if err := tx.Destroy(i); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (i *Invite) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (i *Invite) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

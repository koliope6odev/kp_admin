package actions

import (
	"encoding/json"
	"fmt"
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_Cells_RowEditData() {

	as.LoadFixture("login user")
	as.LoadFixture("lots of users")

	var u models.User     //логнатия потребител
	var editU models.User //потребител, чиито запис в базата ще променяме
	var s models.Site     //сайта на потребителя
	var c models.Column   //Колоната, която ще редактираме

	//Колоната, която ще редактираме
	c = models.Column{
		Column:           "email",
		DataTypeReadOnly: false,
		ReadOnly:         false,
		Verbose:          nulls.NewString("emailColumnTestEditCell"),
	}

	as.NoError(as.DB.First(&editU))

	//Взима потребител и сайт
	as.NoError(as.DB.Where("name=?", "login_user").First(&u))
	as.NoError(as.DB.Where("link=?", "test.site.created.by.login.user.ficture").First(&s))
	u.JoinSite(&s, as.DB, "admin")

	//логва потребителя
	as.Session.Set("current_user_id", u.ID)

	//създава връзка към DB (postgres)

	tbls := models.Tables{
		{
			Name:     "users",
			Verbose:  nulls.NewString("cellEditUsersTable"),
			Access:   "rw",
			Columns:  models.Columns{c},
			PkColumn: nulls.NewString("id"),
		},
	}

	dbc := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
		Tables:   tbls,
		Site:     s,
		Name:     "conn postgres cell edit test",
	}
	verrs, err := dbc.Create(as.DB)
	as.NoError(err)
	as.False(verrs.HasAny())

	as.NoError(as.DB.Where("verbose_name=?", "cellEditUsersTable").First(&tbls[0]))
	tbls[0].Columns = models.Columns{c}

	//Запазва колоната и си я намира
	as.NoError(tbls[0].SaveColumns(as.DB))
	as.NoError(as.DB.Where("verbose_name=?", "emailColumnTestEditCell").First(&c))

	identifiersJSON, err := json.Marshal(map[string]string{
		"id":       editU.ID.String(),
		"provider": editU.Provider.String,
	})

	putData := map[string]string{
		"column_id":   c.ID.String(),
		"identifiers": string(identifiersJSON),
		"new_value":   "new_edited@email.com",
	}
	url := fmt.Sprintf("/site/%v/db/table/update/cell/data", s.ID)

	//празен PUT
	res := as.HTML(url).Put("")
	as.Equal(406, res.Code)
	as.Equal("invalid id", res.Body.String())

	//Невалидно id
	putData["column_id"] = "invalid"
	res = as.HTML(url).Put(putData)
	as.Equal(406, res.Code)
	as.Equal("invalid id", res.Body.String())

	//id на несъществуваща колона
	putData["column_id"] = tbls[0].ID.String()
	res = as.HTML(url).Put(putData)
	as.Equal(406, res.Code)
	as.Equal("wrong column", res.Body.String())

	//id на колона от друг сайт
	alienColumn := models.Column{
		Column: "email",
	}
	as.DB.Save(&alienColumn)

	putData["column_id"] = alienColumn.ID.String()
	res = as.HTML(url).Put(putData)
	as.Equal(406, res.Code)
	as.Equal("wrong column", res.Body.String())

	//всичко ок с колоната
	putData["column_id"] = c.ID.String()
	//res = as.HTML(url).Put(putData)
	//fmt.Println(res.Body.String())
	//as.Equal(200, res.Code)

	//невалидни identifier-и
	putData["identifiers"] = "ivan123_totally_invalidJSON}}"
	res = as.HTML(url).Put(putData)
	as.Equal(406, res.Code)
	as.Equal("invalid identifiers", res.Body.String())

	//празни identifier-и
	putData["identifiers"] = "{}"
	res = as.HTML(url).Put(putData)
	as.Equal(406, res.Code)
	as.Equal("invalid identifiers", res.Body.String())

	//----------  Всичко 6 --------------- //

	putData["identifiers"] = string(identifiersJSON)
	as.DBDelta(1, "edit_logs", func() {
		res = as.HTML(url).Put(putData)
		as.Equal(200, res.Code)

		//проверка, дали реално променя базата
		as.NoError(as.DB.Find(&editU, editU.ID))
		as.Equal("new_edited@email.com", editU.Email.String)

	})

	putData["new_value"] = "123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-"
	//Ако се опитам да въведа данни, които не да приемливи за базата (примерно над 255)
	as.DBDelta(0, "edit_logs", func() {
		res = as.HTML(url).Put(putData)
		as.Equal(403, res.Code)
	})

}

func (as *ActionSuite) Test_CellGetLogs() {
	as.LoadFixture("lots of fictive edit logs")
	as.LoadFixture("login user")
	as.LoadFixture("lots of users")

	var u models.User     //логнатия потребител
	var editU models.User //потребител, чиито запис в базата e бил променен
	var s models.Site     //сайта на потребителя
	var c models.Column   //Колоната, за която ще се търсят логове

	as.NoError(as.DB.First(&editU))

	//Взима потребител и сайт
	as.NoError(as.DB.Where("name=?", "login_user").First(&u))
	as.NoError(as.DB.Where("verbose_name=?", "editLogColumn").First(&c))
	as.NoError(as.DB.Where("link=?", "test.site.created.by.login.user.ficture").First(&s))
	u.JoinSite(&s, as.DB, "admin")

	//логва потребителя
	as.Session.Set("current_user_id", u.ID)

	//създава връзка към DB (postgres)

	tbls := models.Tables{
		{
			Name:     "users",
			Verbose:  nulls.NewString("cellEditUsersTable"),
			Access:   "rw",
			Columns:  models.Columns{c},
			PkColumn: nulls.NewString("id"),
		},
	}

	dbc := &models.DBConn{
		Driver:   "postgres",
		Host:     "localhost",
		User:     "postgres",
		Password: "postgres",
		DB:       "kp_admin_test",
		Port:     5432,
		Tables:   tbls,
		Site:     s,
		Name:     "conn postgres cell get logs test",
	}
	verrs, err := dbc.Create(as.DB)
	as.NoError(err)
	as.False(verrs.HasAny())

	as.NoError(as.DB.Where("verbose_name=?", "cellEditUsersTable").First(&tbls[0]))
	tbls[0].Columns = models.Columns{c}

	//Запазва колоната и си я намира
	as.NoError(tbls[0].SaveColumns(as.DB))
	as.NoError(as.DB.Where("verbose_name=?", "editLogColumn").First(&c))

	c.TableID = tbls[0].ID
	as.NoError(as.DB.Save(&c))

	//---- OT ТУКА СИ ПОЧВА БАШ ТЕСТА ----- //

	PostData := map[string]string{
		"column_id": c.ID.String(),
		"pk_value":  "8",
		"pk_column": "id",
	}
	url := fmt.Sprintf("/site/%v/db/table/get/cell/edit/log", s.ID)

	//празен Post
	res := as.HTML(url).Post("")
	as.Equal(406, res.Code)
	as.Equal("invalid id", res.Body.String())

	//Невалидно id
	PostData["column_id"] = "invalid"
	res = as.HTML(url).Post(PostData)
	as.Equal(406, res.Code)
	as.Equal("invalid id", res.Body.String())

	//id на несъществуваща колона
	PostData["column_id"] = tbls[0].ID.String()
	res = as.HTML(url).Post(PostData)
	as.Equal(406, res.Code)
	as.Equal("wrong column", res.Body.String())

	//id на колона от друг сайт
	alienColumn := models.Column{
		Column: "email",
	}
	as.DB.Save(&alienColumn)

	PostData["column_id"] = alienColumn.ID.String()
	res = as.HTML(url).Post(PostData)
	as.Equal(406, res.Code)
	as.Equal("wrong column", res.Body.String())

	//всичко ок
	PostData["column_id"] = c.ID.String()
	res = as.HTML(url).Post(PostData)
	as.Equal(200, res.Code)

}

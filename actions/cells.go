package actions

//----------------------------------------
// Контролер, който се грижи за
// нащата свързани с клеките в таблиците
// реадктиране на данни и нещата свързани с
// edit log-овете
//-----------------------------------------

import (
	"encoding/json"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

// CellsRowEditData грижи се за редакцията на данни в някоя клетка
func CellsRowEditData(c buffalo.Context) error {

	//иска: column_id,new_value,  identifiers

	tx := models.DB // c.Value("tx").(*pop.Connection)

	column, err := cellsGetColumn(c, tx)
	if err != nil {
		return c.Render(406, r.String(err.Error()))
	}

	//Взима identifier-ите за WHERE частта
	var identifs map[string]string
	err = json.Unmarshal([]byte(c.Request().PostForm.Get("identifiers")), &identifs)
	if err != nil || len(identifs) == 0 {
		return c.Render(406, r.String("invalid identifiers"))
	}

	//проверява, дали има зададена pk колона (за запразването на editLog-a)
	pkColumn := column.Table.PkColumn.String
	if _, ok := identifs[pkColumn]; !ok {
		return c.Render(406, r.String("invalid primary key column"))
	}

	//Взима new_val
	newVal := c.Request().PostForm.Get("new_value")

	//намира старата стойност (връзка към отдалечената база)
	oldVal, err := column.Table.GetCellValue(column, identifs)
	if err != nil {
		return c.Render(406, r.String("cannot find the previous value of the cell"))
	}

	//Създава и запазва edit log-a
	el := models.EditLog{
		Column:   column,
		User:     *c.Value("current_user").(*models.User),
		OldVal:   oldVal,
		NewVal:   newVal,
		PKColumn: pkColumn,
		PKValue:  identifs[pkColumn],
	}

	//Проверява, дали PK колоната съдържа само уникални стойности
	ok, err := el.CheckPKColumn()
	if err != nil {
		return c.Render(406, r.String("invalid table pk column"))
	}
	if !ok {
		return c.Render(403, r.String(T.Translate(c, "site.pkNotUnique")))
	}

	//Запазва edit log-a в нашата база
	verrs, err := el.Create(tx)

	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs.Errors))
	}

	//Изпълнява UPDATE заявка
	err = column.Table.EditCell(column, newVal, identifs)
	if err != nil {
		tx.Destroy(&el)
		return c.Render(403, r.String(err.Error()))
	}

	return c.Render(200, r.String("successfully edited!"))
}

//CellGetLogs връща всички промени на данните в някоя клетка, които сме запазили
func CellGetLogs(c buffalo.Context) error {
	//иска column_id, pk_column, pk_value

	tx := models.DB //c.Value("tx").(*pop.Connection)

	//Намира и валидира колоната на база column_id от request-a
	column, err := cellsGetColumn(c, tx)
	if err != nil {
		return c.Render(406, r.String(err.Error()))
	}

	//Взима стойностите за primary key ключа
	pkCol := c.Request().PostForm.Get("pk_column")
	pkVal := c.Request().PostForm.Get("pk_value")

	//намира edit log-oве
	els := models.EditLogs{}
	if err := els.FindLogs(tx, column, pkCol, pkVal); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(els))
}

// cellsGetColumn функциийка, която връща колоната на база column_id ( от put request-a )
// прави и всички нужни проверки, дали колоната съществува и дали е от сайта
func cellsGetColumn(c buffalo.Context, tx *pop.Connection) (models.Column, error) {
	var err error
	column := models.Column{}
	column.ID, err = uuid.FromString(c.Request().PostForm.Get("column_id"))

	//Aко id-то на колоната не е валидно uuid
	if err != nil {
		return models.Column{}, errors.New("invalid id")
	}

	//Aко не съществува колона с такова id
	err = tx.Eager().Find(&column, column.ID)
	if err != nil || column.ID == uuid.Nil {
		return models.Column{}, errors.New("wrong column")
	}

	//проверка, дали колоната е от сайта
	if err := tx.Find(&column.Table, column.TableID); err != nil {
		return models.Column{}, errors.New("wrong column")
	}
	if err := tx.Find(&column.Table.DB, column.Table.DBID); err != nil {
		return models.Column{}, errors.New("wrong column")
	}
	if err := tx.Find(&column.Table.DB.Site, column.Table.DB.SiteID); err != nil {
		return models.Column{}, errors.New("wrong column")
	}
	if column.Table.DB.Site.ID.String() != c.Param("site_id") {
		return models.Column{}, errors.New("wrong column")
	}

	return column, nil
}

package models

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

//---------------------------------------------------
//
// Модел за функцията за възстановяване на паролата
//
//---------------------------------------------------

//TLength дължината на кода за възстановяване
const TLength = 8

//MaxRequest по колко email-a за 1 ден може да се пратят на 1 потребител
const MaxRequests = 4

type ResetPass struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Token  string    `json:"token" db:"token"`
	User   User      `belongs_to:"user" db:"-"`
	UserID uuid.UUID `db:"user_id"`
}

// String is not required by pop and may be deleted
func (r ResetPass) String() string {
	jr, _ := json.Marshal(r)
	return string(jr)
}

func (r ResetPass) TableName() string {
	return "password_reset"
}

func (r *ResetPass) Create(tx *pop.Connection) (*validate.Errors, error) {
	r.UserID = r.User.ID
	return tx.ValidateAndCreate(r)
}

//GenerateToken генерира случайни символи за кода за възтановяване на паролата
func (r *ResetPass) GenerateToken() error {
	n := TLength / 2
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		return errors.WithStack(err)
	}
	s := fmt.Sprintf("%X", b)
	r.Token = s
	return nil
}

func (r *ResetPass) GetToken(tx *pop.Connection) error {
	return tx.Where("user_id = ?", r.User.ID).Last(r)
}

//MaxRequestsReached връща true, ако са пратени твърде много заявки към един email за определено време
func (r *ResetPass) MaxRequestsReached(tx *pop.Connection) (bool, error) {
	c, err := tx.Where("created_at > ?", time.Now().AddDate(0, 0, -1)).
		Where("user_id = ?", r.User.ID).Count(*r)
	if err != nil {
		return true, errors.WithStack(err)
	}
	if c >= MaxRequests {
		return true, nil
	}
	return false, nil
}

//DestroyForUser изтрива всички заявки за даден потребител
func (r *ResetPass) DestroyForUser(tx *pop.Connection) error {
	var rs []ResetPass
	if err := tx.Where("user_id = ?", r.User.ID).All(&rs); err != nil {
		return errors.WithStack(err)
	}
	if err := tx.Destroy(&rs); err != nil {
		return errors.WithStack(err)
	}
	return nil

}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (r *ResetPass) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (r *ResetPass) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (r *ResetPass) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

package actions

import (
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	// "github.com/gobuffalo/pop/nulls"
	// "github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/mailers"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

//Зареждане на форма за бъг репорт
func getReportForm(c buffalo.Context) error {
	return c.Render(200, r.HTML("reports/report"))
}

//Постване на бъг репорт
func postReportForm(c buffalo.Context) error {
	//Създаване на променлива за репорт със стойностите от формата
	rep := &models.Report{}
	if err := c.Bind(rep); err != nil {
		return errors.WithStack(err)
	}

	//Зареждане на потребителя по параметъра в сесия current_user_id
	uid := c.Session().Get("current_user_id")
	u := &models.User{}
	tx := c.Value("tx").(*pop.Connection)
	if err := tx.Find(u, uid); err != nil {
		return errors.WithStack(err)
	}

	//Добавяне на информация за юзъра към репорта
	rep.User = *u

	//Създаване на репорта
	verrs, err := rep.Create(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		c.Set("report", rep)
		c.Set("errors", verrs)
		c.Set("createSiteErrs", true)
		return c.Render(403, r.HTML("reports/report"))
	}

	//Изпращане на имейл към собствениците на сайта
	if err := mailers.SendBugReportEmail(*rep); err != nil {
		return errors.WithStack(err)
	}

	c.Flash().Add("created_report", T.Translate(c, "reportSent"))
	return c.Redirect(302, fmt.Sprintf("/home"))
}

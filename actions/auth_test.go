package actions

import (
	"net/http"

	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
)

//Test_AuthCallback_NewUser Проврява, дали при Response от github/linkedin се създава user в базата
func (as *ActionSuite) Test_AuthCallback_NewUser() {

	usr := &models.Users{}
	as.DB.All(usr)
	as.DB.Destroy(usr)

	cau := gothic.CompleteUserAuth
	defer func() {
		gothic.CompleteUserAuth = cau
	}()
	gothic.CompleteUserAuth = func(http.ResponseWriter, *http.Request) (goth.User, error) {
		return goth.User{
			Name:     "Nikola",
			Provider: "github",
			UserID:   "123",
			Email:    "nikola@example.com",
		}, nil
	}
	count, err := as.DB.Count("users")
	as.NoError(err)
	as.Equal(0, count)

	res := as.HTML("/auth/github/callback").Get()
	as.Equal(302, res.Code)

	count, err = as.DB.Count("users")
	as.NoError(err)
	as.Equal(1, count)

	u := &models.User{}
	err = as.DB.First(u)
	as.NoError(err)
	as.Equal(nulls.NewString("Nikola"), u.Name)
	as.Equal("github", u.Provider.String)
	as.Equal("123", u.ProviderID.String)
	as.Equal("nikola@example.com", u.Email.String)

	as.Equal(u.ID, as.Session.Get("current_user_id"))
}

//Тest_AuthCallback_ExistingUser Проврява, дали при Response от github/linkedin се логва user, ако той вече е в базата
func (as *ActionSuite) Test_AuthCallback_ExistingUser() {
	u := &models.User{
		Name:       nulls.NewString("Ivan"),
		Provider:   nulls.NewString("github"),
		ProviderID: nulls.NewString("123"),
		Email:      nulls.NewString("ivan@example.com"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	cau := gothic.CompleteUserAuth
	defer func() {
		gothic.CompleteUserAuth = cau
	}()
	gothic.CompleteUserAuth = func(http.ResponseWriter, *http.Request) (goth.User, error) {
		return goth.User{
			Name:     "Go6o",
			Provider: "github",
			UserID:   "123",
			Email:    "go6o@example.com",
		}, nil
	}

	count, err := as.DB.Count("users")
	as.NoError(err)
	as.Equal(1, count)

	res := as.HTML("/auth/github/callback").Get()
	as.Equal(302, res.Code)

	count, err = as.DB.Count("users")
	as.NoError(err)
	as.Equal(1, count)

	u = &models.User{}
	err = as.DB.First(u)
	as.NoError(err)
	as.Equal(nulls.NewString("Go6o"), u.Name)
	as.Equal("github", u.Provider.String)
	as.Equal("123", u.ProviderID.String)
	as.Equal("go6o@example.com", u.Email.String)

	as.Equal(u.ID, as.Session.Get("current_user_id"))
}

//Test_EmailLogIn проверки за правилно влизане с email
func (as *ActionSuite) Test_AuthEmailLogIn() {
	//Невалидни данни
	u := &models.User{}
	u.Email = nulls.NewString("test@mail.com")
	u.Password = "test123456"
	u.PasswordConfirmation = "test123456"

	res := as.HTML("/auth/login").Post(u)
	as.Equal(422, res.Code)
	as.Contains(res.Body.String(), "test@mail.com")
	as.Contains(res.Body.String(), "Грешни Email или парола")
	res = nil

	//Валидни данни
	_, err := u.Create(as.DB)
	as.NoError(err)
	res = as.HTML("/auth/login").Post(u)
	as.Equal(302, res.Code)
	as.Equal("/home", res.Header().Get("Location"))

}

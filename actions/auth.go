package actions

import (
	"database/sql"
	"fmt"
	"os"
	"strings"

	"golang.org/x/crypto/bcrypt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/validate"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/markbates/going/defaults"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/github"
	"github.com/markbates/goth/providers/linkedin"
	"github.com/pkg/errors"
)

func init() {
	gothic.Store = App().SessionStore

	goth.UseProviders(
		github.New(os.Getenv("GITHUB_KEY"), os.Getenv("GITHUB_SECRET"), fmt.Sprintf("%s%s", App().Host, "/auth/github/callback")),
		linkedin.New(os.Getenv("LINKEDIN_KEY"), os.Getenv("LINKEDIN_SECRET"), fmt.Sprintf("%s%s", App().Host, "/auth/linkedin/callback")),
	)
}

//AuthCallback вика се когато при логин с github/linkedin се върне отговор за потребителя
func AuthCallback(c buffalo.Context) error {
	gu, err := gothic.CompleteUserAuth(c.Response(), c.Request())
	if err != nil {
		return c.Error(401, err)
	}
	tx := c.Value("tx").(*pop.Connection)
	q := tx.Where("provider = ? and provider_id = ?", gu.Provider, gu.UserID)
	exists, err := q.Exists("users")
	if err != nil {
		return errors.WithStack(err)
	}
	u := &models.User{}
	if exists {
		if err = q.First(u); err != nil {
			return errors.WithStack(err)
		}
	}
	u.Name = nulls.NewString(defaults.String(gu.Name, gu.NickName))
	u.Provider = nulls.NewString(gu.Provider)
	u.ProviderID = nulls.NewString(gu.UserID)
	u.Email = nulls.NewString(gu.Email)
	u.Avatar = nulls.NewString(gu.AvatarURL)
	if err = tx.Save(u); err != nil {
		return errors.WithStack(err)
	}

	c.Session().Set("current_user_id", u.ID)
	if err = c.Session().Save(); err != nil {
		return errors.WithStack(err)
	}

	c.Flash().Add("success", T.Translate(c, "login.welcome"))
	return c.Redirect(302, "/")
}

//AuthDestroy logout - ва потребител
func AuthDestroy(c buffalo.Context) error {
	c.Session().Clear()
	c.Flash().Add("success", T.Translate(c, "login.goodbye"))
	return c.Redirect(302, "/")
}

//SetCurrentUser Задава current_user в context-a, на база current_user_id от сесията
func SetCurrentUser(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		if uid := c.Session().Get("current_user_id"); uid != nil {
			u := &models.User{}
			tx := c.Value("tx").(*pop.Connection)
			if err := tx.Find(u, uid); err != nil {
				return errors.WithStack(err)
			}
			c.Set("current_user", u)
		}
		return next(c)
	}
}

//Authorize middleware - трябва да си логнат
func Authorize(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		if uid := c.Session().Get("current_user_id"); uid == nil {
			c.Flash().Add("danger", T.Translate(c, "login.middleware"))
			return c.Redirect(302, "/auth/login")
		}
		return next(c)
	}
}

//Authorize middleware - трябва да НЕ си логнат
func NotAuth(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		if uid := c.Session().Get("current_user_id"); uid != nil {
			return c.Redirect(302, "/home")
		}
		return next(c)
	}
}

// AuthCreate прави опти да логне потребител със съществуващ email акаунт
func AuthAttempt(c buffalo.Context) error {

	u := &models.User{}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}

	tx := c.Value("tx").(*pop.Connection)

	// find a user with the email
	err := tx.Where("email = ?", strings.ToLower(nulls.String(u.Email).String)).First(u)

	// helper function to handle bad attempts
	bad := func() error {
		c.Set("user", u)
		verrs := validate.NewErrors()
		verrs.Add("email", T.Translate(c, "login.wrong_credentials"))
		c.Set("logInErrs", true)
		c.Set("errors", verrs)
		return c.Render(422, r.HTML("auth/login.html", "layouts/login.html"))
	}

	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			// couldn't find an user with the supplied email address.
			return bad()
		}
		return errors.WithStack(err)
	}

	// confirm that the given password matches the hashed password from the db
	err = bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(u.Password))
	if err != nil {
		return bad()
	}
	c.Session().Set("current_user_id", u.ID)
	c.Flash().Add("success", T.Translate(c, "login.welcome"))

	if strLink := c.Session().Get("string_link"); strLink != nil && strLink != "" {
		c.Session().Delete("string_link")
		return c.Redirect(302, "/site/join/%v", strLink)
	}

	return c.Redirect(302, "/home")
}

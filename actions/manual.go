package actions

import (
	"github.com/gobuffalo/buffalo"
	//"github.com/gobuffalo/pop"
	"github.com/kolioPesho/kp_admin/models"
	"github.com/pkg/errors"
)

func getManual(c buffalo.Context) error {
	var pages models.ManualPages
	if err := pages.GetAllPages(); err != nil {
		return errors.WithStack(err)
	}

	c.Set("pages", pages)
	return c.Render(200, r.HTML("manual/manual"))
}

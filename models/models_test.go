package models_test

import (
	"github.com/gobuffalo/packr"
	"github.com/gobuffalo/suite"
	"github.com/kolioPesho/kp_admin/actions"
	"github.com/kolioPesho/kp_admin/models"
	"testing"
)

type ModelSuite struct {
	*suite.Model
}

func Test_ModelSuite(t *testing.T) {
	actions.App()
	models.Lang = "bg"

	model, err := suite.NewModelWithFixtures(packr.NewBox("../fixtures"))
	if err != nil {
		t.Fatal(err)
	}
	as := &ModelSuite{
		Model: model,
	}
	suite.Run(t, as)
}

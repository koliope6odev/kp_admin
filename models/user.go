package models

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gobuffalo/envy"

	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"

	"github.com/gobuffalo/buffalo/binding"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

//-------------------------
//
// Модел за потребителите
//
//--------------------------

type User struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Email      nulls.String `json:"email" db:"email" form:"email"`
	Name       nulls.String `json:"name" db:"name" form:"name"`
	Occupation nulls.String `json:"occupation,omitempty" db:"occupation" form:"occupation"`
	Phone      nulls.String `json:"phone,omitempty" db:"phone" form:"phone"`
	Birthday   nulls.Time   `json:"birthday,omitempty" db:"birthday" form:"birthday"`
	City       nulls.String `json:"city,omitempty" db:"city" form:"city"`

	Skills nulls.String `json:"skills,omitempty" db:"skills" form:"skills"`

	Avatar     nulls.String `json:"avatar" db:"avatar"`
	AvatarFile binding.File `json:"avatar_file, omitempty" form:"-" db:"-"`
	Cover      nulls.String `json:"cover" db:"cover"`
	CoverFile  binding.File `json:"cover_file, omitempty" form:"-" db:"-"`

	PasswordHash         string `json:"-" db:"password_hash"`
	Password             string `json:"-" db:"-" form:"password"`
	PasswordConfirmation string `json:"-" db:"-" form:"password_confirmation"`

	Sites   Sites   `many_to_many:"sites_users"`
	Invites Invites `has_many:"invites"`

	Provider   nulls.String `json:"provider" db:"provider"`
	ProviderID nulls.String `json:"provider_id" db:"provider_id"`
}

// String is not required by pop and may be deleted
func (u User) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Users is not required by pop and may be deleted
type Users []User

// Create wrapper около логиката за създаване на потребител
// Пуска валидации и прави хаш на паролата
func (u *User) Create(tx *pop.Connection) (*validate.Errors, error) {
	u.Email = nulls.NewString(strings.ToLower(nulls.String(u.Email).String)) // прави email-a lower case
	ph, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return validate.NewErrors(), errors.WithStack(err)
	}
	u.Name = nulls.NewString("")
	u.PasswordHash = string(ph)
	return tx.ValidateAndCreate(u)
}

// Update wrapper около логиката за промяна на потребител
// Пуска валидации и прави хаш на паролата
func (u *User) Update(tx *pop.Connection) (*validate.Errors, error) {
	u.Email = nulls.NewString(strings.ToLower(nulls.String(u.Email).String)) // прави email-a lower case
	if u.Password != "" && u.PasswordConfirmation != "" {
		ph, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return validate.NewErrors(), errors.WithStack(err)
		}
		u.PasswordHash = string(ph)
	} else {
		u.Password = "123456"
		u.PasswordConfirmation = "123456"
	}
	return tx.ValidateAndUpdate(u)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (u *User) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	vals := []validate.Validator{
		&validators.StringLengthInRange{Field: u.Name.String, Max: 30, Message: t("user.name")},
		&validators.StringLengthInRange{Field: u.Occupation.String, Max: 30, Message: "Полето за окупация е твърде дълго"},
		&validators.StringLengthInRange{Field: u.Phone.String, Max: 13, Message: "Телефонът е твърде дълъг"},
		&validators.StringLengthInRange{Field: u.City.String, Max: 50, Message: "Градът е твърде дълъг"},
	}

	if u.Provider.String == "" || u.PasswordHash != "" {
		vals = append(vals,
			&validators.StringIsPresent{Field: u.PasswordHash, Name: "PasswordHash", Message: t("user.passEmpty")},
			&validators.StringIsPresent{Field: u.Password, Name: "Password", Message: t("user.passEmpty")},
			&validators.StringIsPresent{Field: u.PasswordConfirmation, Name: "Password confirmation", Message: t("user.passConfirmEmpty")},
			&validators.StringIsPresent{Field: u.Email.String, Name: "Email", Message: t("user.emailEmpty")},
			&validators.EmailLike{Field: u.Email.String, Name: "Email", Message: t("user.emailInvalid")},
			&validators.StringLengthInRange{Field: u.Password, Name: "Password", Min: 6, Message: t("user.passToShort")},
			&validators.StringsMatch{
				Field: u.Password, Field2: u.PasswordConfirmation, Name: "PasswordConfirmation", Message: t("user.passDoNotMath")},
			// check to see if the email address is already taken:
			&validators.FuncValidator{
				Field:   nulls.String(u.Email).String,
				Name:    "Email",
				Message: t("user.mailTaken"),
				Fn: func() bool {
					var b bool
					q := tx.Where("email = ?", u.Email)
					if u.ID != uuid.Nil {
						q = q.Where("id != ?", u.ID)
					}
					b, err = q.Exists(u)
					if err != nil {
						return false
					}
					return !b
				},
			},
		)
	}
	return validate.Validate(vals...), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (u *User) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (u *User) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

func (u *User) ProfileUpload(tx *pop.Connection) error {

	saveImg := func(t string, file binding.File) (string, error) {

		file.Filename = fmt.Sprintf("%d%s", time.Now().Unix(), file.Filename)

		ENV := envy.Get("GO_ENV", "development")
		URL := envy.Get("UPLOADS_URL", "http://127.0.0.1:3000")

		dir := filepath.Join(".", "assets/images/"+t)
		if ENV == "production" {
			dir = filepath.Join(envy.Get("UPLOADS_PATH", "/var/www/kp_admin"), t)
		}

		if err := os.MkdirAll(dir, 0777); err != nil {
			return "", errors.WithStack(err)
		}
		f, err := os.Create(filepath.Join(dir, file.Filename))
		if err != nil {
			return "", errors.WithStack(err)
		}

		if err := os.Chmod(filepath.Join(dir, file.Filename), 0777); err != nil {
			return "", errors.WithStack(err)
		}

		defer f.Close()
		_, err = io.Copy(f, file)

		if ENV == "production" {
			return URL + "/" + filepath.Join(t, file.Filename), nil
		}

		return URL + "/" + filepath.Join(dir, file.Filename), nil
	}

	if u.AvatarFile.Valid() {
		name, err := saveImg("avatars", u.AvatarFile)
		if err != nil {
			return errors.WithStack(err)
		}
		u.Avatar = nulls.NewString(name)
	}
	if u.CoverFile.Valid() {
		name, err := saveImg("covers", u.CoverFile)
		if err != nil {
			return errors.WithStack(err)
		}
		u.Cover = nulls.NewString(name)
	}
	return nil
}

//JoinSite
//User-a се присъединява към сайт - trigger-ва базата
func (u *User) JoinSite(site *Site, tx *pop.Connection, rights string) error {
	u.Sites = append(u.Sites, *site)
	site.Users = append(site.Users, *u)

	//Запазва релацията
	err := tx.Save(&SitesUsers{
		UserId: u.ID,
		SiteId: site.ID,
		Rights: rights,
	})

	if err != nil {
		return errors.WithStack(err)
	}

	tx.Save(u)

	return nil
}

//LeaveSite
//User-a напуска сайт -  trigger-ва базата
func (u *User) LeaveSite(site *Site, tx *pop.Connection) error {

	u.RemoveSite(*site)
	site.RemoveUser(*u)

	//Изтрива релацията
	var su []SitesUsers
	tx.Where("user_id = ?", u.ID).Where("site_id = ?", site.ID).All(&su)
	err := tx.Destroy(&su)

	if err != nil {
		return errors.WithStack(err)
	}

	tx.Save(u)

	return nil
}

//IsInSite провеярва дали някой потребител има достъп до сайт s
func (u User) IsInSite(s Site, tx *pop.Connection) (in bool, err error) {
	var su SitesUsers
	in, err = tx.Where("site_id = ?", s.ID).Where("user_id = ?", u.ID).Exists(&su)
	return
}

//FindByEmail
func (u *User) FindByEmail(tx *pop.Connection) (bool, error) {

	exists := true
	err := tx.Where("email = ?", u.Email).First(u)

	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			exists = false
		} else {
			return false, errors.WithStack(err)
		}
	}

	return exists, nil

}

//RemoveSite премахва сайт от масива със сайтове !!!Не trigger-ва базата
func (u *User) RemoveSite(s Site) error {
	var newSs Sites
	for _, e := range u.Sites {
		if e.ID != s.ID {
			newSs = append(newSs, e)
		}
	}
	u.Sites = newSs
	return nil
}

export let  en = {
    back_short: "back",
    yes: "yes",
    no: "no",
    ok: "ok",
    back: "back",
    for: "for",
    next: "next",
    table: "table",
    save: "save",
    close: "close",
    cancel:"cancel",
    open:"open",

    login :{
        login:"Log in",
        signup:"Sign up",
        pass:"Password",
        input:"Input",
        confirmPass:"Retype password",
        forgottenPass:"Forgot your password ?",
        resetPass:{
            header:"Password reset",
            email:"your E-mail",
            send:"Send recovery E-mail ",
            code:"Code",
            codeConfirm:"Confirm code",
            pass:"Password",
            passConfirm:"Confirm password",
            change:"Change password",
            success:"Password for account with email \"{email}\" updated successfully !",
            sendSuccess:"Recovery email sent successfully to {email}"
        }
    },
    site:{
        createForm:{
            name:"Name",
            desc:"Description",
            link:"Link",
            create:"Create"
        },
        allSites:{
            all:"All projects",
            owner:"Owner",
            admin:"Administrator",
            rw:"Read and Write",
            r:"Read",
            show:"Show",
            noSites:"No such projects",
            name:"Name",
            desc:"Description",
            link:"Link",
            created:"Created at",
            updated:"Last modified",
        },

        dbs: {
            header: "Databases",
            noDBs: "No database connections found",
            settings: "Settings",
            delete: "Remove",
            rename: "Rename",

            modals: {
                deleteDB: {
                    header: "Remove database connection",
                    subHeader: "Are you sure you want to remove the connection ",
                    successMsg: "Successfully removed connection"
                },
                renameTable: {
                    header: "Rename table",
                    successMsg: "Successfully changed table name",
                },
                createDB: {
                    successMsg: "Successfully added database connection",
                    header: "Add database connection",
                    type: "Type",
                    port: "Port",
                    host: "Host",
                    user: "DB user",
                    pass: "DB password",
                    connection_name: "Connection name",
                    noTables: "No tables found !",
                    chooseTables: "Choose tables",
                    messages:[
                        "Input name for the database connection for  {db} ( {host} ), please",
                        "Connection refused  - {err}",
                        "Select the tables you want to administrate, please",
                        "Chose at least one table"
                    ],
                },
                updateDB: {
                    successMsg: "Database connection successfully edited",
                    header: "Database connection settings",
                },
                tableSettings: {
                    table: "Table",
                    header: "Table settings",
                    columnsHeader: "Columns",
                    modifications: "Edit log",
                    tableName: "Table name",
                    pkColumnHint: "Chose the column that has only unique value",
                    pkColumn: "Unique/Primary key column",
                    renameSuccess: "Table renamed successfully",
                    pkSuccess: 'Key column set successfully',
                    columns: {
                        chose: "Columns select",
                        rename: "Rename",
                        rights: "Access",
                        modificationsH: "Modifiers",
                        modifications: {
                            mutations: "Value modifiers",
                            relations: "Relations",
                            validations: "Validation rules",
                        }

                    },
                    mutators: {
                        col: "Modified column",
                        func: "Function",
                        params: "Parameters",
                        empty: "No columns with value modifiers found.",
                        stringFText: "+ cell text +"
                    },
                    relations: {
                        col: "Replaced column",
                        tbl: "Relation table",
                        col1: "Replacing column",
                        col2: "ON query column",
                        empty: "No columns with relations found.",
                    },
                    validations: {
                        col: "Column",
                        func: "Function",
                        params: "Parameters",
                        msg: "Error message",
                        empty: "No columns with validation rules found.",

                    },


                },
                addMutation: {
                    header: "Add modifier function",
                    column: "Chose column",
                    columnLabel: "Column",

                    func: "Chose modifier function",
                    funcLabel: "Modifier function",

                    dict: "Dictionary",
                    key: "Key",
                    val: "Value",
                    textBefore: "text before",
                    colText: "cell text",
                    textAfter: "text after",

                    msg: 'Modification function successfully added'
                },
                addRelation: {
                    header: "Add relation",
                    column: "Chose column",
                    columnLabel: "Column to be replace",
                    table: "Chose table",
                    tableLabel: "Table",
                    relationColumnLabel:"Column from {table} to replace  {column}",
                    onColumnLabel:"Column for the ON query (the unique key/id/pk of {table})",


                    errors: [
                        "No suitable tables found",
                        "(having configured columns)",
                        "Unable to save the relation",
                    ],

                    msg: 'Relation added successfully'
                },
                addValidation: {
                    header: "Add validation rules",
                    column: "Chose column",
                    columnLabel: "Column to be validated",

                    func: "Chose validation function",
                    funcLabel: "Validation rule",

                    params :{
                        between:"between",
                        and:"and",
                        characters:"characters",
                        lowerBorder:"lower border",
                        upperBorder:"upper border"
                    },

                    msgLabel: "Error message (optional)",



                    msg: 'Validation rule added successfully'
                },
                deleteMutation:{
                    header:"Are you sure you want to remove the modification rule ?",
                    msg:"Modification rule successfully removed"
                },
                deleteRelation:{
                    header:"Are you sure you want to remove the relation rule",
                    msg:"Relation rule successfully removed",
                },
                deleteValidation:{
                    header:"Are you sure you want to remove the validation rule ?",
                    msg:"Validation rule successfully removed"
                }
            },
            table: {
                search: "Search",
                noCols: "No columns selected, please select the columns you want to manage.",
                connErr: "Connection error",
                tableEmpty: "Empty table",
                showNOnPage: "Show {n} on page",
                edit: "Edit value",
                entries: "{n} entry|{n} entries",
                choseColumns: "Chose columns",
                columnsSuccess: 'Columns updated successfully.',
                renameColumns: "Rename columns",
                renameColumnsSuccess: "Column renamed successfully",
                accessHeader: "Read only",
                accessSubheader: "Chose read only columns",

                cellEdit: {
                    save: "save changes",
                    logs: "edit log",
                    errs: [
                        "Set primary key column in order to edit cell values",
                        "Unable to change the value",
                        "Relation edit not supported yet"
                    ]
                },
                tooltips: {
                    relationTo: "relation to {table}.{column}",
                    mutation: "modification function {f}",
                },
                logs: {
                    revertSuccess: "value reverted successfully",
                    header: "Edit log",
                    empty: "No changes found",
                    col: "Column",
                    old: "Old value",
                    new: "New value",
                    modifier: "Modified by",
                    date: "Modified at",
                    revert: "revert"
                }
            }
        },

        charts:{
            header:"Charts",
            empty:"No charts found",
            db:"Database",
            table:"Table",
            column:"Column",
            settings:"Settings",
            delete:"Remove",
            deleteHeader:"Remove chart",
            deleteAsk:"Are you sure you want to remove the chart \"{name}\" ?",
            deleteSuccess:"Chart remove successfully",
            modal:{
                headerCreate:"Add chart",
                headerUpdate:"Edit chart",
                types:[
                    "Percentage (group by)",
                    "Number of entries through time",
                ],
                typeLabel:"Statistics type",
                db:"Chose database connection",
                dbLabel:"Database Connection",
                table:"Chose table",
                tableLabel:"Table",
                column:"Chose column",
                columnLabel:'Column',
                createdAtColumnLabel:'created_at column',
                isTimestamp:"the column timestamp (integer)",
                chart:"Chose chart type",
                recommended:"recommended",
                chartLabel:"Chart type",
                name:"Chart name",
                names:[
                    "{type} chart for {column}",
                    "Number of entries in {table}"
                ],
                messages:[
                    "Chart added successfully",
                    "Chart edited successfully",
                    "Connection error - {err}"
                ]
            }
        },

        users:{
            header:"Users with access",
            owners:"Owner",
            admins:"Administrators",
            admin:"Administrator",
            rws:"Read and write",
            r: "Read",
            access:"Access level",
            remove:"Remove",
            change:"Change access level",
            removeSuccess:'User removed successfully',
            changeSuccess:'Rights change successfully for user {user}',
            empty:"No such users",

            modal:{
                header:"Invite user",
                subHeader:"Grant a user access to the site's admin panel",
                link:"Link",
                generate:"Generate",
                emailLabel:"User's E-mail",
                send:"Send invitation",
                copied:"Copied to clipboard !",
                sendSuccess:"Invite sent successfully"
            }
        },

        emails:{
            header:"Buck email send",
            msg:"Feature will be available soon "
        },

        settings:{
            header:"Settings",
            name:"Name",
            link:"Link",
            desc:"Description",
            delete:"Delete site",
            deleteHeader:"Delete site settings",
            deleteAsk:"Are you sure you want to remove the settings for {site} ?",
            deleteWarn:"all db settings, charts and others will be removed too",
            editSuccess:"Site edited successfully",

        },

        sideBar:{
            dbs:"Databases",
            users:"User access",
            charts:"Charts",
            emails:"Bulk emails",
            settings:"Settings",
        }
    },

    chart:{
        from:"From date",
        padding: "every {n}th day",
        total:"total"
    },

    invites:{
        header:'You are invited to join "{site}"',
        join:"Join site",
        empty:"No active invites found",
        joinSite:"Join {site}"
    },

    profile:{
        mySites:"Потребител в сайтове",
        ownedSites:"Притежавани сайтове",
        skills:"Умения",
        email:"Име/Имейл",
        fileHeader:"Смени аватара или корицата",
        avatar:"Аватар",
        cover:"Корица",
        upload:"Качи",
    },



    bugreport:{
        os:"Operation system",
        os_version:"OS version",
        browser:"Browser",
        ip:"IP address",
        mail:"E-mail",
        comment:"Bug description",
        photo:"Photo",
        report:"Send bug report"
    }
};
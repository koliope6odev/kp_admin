package models

// ----------------------------------
//
//  Модел за бъг репорти
//
// ----------------------------------

import (
	"time"

	"github.com/gobuffalo/buffalo/binding"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

type Report struct {
	ID        uuid.UUID `db:"id" json:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Os        string `db:"os" form:"os" json:"os"`                         //Операционна система
	OsVersion string `db:"os_version" form:"os_version" json:"os_version"` //версия на операционната система
	Browser   string `db:"browser" form:"browser" json:"browser"`          //вид на браузъра
	Ip        string `db:"ip_address" form:"ip_address" json:"ip_address"` //Ip адрес
	Email     string `db:"email" form:"email" json:"email"`                //имейл на потребителя, който пише репорта
	Comment   string `db:"comment" form:"comment" json:"comment"`          //коментар към репора

	User User `db:"-" json:"user"` //потребител създал репорта

	ScreenshotFile binding.File `json:"photo_file, omitempty" form:"screenshot_file" db:"-"` //скрийншот снимка
	Screenshot     nulls.String `json:"screenshots" db:"screenshots" form:"-"`               //път към скрийншот във файловата система
}

//Create wrapper за създаване на репорт
func (r *Report) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(r)
}

//Update wrapper за редактиране на репорти
func (r *Report) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(r)
}

//Валидация
func (r *Report) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{Field: r.Os, Name: "Os", Message: t("report.os")},
		&validators.StringLengthInRange{Field: r.Os, Name: "Os", Max: 50, Message: t("report.osMax", map[string]interface{}{"max": 50})},
		&validators.StringIsPresent{Field: r.OsVersion, Name: "OsVersion", Message: t("report.osv")},
		&validators.StringLengthInRange{Field: r.OsVersion, Name: "OsVersion", Max: 50, Message: t("report.osVMax", map[string]interface{}{"max": 50})},
		&validators.StringIsPresent{Field: r.Browser, Name: "Browser", Message: t("report.Browser")},
		&validators.StringLengthInRange{Field: r.Browser, Name: "Browser", Max: 50, Message: t("report.BrowserMax", map[string]interface{}{"max": 50})},
		&validators.StringIsPresent{Field: r.Ip, Name: "Ip", Message: t("report.Ip")},
		&validators.StringLengthInRange{Field: r.Ip, Name: "Ip", Max: 25, Message: t("report.IpMax", map[string]interface{}{"max": 50})},
		&validators.StringIsPresent{Field: r.Email, Name: "Email", Message: t("report.Email")},
		&validators.StringLengthInRange{Field: r.Email, Name: "Email", Max: 50, Message: t("report.EmailMax", map[string]interface{}{"max": 50})},
		&validators.StringIsPresent{Field: r.Comment, Name: "Comment", Message: t("report.Comment")},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (r *Report) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (r *Report) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

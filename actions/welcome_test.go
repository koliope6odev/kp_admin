package actions

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_Welcome_WelcomeHandler() {
	//Login in
	// Взима потребител от базата
	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)

	// задава стойност на current_user_id (с id-то на потребителя, който сме взели от базата)
	as.Session.Set("current_user_id", u.ID)

	res := as.HTML("/").Get()
	as.Equal(302, res.Code)
	//Трябва да редиректва към /home
	as.Equal(res.Header().Get("Location"), "/home")

	// изтрива сесията (log out-ва потребителя)
	as.Session.Clear()
	res = as.HTML("/").Get()
	as.Equal(200, res.Code)

}

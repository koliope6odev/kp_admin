package models_test

import (
	"strconv"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (ms *ModelSuite) TestMutationFuncs_Mutate() {

	mf := models.MutationFunc{
		FuncName: "timestamp2timestring",
	}

	//timestamp2timestring
	s := mf.Mutate("1518328047")
	ms.Contains(s, "2018-02-11 (")
	s = mf.Mutate("invalid")
	ms.Equal("NULL", s)

	//int2bool
	mf.FuncName = "int2bool"
	ms.Equal("Bool(False)", mf.Mutate("0"))
	ms.Equal("Bool(True)", mf.Mutate("1"))
	ms.Equal("Bool(True)", mf.Mutate("ivan"))

	//dict
	mf.FuncName = "dict"
	mf.Dict = nulls.NewString(`{
		"1":"nikola",
		"2":"pe6o", 
		"qwe":"ivan"
	}`)
	ms.Equal("nikola", mf.Mutate("1"))
	ms.Equal("ivan", mf.Mutate("qwe"))
	ms.Equal("NULL", mf.Mutate("3"))

	//stringF
	mf.FuncName = "stringF"
	mf.StringF = nulls.NewString("**{{@}} лв")
	ms.Equal("**1 лв", mf.Mutate("1"))
	ms.Equal("**qwe лв", mf.Mutate("qwe"))
}

func (ms *ModelSuite) TestMutationFuncs_MutateReverse() {
	mf := models.MutationFunc{
		FuncName: "timestamp2timestring",
	}

	//timestamp2timestring
	s := mf.MutateReverse("2018-01-14 (09:26)")
	timestampt, err := strconv.Atoi(s)
	ms.NoError(err)
	ms.InDelta(1515965160, timestampt, 43200)
	s = mf.Mutate("invalid")
	ms.Equal("NULL", s)

	//int2bool
	mf.FuncName = "int2bool"
	ms.Equal("0", mf.MutateReverse("Bool(False)"))
	ms.Equal("1", mf.MutateReverse("Bool(True)"))
	ms.Equal("1", mf.MutateReverse("ivan"))

	//dict
	mf.FuncName = "dict"
	mf.Dict = nulls.NewString(`{
		"1":"nikola",
		"2":"pe6o", 
		"qwe":"ivan"
	}`)
	ms.Equal("1", mf.MutateReverse("nikola"))
	ms.Equal("2", mf.MutateReverse("pe6o"))
	ms.Equal("qwe", mf.MutateReverse("ivan"))

	//stringF
	mf.FuncName = "stringF"
	mf.StringF = nulls.NewString("**{{@}} лв")
	ms.Equal("1", mf.MutateReverse("**1 лв"))
	ms.Equal("qwe", mf.MutateReverse("**qwe лв"))

}

func (ms *ModelSuite) TestMutationFunc_Create() {
	c := testRelation_getColumn(ms, "site_users", "site_id", "id", "_TestMutationFunc_Create")

	mf := models.MutationFunc{
		FuncName: "int2bool",
		ColumnID: c.ID,
	}

	//Валидни данни
	ms.DBDelta(1, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.False(verrs.HasAny())
		ms.NoError(err)
	})
	ms.DB.Destroy(&mf)

	//Без column_id
	mf.ColumnID = uuid.Nil
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})
	mf.ColumnID = c.ID

	//Без function_name
	mf.FuncName = ""
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

	//Невалидно function_name
	mf.FuncName = "ivan"
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

	//Dict без dict
	mf.FuncName = "dict"
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

	//Dict c невалиден  dict
	mf.FuncName = "dict"
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

	//Dict c валиден  dict
	mf.FuncName = "dict"
	mf.Dict = nulls.NewString("{\"1\":\"nikola\"}")
	ms.DBDelta(1, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.False(verrs.HasAny())
		ms.NoError(err)
	})
	ms.DB.Destroy(&mf)

	//StringF без stringF
	mf.FuncName = "stringF"
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

	//StringF с невалиден stringF
	mf.FuncName = "stringF"
	mf.StringF = nulls.NewString("invalid")
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

	//StringF с валиден stringF
	mf.FuncName = "stringF"
	mf.StringF = nulls.NewString("{{@}}лв")
	ms.DBDelta(1, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.False(verrs.HasAny())
		ms.NoError(err)
	})

	//Два пъти за 1 и съща колона
	ms.DBDelta(0, "mutation_functions", func() {
		verrs, err := mf.Create(ms.DB)
		ms.True(verrs.HasAny())
		ms.NoError(err)
	})

}

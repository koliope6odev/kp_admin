package models_test

import (
	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (ms *ModelSuite) Test_User() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	err := models.DB.Save(u)
	ms.NoError(err)

}

//Провеярва, дали правилно работи JoinSite
func (ms *ModelSuite) TestUser_JoinSite() {
	u := &models.User{
		Name:  nulls.NewString("hasAccessToSites"),
		Email: nulls.NewString("ivan@example.com"),
	}

	s := &models.Site{
		Name: nulls.NewString("HasUser"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}
	ms.Empty(u.Sites)

	models.DB.Save(u)
	models.DB.Save(s)

	u.JoinSite(s, models.DB, "read")
	//models.DB.Save(u)
	ms.NotEmpty(u.Sites)
	ms.Len(u.Sites, 1)
	ms.Len(s.Users, 1)
	ms.Equal(u.ID, s.Users[0].ID)

	//Reset-ва потребителя и сайта, за да симулира дърпане от базата
	s = &models.Site{}
	u = &models.User{}

	models.DB.Eager().Last(u)
	models.DB.Eager().Last(s)

	ms.NotEmpty(u.Sites)
	ms.Len(u.Sites, 1)
	ms.Len(s.Users, 1)
	ms.Equal(u.ID, s.Users[0].ID)

}

func (ms *ModelSuite) TestUser_LeaveSite() {
	u := &models.User{
		Name: nulls.NewString("hasAccessToSites"),

		Email: nulls.NewString("ivan@example.com"),
	}

	s := &models.Site{
		Name: nulls.NewString("SiteNotToLeave"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}

	sToLeave := &models.Site{
		Name: nulls.NewString("SiteToLeave"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}

	ms.Empty(u.Sites)

	models.DB.Save(u)
	models.DB.Save(s)
	models.DB.Save(sToLeave)

	u.JoinSite(s, models.DB, "admin")
	u.JoinSite(sToLeave, models.DB, "admin")

	ms.Len(u.Sites, 2)

	u.LeaveSite(sToLeave, models.DB)

	ms.Len(u.Sites, 1)
	ms.Len(s.Users, 1)
	ms.Empty(sToLeave.Users)

	//Reset-ва потребителя и сайта, за да симулира дърпане от базата
	sToLeaveId := sToLeave.ID
	sId := s.ID
	s = &models.Site{}
	sToLeave = &models.Site{}
	u = &models.User{}

	models.DB.Eager().Last(u)
	models.DB.Eager().Find(s, sId)
	models.DB.Eager().Find(sToLeaveId, sToLeaveId)

	ms.Len(u.Sites, 1)
	ms.Len(s.Users, 1)
	ms.NotEqual(1, len(sToLeave.Users))
	ms.Equal(u.ID, s.Users[0].ID)

}

func (ms *ModelSuite) TestUser_IsInSite() {
	u := &models.User{
		Name:  nulls.NewString("hasAccessToSites"),
		Email: nulls.NewString("ivan@example.com"),
	}

	s := &models.Site{
		Name: nulls.NewString("SiteNotToLeave"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}

	s1 := &models.Site{
		Name: nulls.NewString("SiteToLeave"),
		Desc: nulls.NewString("Description"),
		Link: nulls.NewString("Link"),
	}

	models.DB.Save(u)
	models.DB.Save(s1)
	models.DB.Save(s)

	ms.Empty(u.Sites)

	u.JoinSite(s, models.DB, "admin")

	ms.Len(u.Sites, 1)

	in, err := u.IsInSite(*s, models.DB)
	ms.NoError(err)
	ms.True(in)

	in, err = u.IsInSite(*s1, models.DB)
	ms.NoError(err)
	ms.False(in)

}

func (ms *ModelSuite) TestUser_RemoveSite() {

	u := &models.User{
		Name: nulls.NewString("Ivan"),
		Sites: models.Sites{
			models.Site{ID: uuid.UUID{1}},
			models.Site{ID: uuid.UUID{2}},
			models.Site{ID: uuid.UUID{3}},
			models.Site{ID: uuid.UUID{4}},
		},
	}

	u.RemoveSite(models.Site{ID: uuid.UUID{3}})

	ms.Len(u.Sites, 3)

	for _, s := range u.Sites {
		id3 := uuid.UUID{3}
		ms.NotEqual(id3, s.ID)
	}

}

func (ms *ModelSuite) TestUser_FindByEmail() {

	//Правим си някакъв user, който после да намерим по email
	savedU := models.User{}
	savedU.Email = nulls.NewString("email@to.find")
	savedU.Name = nulls.NewString("Imeto")
	savedU.Avatar = nulls.NewString("https://avatar.com?avatar1")
	models.DB.Create(&savedU)

	u := models.User{}
	//Несъществуващ потребител
	u.Email = nulls.NewString("email@Notto.find")

	exist, err := u.FindByEmail(models.DB)
	ms.NoError(err)
	ms.False(exist)

	//Съществуващ потребител
	u.Email = nulls.NewString("email@to.find")
	exist, err = u.FindByEmail(models.DB)
	ms.NoError(err)
	ms.True(exist)

	//Празен email
	u.Email = nulls.NewString("")
	exist, err = u.FindByEmail(models.DB)
	ms.NoError(err)
	ms.False(exist)

}

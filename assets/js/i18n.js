let {bg} = require("./i18n_bg");
let {en} = require("./i18n_en");

export let messages =  {
    bg, en
};
package actions

import (
	"fmt"

	"github.com/gobuffalo/pop/nulls"
	"github.com/gofrs/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

func (as *ActionSuite) Test_Columns_GetColumns() {

	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	var tbl models.Table
	tbl = models.Table{
		Name:   "databases",
		Access: "admin",
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Host:     "localhost",
		Password: "postgres",
		Port:     5432,
		DB:       "kp_admin_development",
		Name:     "test db conn",
		Tables:   []models.Table{tbl},
		Site:     s,
	}
	_, err = dbc.Create(models.DB)
	as.NoError(err)

	models.DB.Where("db = ?", dbc.ID).First(&tbl)
	tbl.DB = dbc

	//Aко не е част от сайт
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/get/columns", s.ID)).Post(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	u.JoinSite(&s, as.DB, "admin")

	res = as.JSON(fmt.Sprintf("/site/%s/db/table/get/columns", s.ID)).Post(nil)
	as.Equal(406, res.Code)

	res = as.JSON(fmt.Sprintf("/site/%s/db/table/get/columns", s.ID)).Post(tbl)
	as.Equal(200, res.Code)

}

func (as *ActionSuite) Test_Columns_CreateColumns() {
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	url := fmt.Sprintf("/site/%s/db/table/create/columns", s.ID)

	//Aко не е логнат
	res := as.JSON(url).Put(nil)
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	var tbl models.Table
	tbl = models.Table{
		Name:   "databases",
		Access: "admin",
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Host:     "localhost",
		Password: "postgres",
		Port:     5432,
		DB:       "kp_admin_development",
		Name:     "test db conn",
		Tables:   []models.Table{tbl},
		Site:     s,
	}
	_, err = dbc.Create(models.DB)
	as.NoError(err)

	models.DB.Where("db = ?", dbc.ID).First(&tbl)
	tbl.DB = dbc

	//Aко не е част от сайт
	res = as.JSON(url).Put(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(url).Put(nil)
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	res = as.JSON(url).Put(nil)
	as.Equal(406, res.Code)

	res = as.JSON(url).Put(tbl)
	as.Contains(res.Body.String(), "Няма избрани колони!")
	as.Equal(403, res.Code)

	tbl.Columns = models.Columns{
		{
			Column:   "column1",
			DataType: "string",
		},
		{
			Column:   "column2",
			DataType: "string",
		},
	}

	res = as.JSON(url).Put(tbl)
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Columns saved!")

	res = as.JSON(url).Put(tbl)
	as.Equal(406, res.Code)
	as.Contains(res.Body.String(), "Вече има запазени колони за тази таблица!")
}

func (as *ActionSuite) Test_Columns_UpdateColumns() {
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	url := fmt.Sprintf("/site/%s/db/table/update/columns", s.ID)

	//Aко не е логнат
	res := as.JSON(url).Put(nil)
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	var tbl models.Table
	tbl = models.Table{
		Name:   "databases",
		Access: "admin",
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Host:     "localhost",
		Password: "postgres",
		Port:     5432,
		DB:       "kp_admin_development",
		Name:     "test db conn",
		Tables:   []models.Table{tbl},
		Site:     s,
	}
	_, err = dbc.Create(models.DB)
	as.NoError(err)

	models.DB.Where("db = ?", dbc.ID).First(&tbl)
	tbl.DB = dbc

	//Aко не е част от сайт
	res = as.JSON(url).Put(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(url).Put(nil)
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	//Първо запазва колони
	tbl.Columns = models.Columns{
		{
			Column:   "column1",
			DataType: "string",
		},
		{
			Column:   "column2",
			DataType: "string",
		},
	}

	res = as.JSON(fmt.Sprintf("/site/%s/db/table/create/columns", s.ID)).Put(tbl)
	as.Equal(200, res.Code)

	//Новите колони
	newCols := models.Columns{
		{
			Column:   "column1",
			DataType: "time",
		},
		{
			Column:   "column2",
			DataType: "string",
		},
		{
			Column:   "column3",
			DataType: "string",
		},
	}
	tbl.Columns = newCols

	res = as.JSON(url).Put(tbl)
	as.Equal(200, res.Code)
	//Проверява, дали е променило базата
	t := &models.Table{}
	as.NoError(as.DB.Find(t, tbl.ID))
	cols, err := t.GetSavedColumns(as.DB)
	as.NoError(err)
	as.Equal(len(newCols), len(cols))
	as.Equal(newCols[1].Column, cols[1].Column)

	//Aко няма избрани колони
	tbl.Columns = models.Columns{}
	res = as.JSON(url).Put(tbl)
	as.Equal(403, res.Code)

}

func (as *ActionSuite) Test_Columns_GetSavedColumns() {

	s := models.Site{
		Name: nulls.NewString("Site333"),
		Desc: nulls.NewString("Site333Desc"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	url := fmt.Sprintf("/site/%s/db/table/get/saved/columns/", s.ID)

	//Aко не е логнат
	res := as.JSON(url).Post(nil)
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	var tbl models.Table
	tbl = models.Table{
		Name:   "databases",
		Access: "admin",
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Host:     "localhost",
		Password: "postgres",
		Port:     5432,
		DB:       "kp_admin_development",
		Name:     "test db conn",
		Tables:   []models.Table{tbl},
		Site:     s,
	}
	_, err = dbc.Create(models.DB)
	as.NoError(err)

	models.DB.Where("db = ?", dbc.ID).First(&tbl)
	tbl.DB = dbc

	//Aко не е част от сайт
	res = as.JSON(url).Post(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	u.JoinSite(&s, as.DB, "admin")

	//празен пост
	res = as.JSON(url).Post(nil)
	as.Equal(406, res.Code)

	//ако няма колони
	res = as.JSON(url).Post(tbl)
	as.Contains(res.Body.String(), "null")
	as.Equal(200, res.Code)

	//задава колони
	tbl.Columns = models.Columns{
		{
			Column:   "saved_col1",
			Verbose:  nulls.NewString("Saved Column #1"),
			DataType: "string",
		},
		{
			Column:   "saved_col2",
			Verbose:  nulls.NewString("Saved Column #2"),
			DataType: "string",
		},
	}
	err = tbl.SaveColumns(models.DB)
	as.NoError(err)

	res = as.JSON(url).Post(tbl)
	as.Contains(res.Body.String(), "saved_col1")
	as.Contains(res.Body.String(), "saved_col2")
	as.Contains(res.Body.String(), "Saved Column #1")
	as.Contains(res.Body.String(), "Saved Column #2")
	as.Contains(res.Body.String(), "string")

	as.Equal(200, res.Code)
}

func (as *ActionSuite) Test_Columns_GetRowWS() {
	//това бачка с web socket-и и нямам идея как да го тествам
	//(модела си е тестван обаче)
}

func (as *ActionSuite) Test_Columns_CountRows() {

	s := models.Site{
		Name: nulls.NewString("Site333"),
		Desc: nulls.NewString("Site333Desc"),
		Link: nulls.NewString("localhost:3000"),
	}
	models.DB.Create(&s)

	url := fmt.Sprintf("/site/%s/db/table/rows/count/", s.ID)

	//Aко не е логнат
	res := as.JSON(url).Post(nil)
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Колио"),
		Email: nulls.NewString("test@gmail.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	as.Session.Set("current_user_id", u.ID)

	var tbl models.Table
	tbl = models.Table{
		Name:   "databases",
		Access: "admin",
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Host:     "localhost",
		Password: "postgres",
		Port:     5432,
		DB:       "kp_admin_development",
		Name:     "test db conn",
		Tables:   []models.Table{tbl},
		Site:     s,
	}
	_, err = dbc.Create(models.DB)
	as.NoError(err)

	models.DB.Where("db = ?", dbc.ID).First(&tbl)
	tbl.DB = dbc

	//Aко не е част от сайт
	res = as.JSON(url).Post(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	u.JoinSite(&s, as.DB, "admin")

	//празен пост
	res = as.JSON(url).Post(nil)
	as.Equal(406, res.Code)

	//с колони
	res = as.JSON(url).Post(tbl)
	//as.Contains(res.Body.String(), "4")
	as.Equal(200, res.Code)

}

func (as *ActionSuite) Test_Columns_UpdateColumn() {
	s := models.Site{
		Name: nulls.NewString("Site111"),
		Desc: nulls.NewString("Site222"),
		Link: nulls.NewString("localhost:3000")}
	models.DB.Create(&s)

	url := fmt.Sprintf("/site/%s/db/table/update/column/", s.ID)

	//Aко не е логнат
	res := as.JSON(url).Put(nil)
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Go6o"),
		Email: nulls.NewString("test@go6o.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	//Логва се
	as.Session.Set("current_user_id", u.ID)

	var c models.Column
	as.LoadFixture("a column")
	as.DB.Where("column_name = ?", "column1").First(&c)

	//Aко не е част от сайт
	res = as.JSON(url).Put(c)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(url).Put(nil)
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	//Без промени
	res = as.JSON(url).Put(c)
	as.Contains("OK", res.Body.String())
	as.Equal(200, res.Code)

	//Твърде дълго име
	c.Verbose = nulls.NewString("123456789–123456789–123456789–123456789–123456789–123456789–")
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	c.Verbose = nulls.NewString("verbose")

	//Пробва се да смени оригиналния тип данни с несъществуващ
	c.DataType = "измислен–тип"
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)

	//Пробва се да смени оригиналния тип данни
	c.DataTypeOriginal = "varchar 253"
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Оригиналният тип данни не може да бъде сменен!")
	c.DataTypeOriginal = "varchar 255"

	//Пробва се да смени типа данни
	c.DataType = "json"
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Типа данни не може да бъде сменен!")
	c.DataType = "string"

	//Пробва се да смени оригиналното read write
	c.DataTypeReadOnly = false
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Оригиналните права за четене не могат да бъдат сменени!")
	c.DataTypeReadOnly = true

	//Пробва се да смени оригиналното име на колоната
	c.Column = "column####"
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Оригиналното име на колоната не може да буде променяно!")
	c.Column = "column1"

	//Промяна  на правата за четене
	c.ReadOnly = false
	res = as.JSON(url).Put(c)
	as.Equal(403, res.Code)
	as.Contains(res.Body.String(), "Не може да променяте правата за писане на тази колона!")
	c.ReadOnly = true

	//Промяна на тривиалното име
	c.Verbose = nulls.NewString("verbose name")
	res = as.JSON(url).Put(c)
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "OK")

	var newCol models.Column
	as.DB.Find(&newCol, c.ID)
	as.Equal(nulls.NewString("verbose name"), newCol.Verbose)

	//Промяна на правата за писане
	var c2 models.Column
	as.DB.Where("column_name = ?", "column1_readOnly_false").First(&c2)
	as.NoError(as.DB.Save(&c2))

	c2.ReadOnly = true
	res = as.JSON(url).Put(c2)
	as.Contains(res.Body.String(), "OK")
	as.Equal(200, res.Code)

	as.DB.Find(&newCol, c.ID)
	as.Equal(true, newCol.ReadOnly)
}

func (as *ActionSuite) Test_Columns_AddRelation() {
	as.DB.Destroy(&models.Relations{})
	addRelation(as)
}

func (as *ActionSuite) Test_Columns_AddMutation() {
	as.DB.Destroy(&models.MutationFuncs{})
	addMutation(as)
}

func (as *ActionSuite) Test_Columns_AddValidation() {
	as.DB.Destroy(&models.CellEditValidationRules{})
	addValidation(as)
}

func (as *ActionSuite) Test_Columns_DeleteRelation() {
	c := addRelation(as)

	s := models.Site{}
	as.NoError(as.DB.Where("name=?", "Test_Columns_Relations").First(&s))

	//---- несъщесвуваща колона -----/
	res := as.JSON(fmt.Sprintf(
		"/site/%s/db/table/column/a2d044c6-62a8-4939-84e0-b17638d8a424/remove/relation/", s.ID)).Delete()
	as.Equal(406, res.Code)

	// --- колона, която няма релации ---///
	c1 := models.Column{Column: "ColWithNoRels"}
	as.NoError(as.DB.Create(&c1))
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/remove/relation/", s.ID, c1.ID)).Delete()
	as.Equal(406, res.Code)

	////-----------Всичко 6------------//
	as.DBDelta(-1, "relations", func() {
		res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/remove/relation/", s.ID, c.ID)).Delete()
		as.Equal(200, res.Code)
	})

}

func (as *ActionSuite) Test_Columns_DeleteMutation() {
	c := addMutation(as)

	s := models.Site{}
	as.NoError(as.DB.Where("name=?", "Test_Columns_Mutations").First(&s))

	//---- несъщесвуваща колона -----/
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/column/a2d044c6-62a8-4939-84e0-b17638d8a424/remove/mutation/", s.ID)).Delete()
	as.Equal(406, res.Code)

	// --- колона, която няма мутации ---///
	c1 := models.Column{Column: "ColWithNoMuts"}
	as.NoError(as.DB.Create(&c1))
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/remove/relation/", s.ID, c1.ID)).Delete()
	as.Equal(406, res.Code)

	////-----------Всичко 6------------//
	as.DBDelta(-1, "mutation_functions", func() {
		res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/remove/mutation/", s.ID, c.ID)).Delete()
		as.Equal(200, res.Code)
	})

}

func (as *ActionSuite) Test_Columns_DeleteValidation() {
	c := addValidation(as)

	s := models.Site{}
	as.NoError(as.DB.Where("name=?", "Test_Columns_Mutations").First(&s))

	//---- несъщесвуваща колона -----/
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/column/a2d044c6-62a8-4939-84e0-b17638d8a424/remove/validation/123", s.ID)).Delete()
	as.Equal(406, res.Code)

	// --- грешно id на валидацията ---///
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/remove/validation/a2d044c6-62a8-4939-84e0-b17638d8a424", s.ID, c.ID)).Delete()
	as.Equal(406, res.Code)

	////-----------Всичко 6------------//
	as.DBDelta(-1, "cell_edit_validation_rules", func() {
		res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/remove/validation/%s", s.ID, c.ID, c.ValidationRules[0].ID)).Delete()
		as.Equal(200, res.Code)
	})
}

func addRelation(as *ActionSuite) models.Column {
	as.DB.Destroy(&models.Site{})

	s := models.Site{
		Name: nulls.NewString("Test_Columns_Relations"),
		Desc: nulls.NewString("Site333Desc"),
		Link: nulls.NewString("localhost:3000")}
	as.NoError(models.DB.Create(&s))

	//Aко не е логнат
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/relation/", s.ID)).Put("")
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Go6o"),
		Email: nulls.NewString("test@go6oAdRel.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	//Логва се
	as.Session.Set("current_user_id", u.ID)

	//Aко не е част от сайт
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/relation/", s.ID)).Put(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/relation/", s.ID)).Put(nil)
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	// ------ Пробва да направи заяква с невалидно column_id -----//
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/relation/", s.ID)).Put("")
	as.Equal(406, res.Code)

	// ------ Пробва да направи заяква с несъществуващо column_id -----///
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/a2d044c6-62a8-4939-84e0-b17638d8a424/add/relation/", s.ID)).Put("")
	as.Equal(406, res.Code)

	//OriginTable
	var oTbl models.Table
	oTbl = models.Table{
		Name:    "columns",
		Verbose: nulls.NewString("originTable"),
		Access:  "admin",
	}

	//RelationTable
	var rTbl models.Table
	rTbl = models.Table{
		Name:   "tables",
		Access: "admin",
	}

	dbc := models.DBConn{
		Driver:   "postgres",
		User:     "postgres",
		Host:     "localhost",
		Password: "postgres",
		Port:     5432,
		DB:       "kp_admin_development",
		Name:     "test db conn",
		Tables:   []models.Table{oTbl, rTbl},
		Site:     s,
	}
	verrs, err := dbc.Create(models.DB)
	as.False(verrs.HasAny())
	as.NoError(err)

	as.DB.Where("verbose_name = ?", oTbl.Verbose.String).First(&oTbl)
	as.NotEqual(oTbl.ID, uuid.Nil)

	oTbl.Columns = models.Columns{
		{Column: "table_id", Verbose: nulls.NewString("OriginColumnTableID")},
		{Column: "id"}, {Column: "verbose_name"},
	}
	err = oTbl.SaveColumns(as.DB)
	as.NoError(err)

	var originCol models.Column
	err = as.DB.Where("verbose_name = ?", "OriginColumnTableID").First(&originCol)
	as.NoError(err)
	as.NotEqual(originCol.ID, uuid.Nil)

	// ------ Пробва да направи заяква с валиден url но празен put -----//
	url := fmt.Sprintf("/site/%s/db/table/column/%s/add/relation/", s.ID, originCol.ID)
	var r models.Relation
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)

	r.OriginColumn = originCol.Column
	r.OriginTable = oTbl.Name
	r.RelationTable = rTbl.Name
	r.RelationColumn = "id"
	r.ReplaceColumn = "name"

	// ------ Пробва да направи заяква с валиден url но празно originColumn -----//
	r.OriginColumn = ""
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)
	r.OriginColumn = originCol.Column

	// ------ Пробва да направи заяква с валиден url но празно originTable -----//
	r.OriginTable = ""
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)
	r.OriginTable = oTbl.Name

	// ------ Пробва да направи заяква с валиден url но празно relationColumn -----//
	r.RelationColumn = ""
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)
	r.RelationColumn = "id"

	// ------ Пробва да направи заяква с валиден url но празно relationTable -----//
	r.RelationTable = ""
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)
	r.RelationTable = rTbl.Name

	// ------ Пробва да направи заяква с валиден url но празно ReplaceColumn -----//
	r.ReplaceColumn = ""
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)
	r.ReplaceColumn = "name"

	//----- Пробва да направи заяква с грешни данни ----//
	r.RelationTable = "ivan"
	res = as.JSON(url).Put(r)
	as.Equal(422, res.Code)
	r.RelationTable = rTbl.Name

	//---------- Валидни данни ------------------------//
	as.DBDelta(1, "relations", func() {
		res = as.JSON(url).Put(r)
		as.Equal(200, res.Code)
	})

	//-------- Пробва да запази 2-ра релация за същата колона --- //
	res = as.JSON(url).Put(r)
	as.Equal(403, res.Code)
	return originCol
}

func addMutation(as *ActionSuite) models.Column {
	as.DB.Destroy(&models.Site{})

	s := models.Site{
		Name: nulls.NewString("Test_Columns_Mutations"),
		Desc: nulls.NewString("Site333Desc"),
		Link: nulls.NewString("localhost:3000")}
	as.NoError(models.DB.Create(&s))

	//Aко не е логнат
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/mutation/", s.ID)).Put("")
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Go6oMutant"),
		Email: nulls.NewString("test@go6oAdRel.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	//Логва се
	as.Session.Set("current_user_id", u.ID)

	//Aко не е част от сайт
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/relation/", s.ID)).Put(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/relation/", s.ID)).Put(nil)
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	// ------ Пробва да направи заяква с невалидно column_id -----//
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/mutation/", s.ID)).Put("")
	as.Equal(406, res.Code)

	// ------ Пробва да направи заяква с несъществуващо column_id -----///
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/a2d044c6-62a8-4939-84e0-b17638d8a424/add/mutation/", s.ID)).Put("")
	as.Equal(406, res.Code)

	col := models.Column{
		Column:   "mutation_column",
		Verbose:  nulls.NewString("MutationColumn"),
		DataType: "string",
	}
	as.NoError(as.DB.Save(&col))

	// -------Пробва да направяи заяка с валидно id но празен put ---- //
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/mutation/", s.ID, col.ID)).Put(&models.MutationFunc{})
	as.Equal(403, res.Code)

	mf := models.MutationFunc{
		FuncName: "nikoa",
	}

	// -------Пробва да направяи заяка с валидно id но невалиден put ---- //
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/mutation/", s.ID, col.ID)).Put(mf)
	as.Equal(403, res.Code)

	// ---- Всичко валидно ---- //
	as.DBDelta(1, "mutation_functions", func() {
		mf.FuncName = "int2bool"
		res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/mutation/", s.ID, col.ID)).Put(mf)
		as.Equal(200, res.Code)
	})

	//--- Пробва да добави 2-ра мутаця за същата колона --- //
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/mutation/", s.ID, col.ID)).Put(mf)
	as.Equal(403, res.Code)
	return col
}

func addValidation(as *ActionSuite) models.Column {
	as.DB.Destroy(&models.Site{})

	s := models.Site{
		Name: nulls.NewString("Test_Columns_Mutations"),
		Desc: nulls.NewString("Site333Desc123"),
		Link: nulls.NewString("localhost:3000")}
	as.NoError(models.DB.Create(&s))

	//Aко не е логнат
	res := as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/validation/", s.ID)).Put("")
	as.Equal(302, res.Code)

	u := &models.User{
		Name:  nulls.NewString("Go6oMutant"),
		Email: nulls.NewString("test@go6oAdRel.test"),
	}
	err := as.DB.Create(u)
	as.NoError(err)
	//Логва се
	as.Session.Set("current_user_id", u.ID)

	//Aко не е част от сайт
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/validation/", s.ID)).Put(nil)
	as.Equal(302, res.Code)
	as.Equal("/site/all/", res.Header().Get("Location"))
	//Ако е част от сайта, но не admin или owner
	u.JoinSite(&s, as.DB, "rw")
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/validation/", s.ID)).Put(nil)
	as.Equal(302, res.Code)
	u.LeaveSite(&s, as.DB)
	u.JoinSite(&s, as.DB, "admin")

	// ------ Пробва да направи заяква с невалидно column_id -----//
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/123/add/validation/", s.ID)).Put("")
	as.Equal(406, res.Code)

	// ------ Пробва да направи заяква с несъществуващо column_id -----///
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/a2d044c6-62a8-4939-84e0-b17638d8a424/add/validation/", s.ID)).Put("")
	as.Equal(406, res.Code)

	col := models.Column{
		Column:   "validation_column",
		Verbose:  nulls.NewString("ValidationColumn"),
		DataType: "string",
	}
	as.NoError(as.DB.Save(&col))

	// -------Пробва да направяи заяка с валидно id но празен put ---- //
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/validation/", s.ID, col.ID)).Put(&models.MutationFunc{})
	as.Equal(403, res.Code)

	v := models.CellEditValidationRule{
		Function: "invalid",
	}

	// -------Пробва да направяи заяка с валидно id но невалиден put ---- //
	res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/validation/", s.ID, col.ID)).Put(v)
	as.Equal(403, res.Code)

	// ---- Всичко валидно ---- //
	v.Function = "email"
	as.DBDelta(1, "cell_edit_validation_rules", func() {
		res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/validation/", s.ID, col.ID)).Put(v)
		as.Equal(200, res.Code)
	})

	//--- Пробва да добави 2-ра мутаця за същата колона (няма проблем) --- //
	as.DBDelta(1, "cell_edit_validation_rules", func() {
		res = as.JSON(fmt.Sprintf("/site/%s/db/table/column/%s/add/validation/", s.ID, col.ID)).Put(v)
		as.Equal(200, res.Code)
	})

	as.NoError(as.DB.Eager().Find(&col, col.ID))

	return col
}

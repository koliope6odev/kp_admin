package actions

//---------------------------------------------------------
// Контролер, който се грижи за най усновните функции
// връща index.html-a или 404 и 500 страниците за грешка
//---------------------------------------------------------

import (
	"io/ioutil"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/plush"
)

// HomeHandler връща началната станица
func HomeHandler(c buffalo.Context) error {
	return c.Render(200, r.HTML("index.html"))
}

//ErrorPage Страница за 404, 500
func ErrorPage(status int, err error, c buffalo.Context) error {
	//Задава  content-type = text/html
	c.Response().Header().Set("content-type", "text/html; charset=utf-8")
	//Намира html, който да покаже
	b, err := ioutil.ReadFile(envy.Get("ERROR_PAGE_PATH", "templates/errors/errors.html"))
	//Компилира го с помоща на plush
	ctx := plush.NewContext()
	ctx.Set("errorCode", status)
	s, err := plush.Render(string(b), ctx)
	//Връща компилирания html
	res := c.Response()
	res.WriteHeader(status)
	res.Write([]byte(s))
	return nil
}

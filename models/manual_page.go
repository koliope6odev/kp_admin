package models

// -----------------------------------------------------
//
//  Модел за страниците на ръководството за потребители
//
// ------------------------------------------------------

import (
	"encoding/json"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/pkg/errors"
	"time"

	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

type ManualPage struct {
	ID        uuid.UUID `db:"id" json:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Title           string       `db:"title" json:"title"`   //Заглавие на страницата
	Text            string       `db:"text" json:"text"`     //Текст на страницата
	Screenshots     []string     `db:"-" json:"screenshots"` //Скрийншоти
	ScreenshotsJson nulls.String `db:"screenshots" json:"-"`
}

type ManualPages []ManualPage

func (m ManualPage) String() string {
	ju, _ := json.Marshal(m)
	return string(ju)
}

//Create wrapper за създаване на страници
func (m *ManualPage) Create(tx *pop.Connection) (*validate.Errors, error) {
	b, err := json.Marshal(m.Screenshots)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	m.ScreenshotsJson = nulls.NewString(string(b))
	return tx.ValidateAndCreate(m)
}

//Update wrapper за редактиране на страници
func (m *ManualPage) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(m)
}

//Зареждане на всички страници
func (m *ManualPages) GetAllPages() error {
	var pages []ManualPage
	if err := DB.Eager().Order("created_at asc").All(&pages); err != nil {
		return errors.WithStack(err)
	}

	//Временна променлива
	mm := *m
	for _, page := range pages {
		mm = append(mm, page)
	}
	*m = mm

	return nil
}

//AfterFind вика се след намиране на запис
func (m *ManualPage) AfterFind(tx *pop.Connection) error {
	if m.ScreenshotsJson.String != "" {
		if err := json.Unmarshal([]byte(m.ScreenshotsJson.String), &m.Screenshots); err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (m ManualPage) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (m ManualPage) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

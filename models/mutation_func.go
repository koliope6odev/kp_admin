package models

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
)

// ----------------------------------
//
//  Model за функциите за промяна на стойностите на някоя колона
//    Пример_01  - timestamp -> timestring
//      1551859170 -> "2019-06-03 (15:04)"
//    Пример_02 - 0/1 -> bool(True)/bool(false)
//
// ----------------------------------

type MutationFunc struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	ColumnID uuid.UUID `db:"column_id"` //към коя колона е "прикачена" мутацията

	FuncName string            `json:"func,omitempty"  db:"mutation_function"`
	Dict     nulls.String      `json:"dict,omitempty" db:"dictionary"` //ако функцията е dict
	DictMap  map[string]string `json:"-" db:"-"`
	StringF  nulls.String      `json:"stringF,omitempty" db:"string_f"` //ако функцията е за добавяне на string-ове
}

func (m MutationFunc) TableName() string {
	return "mutation_functions"
}

// String is not required by pop and may be deleted
func (m MutationFunc) String() string {
	jm, _ := json.Marshal(m)
	return string(jm)
}

// MutationFuncs is not required by pop and may be deleted
type MutationFuncs []MutationFunc

// String is not required by pop and may be deleted
func (m MutationFuncs) String() string {
	jm, _ := json.Marshal(m)
	return string(jm)
}

//Mutate връща променен string
func (m MutationFunc) Mutate(s string) string {
	switch m.FuncName {
	case "timestamp2timestring":
		return m.timestamp2timestring(s)
	case "int2bool":
		return m.int2bool(s)
	case "dict":
		return m.dict(s)
	case "stringF":
		return m.strF(s)
	}
	return s
}

//MutateReverse връща променен string
func (m MutationFunc) MutateReverse(s string) string {
	switch m.FuncName {
	case "timestamp2timestring":
		return m.timestring2timestamp(s)
	case "int2bool":
		return m.bool2int(s)
	case "dict":
		return m.dictReverse(s)
	case "stringF":
		return m.strFReverse(s)
	}
	return s
}

func (m *MutationFunc) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(m)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (m *MutationFunc) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error

	return validate.Validate(
		&validators.UUIDIsPresent{Name: "column_id", Field: m.ColumnID},
		&validators.StringIsPresent{Name: "function_name", Field: m.FuncName},
		&validators.StringInclusion{Name: "function_name", Field: m.FuncName,
			List: []string{"dict", "int2bool", "timestamp2timestring", "stringF"},
		},
		//Дали има dict
		&validators.FuncValidator{
			Field: m.Dict.String,
			Name:  t("mutationf_dict"),
			Fn: func() bool {
				if m.FuncName != "dict" {
					return true
				}
				if m.Dict.String == "" {
					return false
				}
				//Дали е валиден json ( {k:v})
				var js map[string]interface{}
				return json.Unmarshal([]byte(m.Dict.String), &js) == nil

			},
		},
		//Дали има stringF
		&validators.FuncValidator{
			Field:   m.StringF.String,
			Name:    "stringF",
			Message: t("mutationf_stringf"),
			Fn: func() bool {
				if m.FuncName != "stringF" {
					return true
				}
				if m.StringF.String == "" {
					return false
				}
				//Дали е валиден str+{{@}}+str
				return strings.Contains(m.StringF.String, "{{@}}")

			},
		},
		//Дали вече има мутация за тази колона
		&validators.FuncValidator{
			Field:   m.ColumnID.String(),
			Name:    "Column",
			Message: t("mutationf_hasf"),
			Fn: func() bool {
				var b bool
				b, err = tx.Where("column_id = ?", m.ColumnID).Exists(&MutationFunc{})
				if err != nil {
					return false
				}
				return !b
			},
		},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (m *MutationFunc) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (m *MutationFunc) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

func (m *MutationFunc) BeforeCreate(tx *pop.Connection) {
	if m.FuncName != "dict" {
		m.Dict = nulls.String{}
	}
	if m.FuncName != "stringF" {
		m.StringF = nulls.String{}
	}
}

//timestamp2timestring unix timestamp -> timestring
func (m MutationFunc) timestamp2timestring(s string) string {
	timestamp, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return "NULL"
	}
	return time.Unix(timestamp, 0).Format("2006-01-02 (15:04)")
}

//timestring2timestamp timestring->unix timestamp
//Обратната функция на timestamp2timestring
func (m MutationFunc) timestring2timestamp(s string) string {
	t, err := time.Parse("2006-01-02 (15:04)", s)
	if err != nil {
		return "NULL"
	}
	return fmt.Sprintf("%d", t.Unix())
}

//int2bool превръща число във boolean
// 0/1 -> Bool(False)/Bool(True)
func (m MutationFunc) int2bool(s string) string {
	if s == "0" {
		return "Bool(False)"
	}
	return "Bool(True)"
}

//bool2int превръща boolean (стрингова репрезентация) в число
//Обратна функция на int2bool
//0/1 -> Bool(False)/Bool(True)
func (m MutationFunc) bool2int(s string) string {
	if s == "Bool(False)" {
		return "0"
	}
	return "1"
}

//dict прави mapping към данни от базата
//{k:v}
func (m MutationFunc) dict(s string) string {
	err := json.Unmarshal([]byte(m.Dict.String), &m.DictMap)
	if err != nil {
		return "NULL"
	}
	if m.DictMap[s] != "" {
		return m.DictMap[s]
	}
	return "NULL"
}

//dictReverse обратната фунция на dict
func (m MutationFunc) dictReverse(s string) string {
	err := json.Unmarshal([]byte(m.Dict.String), &m.DictMap)
	if err != nil {
		return "NULL"
	}
	for k, v := range m.DictMap {
		if s == v {
			return k
		}
	}
	return "NULL"
}

//strF форматира стринг
// добавя string-oве от вете страни да оригиналния текст
//използва m.StringF за pattern
// prependStr+{{@}}+appendStr
func (m MutationFunc) strF(s string) string {
	return strings.Replace(m.StringF.String, "{{@}}", s, -1)
}

//strFReverse обратната фукнция на strF
func (m MutationFunc) strFReverse(s string) string {
	f := m.StringF.String //формата
	prefix := f[0:strings.Index(f, "{{@}}")]
	suffix := f[strings.Index(f, "{{@}}")+5:]
	formated := strings.Replace(s, prefix, "", -1) //деформатирания текст
	formated = strings.Replace(formated, suffix, "", -1)
	return formated
}

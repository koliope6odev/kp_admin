package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/gobuffalo/envy"
	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

// -----------------------------------------------
//
//  Модел за връзките към отдалечени БД
//
// Добавя/редактира връзки
// Намира имената на таблицте в дадена база
// Намира колоните в дадена таблица
// Проверява, дали е възможна връзка със сървъра
// и др
// -----------------------------------------------

type DBConn struct {
	ID        uuid.UUID `json:"id" db:"id" form:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Driver   string `json:"type" db:"driver" form:"type"` //Вида на връзката - mysql, postgresSql ...
	User     string `json:"user" db:"-" form:"user"`      //Потребител за сврързване с базата
	Password string `json:"-" db:"-" form:"password"`     // парола
	Port     uint16 `json:"port" db:"port" form:"port"`   //порта на който да се свърже
	Host     string `json:"host" db:"-"  form:"host"`     //хоста, на който трябва да се свърже
	DB       string `json:"db" db:"-"  form:"db"`         // база данни, към която да се свърже
	Tables   Tables `json:"tables" db:"-"`

	Name string `json:"connection_name" form:"connection_name" db:"connection_name"`

	UserEnc     []byte `json:"-" form:"-" db:"username"`  //Криптиран потребител за сврързване с базата
	PasswordEnc []byte `json:"-" form:"-" db:"password" ` //Криптирана парола
	HostEnc     []byte `json:"-" form:"-" db:"host"`      //хоста, на който трябва да се свърже
	DBEnc       []byte `json:"-" form:"-" db:"db"`        //криптирано име на базата данни

	Site   Site      `db:"-" json:"site" form:"site"`
	SiteID uuid.UUID `db:"site" json:"site_id"`
}

// String is not required by pop and may be deleted
func (dbc DBConn) String() string {
	ju, _ := json.Marshal(dbc)
	return string(ju)
}

func (dbc DBConn) TableName() string {
	return "databases"
}

//Create wrapper около логиката за създаване на запис за  ДБ
//Пуска валидации и криптира чуствителните данни
func (dbc *DBConn) Create(tx *pop.Connection) (*validate.Errors, error) {

	//Криптиране на чувствителните данни
	key := envy.Get("APP_KEY", "password_123")
	dbc.HostEnc = encrypt(dbc.Host, key)
	dbc.DBEnc = encrypt(dbc.DB, key)
	dbc.UserEnc = encrypt(dbc.User, key)
	dbc.PasswordEnc = encrypt(dbc.Password, key)

	dbc.SiteID = dbc.Site.ID

	return tx.ValidateAndCreate(dbc)
}

//Update wrapper около логиката за редактиране  на запис за  ДБ
//Пуска валидации и криптира чуствителните данни
func (dbc *DBConn) Update(tx *pop.Connection) (*validate.Errors, error) {
	//Криптиране на чувствителните данни
	key := envy.Get("APP_KEY", "password_123")
	dbc.HostEnc = encrypt(dbc.Host, key)
	dbc.DBEnc = encrypt(dbc.DB, key)
	dbc.UserEnc = encrypt(dbc.User, key)
	dbc.PasswordEnc = encrypt(dbc.Password, key)
	dbc.SiteID = dbc.Site.ID
	return tx.ValidateAndUpdate(dbc)
}

func (dbc DBConn) getConnString() string {
	switch dbc.Driver {
	case "mysql":
		//[username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
		return fmt.Sprintf("%s:%s@(%s:%d)/%s?timeout=10s",
			dbc.User, dbc.Password, dbc.Host, dbc.Port, dbc.DB)
	case "postgres":
		return fmt.Sprintf("host=%s port=%d user=%s "+
			"password=%s dbname=%s sslmode=disable connect_timeout=10",
			dbc.Host, dbc.Port, dbc.User, dbc.Password, dbc.DB)

	}
	return ""

}

// DBConns is not required by pop and may be deleted
type DBConns []DBConn

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (dbc *DBConn) Validate(tx *pop.Connection) (*validate.Errors, error) {
	vals := []validate.Validator{
		&validators.StringIsPresent{Field: dbc.User, Name: "Username"},
		&validators.StringIsPresent{Field: dbc.Host, Name: "Host"},
		&validators.StringIsPresent{Field: dbc.Driver, Name: "Type"},
		&validators.IntIsPresent{Field: int(dbc.Port), Name: "Port"},
		&validators.StringIsPresent{Field: dbc.DB, Name: "DB"},
		&validators.StringInclusion{Field: dbc.Driver, Name: "Type", List: []string{"mysql", "postgres"}},
		&validators.UUIDIsPresent{Field: dbc.Site.ID, Name: "site", Message: t("dbconn.noSite")},
	}

	if "production" == envy.Get("GO_ENV", "development") && "false" == envy.Get("ALLOW_DB_HOST_LOCALHOST", "false") {
		//Проверява, дали е localhost и нещо подобно
		vals = append(vals,
			&validators.FuncValidator{
				Field:   dbc.Host,
				Name:    "host",
				Message: t("dbconn.forbiddenHost"),
				Fn: func() bool {
					domains := []string{"localhost", "localdomain", "127.0.0.1"}
					for _, d := range domains {
						if strings.Contains(dbc.Host, d) {
							return false
						}
					}
					return true
				},
			})
	}

	//var err error
	return validate.Validate(vals...), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (dbc *DBConn) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		// проверява, дали името не е заето
		&validators.FuncValidator{
			Field:   dbc.Name,
			Name:    "Име",
			Message: t("dbconn.nameTaken"),
			Fn: func() bool {
				var b bool
				dbc.SiteID = dbc.Site.ID
				q := tx.Where("connection_name = ?", dbc.Name).Where("site=?", dbc.SiteID)
				if dbc.ID != uuid.Nil {
					q = q.Where("id != ?", dbc.ID)
				}
				b, err = q.Exists(dbc)
				if err != nil {
					return false
				}
				return !b
			},
		},
	), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (dbc *DBConn) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

func (dbc *DBConn) AfterFind(tx *pop.Connection) error {
	//За декриптация на чувствителните данни
	k := envy.Get("APP_KEY", "password_123")

	dbc.User = decrypt(dbc.UserEnc, k)
	dbc.Host = decrypt(dbc.HostEnc, k)
	dbc.Password = decrypt(dbc.PasswordEnc, k)
	dbc.DB = decrypt(dbc.DBEnc, k)

	//За зареждане на таблиците
	tbls := Tables{}
	tx.Where("db=?", dbc.ID).Order("created_at desc").All(&tbls)
	for _, t := range tbls {
		t.DB = *dbc
		dbc.Tables = append(dbc.Tables, t)
	}

	//за зареждане на сайта
	s := Site{}
	tx.Find(&s, dbc.SiteID)
	dbc.Site = s
	return nil
}

//AfterCreate вика се след създаване на запис
func (dbc *DBConn) AfterCreate(tx *pop.Connection) error {
	e := make(chan error)
	//Запазване на relate-натите таблици
	for _, t := range dbc.Tables {
		t.DB = *dbc
		go func(t Table) {
			err := tx.Create(&t)
			e <- err
		}(t)
	}
	for range dbc.Tables {
		if err := <-e; err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}

//AfterUpdate вика се след промяна ня някой запис
func (dbc *DBConn) AfterUpdate(tx *pop.Connection) error {
	tx = DB
	//таблиците, които вече са запазени
	var savedTbls Tables
	tx.Where("db = ?", dbc.ID).All(&savedTbls)
	//таблиците, които не са запазени
	var tbls Tables
	for _, t := range dbc.Tables {
		//Проверява дали  е някое от savedTbls
		if !tableIn(t, savedTbls) {
			tbls = append(tbls, t)
		}
	}

	//таблиците, които трябва да изтрие
	var toDelTbls Tables
	for _, t := range savedTbls {
		if !tableIn(t, dbc.Tables) {
			toDelTbls = append(toDelTbls, t)
		}
	}

	if err := tx.Destroy(&toDelTbls); err != nil {
		return errors.WithStack(err)
	}

	e := make(chan error)
	//Запазване на relate-натите таблици
	for _, t := range tbls {
		t.DB = *dbc
		go func(t Table) {
			err := tx.Create(&t)
			e <- err
		}(t)
	}
	for range tbls {
		if err := <-e; err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

//CheckConnection проверява, дали може да се свърже
func (dbc *DBConn) CheckConnection() (bool, error) {
	//[username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
	db, err := sql.Open(dbc.Driver, dbc.getConnString())
	defer db.Close()

	err = db.Ping()
	if err != nil {
		return false, errors.WithStack(err)
	}
	return true, nil
}

//GetTables връща информация за таблиците в базата
func (dbc *DBConn) GetTables() ([]string, error) {

	var tables []string

	db, err := sql.Open(dbc.Driver, dbc.getConnString())
	defer db.Close()

	q := queries[dbc.Driver]["tables"]

	rows, err := db.Query(q)

	if err != nil {
		return []string{}, errors.WithStack(err)
	}

	for rows.Next() {
		var table string
		err := rows.Scan(&table)
		if err != nil {
			return []string{}, errors.WithStack(err)
		}
		if dbc.Driver == "postgres" {
			table = table[7:]
		}
		tables = append(tables, table)
	}
	err = rows.Err()
	if err != nil {
		return []string{}, errors.WithStack(err)
	}
	defer rows.Close()
	return tables, nil
}

//GetColumns връща  именат на редовете за дадена таблица
func (dbc *DBConn) GetColumns(table string) (Columns, error) {
	db, _ := sql.Open(dbc.Driver, dbc.getConnString())
	defer db.Close()

	rows, err := db.Query(queries[dbc.Driver]["columns"], dbc.DB, table)
	defer rows.Close()

	if err != nil {
		return nil, errors.WithStack(err)
	}
	var columns Columns

	for rows.Next() {
		//column
		var tableName string
		var dataType string
		err := rows.Scan(&tableName, &dataType)

		if err != nil {
			return nil, errors.WithStack(err)
		}

		//Избира правилния тип данни
		var verboseDt string //тривиалното име на типа данни
		for k := range dataTypes[dbc.Driver] {
			if verboseDt != "" {
				break
			}
			for _, dt := range dataTypes[dbc.Driver][k] {
				if dt == strings.Fields(dataType)[0] {
					verboseDt = k
					break
				}
			}
		}

		//Задава readonly
		rOnly := false
		for _, dt := range readOnly[dbc.Driver] {
			if dt == strings.Fields(dataType)[0] {
				rOnly = true
			}
		}

		//Задава че колона е "ключ" (само за mySQL)
		if dbc.Driver == "mysql" && (tableName == "id" || tableName == "pk") {
			rOnly = true
			verboseDt = "id"
		}

		columns = append(columns, Column{
			Column:           tableName,
			DataType:         verboseDt,
			DataTypeOriginal: dataType,
			DataTypeReadOnly: rOnly,
		})
	}
	return columns, nil
}

//GetConns връща всички връзки за даден сайт
func (dbcs *DBConns) GetConns(tx *pop.Connection, site Site) error {
	if err := tx.Where("site=?", site.ID).Order("created_at desc").All(dbcs); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

//BeforeDestroy вика се преди да се изтрие
//изтрива диаграми и таблици
func (dbc DBConn) BeforeDestroy(tx *pop.Connection) error {
	var tbls Tables
	var charts Charts
	if err := tx.Where("db=?", dbc.ID).All(&tbls); err != nil {
		return errors.WithStack(err)
	}
	if err := tx.Where("dbid=?", dbc.ID).All(&charts); err != nil {
		return errors.WithStack(err)
	}
	if err := tx.Destroy(&tbls); err != nil {
		return errors.WithStack(err)
	}
	if err := tx.Destroy(&charts); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//tableIn проверява, дали някоя таблица е част от масив с таблици (сравнява по id или name)
func tableIn(t Table, ts Tables) bool {
	for _, i := range ts {
		if i.ID == t.ID || t.Name == i.Name {
			return true
		}
	}
	return false
}

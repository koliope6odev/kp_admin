package actions

//КОМЕНТИРАНО Е, ЗАЩОТО
//КОГАТО СЕ ПУСКАТ ВСИЧКИ ТЕСТОВЕ НА ПРИЛОЖЕНИЕТО
//ОТНЕМА ТВЪРДЕ МНОГО ВРЕМЕ
//ЗА ИЗПЪЛНЕНИЕ
//ЗА ДА ТЕСТВАШ НЕЩА
//СВРЪЗАНИ С ВРЪЗКИ ТЕ ЗА БАЗИ ДАННИ  - ОТКОМЕНТИРАЙ !

//func (as *ActionSuite) Test_DbConnsCheckConn() {
//
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//
//	//логва се
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//Сайта, към който е базата
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&site)
//	url := fmt.Sprintf("/site/%s/db/check/connection", site.ID)
//	//mysql
//	dbcM := models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//		Site:     site,
//	}
//
//	//postgres
//	dbcP := models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Site:     site,
//	}
//
//	//Aко не е част от сайт
//	res := as.HTML(url).Post(dbcM)
//	as.Equal(302, res.Code)
//	as.Equal("/site/all/", res.Header().Get("Location"))
//	u.JoinSite(&site, as.DB, "admin")
//
//	//ВАЛИДНИ ДАННИ
//	//при MySQL
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(200, res.Code)
//
//	//при PostgreSQL
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(200, res.Code)
//
//	//ГРЕШЕН HOST
//	dbcM.Host, dbcP.Host = "fakehost.ivan", "fakehost.ivan"
//
//	//при MySQL
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), "fakehost.ivan: no such host")
//
//	//при PostgreSQL
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), "fakehost.ivan: no such host")
//
//	dbcM.Host = "194.135.94.89"
//	dbcP.Host = "localhost"
//
//	//ГРЕШНА ПАРОЛА
//	dbcM.Password = "wrong_pass"
//	//при MySQL
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), "Access denied")
//
//	dbcM.Password = "nikolape6o"
//
//	//НЕ правя проверка за postgres localhost, защото може да се свърже и без парола
//
//	//ГРЕШЕН USER
//
//	dbcM.User, dbcP.User = "fake_user", "fake_user"
//
//	//при MySQL
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), "Access denied")
//
//	//при PostgreSQL
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), "\\\"fake_user\\\" does not exist")
//
//	dbcM.User, dbcP.User = "kp_admin", "postgres"
//
//	//ГРЕШЕН ПОРТ
//
//	dbcM.Port, dbcP.Port = 1000, 1000
//
//	//при MySQL
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), ":1000: connect: connection refused")
//
//	//при PostgreSQL
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), ":1000: connect: connection refused")
//
//	dbcM.Port, dbcP.Port = 3306, 5432
//
//	//ГРЕШЕН ДРАЙВЪР
//	dbcM.Driver = "2mysql1"
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(403, res.Code)
//	as.Contains(res.Body.String(), "Type is not in the list [mysql, postgres]")
//	dbcM.Driver = "mysql"
//
//	//ПРОВЕРКА ПРИ HOST = GOOGLE.COM - проверява за timeout
//	dbcM.Host = "google.com"
//	dbcM.Password = "1234"
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(422, res.Code)
//	//as.Contains(res.Body.String(), "timeout")
//	dbcM.Host = "194.135.94.89"
//	dbcM.Password = "nikolape6o"
//
//	dbcP.Host = "google.com"
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(422, res.Code)
//	as.Contains(res.Body.String(), "timeout")
//	dbcP.Host = "localhost"
//
//	//ПРОВЕРКА ЗА СВЪРЗВАНЕ С localhost, без парола
//	dbcP.Password = ""
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(200, res.Code)
//	dbcP.Password = "postgres"
//
//	//ПРОВЕРКА ЗА driver = ""
//	dbcP.Driver = ""
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(403, res.Code)
//	dbcP.Driver = "postgres"
//
//	//ПРОВЕРКА ЗА user = ""
//	dbcP.User = ""
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(403, res.Code)
//	dbcP.User = "postgres"
//
//	//ПРОВЕРКА ЗА port = ""
//	dbcP.Port = 0
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(403, res.Code)
//	dbcP.Port = 5432
//
//	//ПРОВЕРКА ЗА host = ""
//	dbcP.Host = ""
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(403, res.Code)
//	dbcP.Host = "localhost"
//
//	//ПРОВЕКА ЗА Повторно използване на име
//
//}
//
//func (as *ActionSuite) Test_DBConnsTableCheckConn() {
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//	as.DB.Destroy(&models.SitesUsers{})
//	//Сайта, към който е базата
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&site)
//
//	//логва се
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//postgres
//	dbc := models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Site:     site,
//		Tables: models.Tables{
//			{Name: "users", Verbose: nulls.NewString("testing_connection_users")},
//			{Name: "not_existing", Verbose: nulls.NewString("testing_connection_not_existing")},
//		},
//	}
//
//	verrs, err := dbc.Create(as.DB)
//	as.False(verrs.HasAny(), fmt.Sprintf("Има грешки %v", verrs.Errors))
//	as.NoError(err)
//
//	var t models.Table
//	err = as.DB.Where("verbose_name = ?", "testing_connection_users").First(&t)
//	as.NoError(err)
//	as.False(t.ID == uuid.Nil)
//
//	url := fmt.Sprintf("/site/%s/db/table/check/conn/%s", site.ID, t.ID)
//
//	//Aко не е част от сайт
//	res := as.JSON(url).Post("")
//	as.Equal(302, res.Code)
//	as.Equal("/site/all/", res.Header().Get("Location"))
//	u.JoinSite(&site, as.DB, "admin")
//
//	res = as.JSON(url).Post("")
//	as.Equal(200, res.Code)
//
//	//----- Невалидни данни за свързване ------ //
//	dbc.User = "invalid_user"
//	verrs, err = dbc.Update(as.DB)
//	as.False(verrs.HasAny(), fmt.Sprintf("Има грешки %v", verrs.Errors))
//	as.NoError(err)
//
//	res = as.JSON(url).Post("")
//	as.Equal(422, res.Code)
//
//	dbc.User = "postgres"
//	verrs, err = dbc.Update(as.DB)
//	as.False(verrs.HasAny(), fmt.Sprintf("Има грешки %v", verrs.Errors))
//	as.NoError(err)
//
//	//------ Невалидно име на таблица ------- //
//	err = as.DB.Where("verbose_name = ?", "testing_connection_not_existing").First(&t)
//	as.NoError(err)
//	as.False(t.ID == uuid.Nil)
//	url = fmt.Sprintf("/site/%s/db/table/check/conn/%s", site.ID, t.ID)
//	res = as.JSON(url).Post("")
//	as.Equal(422, res.Code)
//}
//
//func (as *ActionSuite) Test_DbConnsGetTables() {
//
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//
//	//логва се
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//Сайта, към който е базата
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("localhost:3000")}
//	models.DB.Create(&site)
//	url := fmt.Sprintf("/site/%s/db/get/tables", site.ID)
//	//mysql
//	dbcM := models.DBConn{
//		Driver:   "mysql",
//		Host:     "194.135.94.89",
//		User:     "kp_admin",
//		Password: "nikolape6o",
//		DB:       "zora",
//		Port:     3306,
//		Site:     site,
//	}
//
//	//postgres
//	dbcP := models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Site:     site,
//	}
//
//	//Aко не е част от сайт
//	res := as.HTML(url).Post(dbcM)
//	as.Equal(302, res.Code)
//	as.Equal("/site/all/", res.Header().Get("Location"))
//	//Ако е част от сайта, но не admin или owner
//	u.JoinSite(&site, as.DB, "rw")
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(302, res.Code)
//	u.LeaveSite(&site, as.DB)
//	u.JoinSite(&site, as.DB, "admin")
//
//	res = as.HTML(url).Post(dbcM)
//	as.Equal(200, res.Code)
//	as.Contains(res.Body.String(), "users")
//	as.Contains(res.Body.String(), "suppliers")
//	as.Contains(res.Body.String(), "orders")
//	as.Contains(res.Body.String(), "payments")
//	as.Contains(res.Body.String(), "opticans")
//	as.Contains(res.Body.String(), "jobs")
//
//	res = as.HTML(url).Post(dbcP)
//	as.Equal(200, res.Code)
//	as.Contains(res.Body.String(), "users")
//	as.Contains(res.Body.String(), "password_reset")
//	as.Contains(res.Body.String(), "sites")
//
//}
//
//func (as *ActionSuite) Test_DbConnsCreateConn() {
//
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//
//	//логва се
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//Сайта, към който е базата
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("https://localhost:3000")}
//	models.DB.Create(&site)
//
//	//mysql
//	dbc := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Site:     site,
//		Name:     "Връзка 123",
//	}
//	//бройката на записите преди това
//	var dbcs models.DBConns
//	n, err := models.DB.Count(&dbcs)
//	as.NoError(err)
//
//	//Aко не е част от сайт
//	res := as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc)
//	as.Equal(302, res.Code)
//	as.Equal("/site/all/", res.Header().Get("Location"))
//	//Ако е част от сайта, но не admin или owner
//	u.JoinSite(&site, as.DB, "rw")
//	res = as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc)
//	as.Equal(302, res.Code)
//	u.LeaveSite(&site, as.DB)
//	u.JoinSite(&site, as.DB, "admin")
//
//	res = as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc)
//
//	n2, err := models.DB.Count(&dbcs)
//	as.NotEqual(res.Body.String(), uuid.Nil.String())
//	as.NotEqual(res.Body.String(), "")
//	as.NotEqual(n, n2)
//	as.Equal(n2, n+1)
//	as.Equal(200, res.Code)
//
//	//Oпит да направи база данни към същия сайт със същото име
//	dbc2 := deepcopier.Copy(dbc)
//	res = as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc2)
//	as.Equal(403, res.Code)
//
//	//провекра с липсващ сайт
//	dbc3 := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Name:     "Връзка 123 уникално име",
//	}
//	res = as.HTML(fmt.Sprintf("/site/%s/db/create", uuid.Nil)).Put(dbc3)
//	as.Equal(302, res.Code)
//}
//
//func (as *ActionSuite) Test_DbConnsUpdateConn() {
//
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//
//	//логва се
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//Сайта, към който е базата
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("https://localhost:3000")}
//	models.DB.Create(&site)
//
//	//postgres
//	dbc := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Site:     site,
//		Name:     "Връзка за update-ване",
//	}
//
//	//Aко не е част от сайт
//	res := as.HTML(fmt.Sprintf("/site/%s/db/update", dbc.Site.ID)).Put(dbc)
//	as.Equal(302, res.Code)
//	as.Equal("/site/all/", res.Header().Get("Location"))
//	//Ако е част от сайта, но не admin или owner
//	u.JoinSite(&site, as.DB, "rw")
//	res = as.HTML(fmt.Sprintf("/site/%s/db/update", dbc.Site.ID)).Put(dbc)
//	as.Equal(302, res.Code)
//	u.LeaveSite(&site, as.DB)
//	u.JoinSite(&site, as.DB, "admin")
//
//	//Първо запазва
//	as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc)
//	err = models.DB.Where("connection_name = ?", "Връзка за update-ване").First(dbc)
//	as.NoError(err)
//
//	//бройката на записите преди това
//	var dbcs models.DBConns
//	n, err := models.DB.Count(&dbcs)
//	as.NoError(err)
//	res = as.HTML(fmt.Sprintf("/site/%s/db/update", dbc.Site.ID)).Put(dbc)
//
//	n2, err := models.DB.Count(&dbcs)
//	as.NotEqual(res.Body.String(), uuid.Nil.String())
//	as.NotEqual(res.Body.String(), "")
//	as.Equal(n, n2)
//	as.Equal(200, res.Code)
//
//	//Промяна на името
//	dbc.Name = "Edited Name"
//	res = as.HTML(fmt.Sprintf("/site/%s/db/update", dbc.Site.ID)).Put(dbc)
//
//	n2, err = models.DB.Count(&dbcs)
//	as.NotEqual(res.Body.String(), uuid.Nil.String())
//	as.NotEqual(res.Body.String(), "")
//	as.Equal(200, res.Code)
//	var d models.DBConn
//	models.DB.Find(&d, dbc.ID)
//	as.Equal("Edited Name", d.Name)
//
//	//провекра с липсващ сайт
//	dbc3 := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Name:     "Връзка 123 уникално име",
//	}
//	res = as.HTML(fmt.Sprintf("/site/%s/db/update", uuid.Nil)).Put(dbc3)
//	as.Equal(302, res.Code)
//
//}
//
//func (as *ActionSuite) Test_DbConnsDelConn() {
//
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//
//	//логва се
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//Сайта, към който е базата
//	site := models.Site{
//		Name: nulls.NewString("Site111"),
//		Desc: nulls.NewString("Site222"),
//		Link: nulls.NewString("https://localhost:3000")}
//	models.DB.Create(&site)
//
//	//postgres
//	dbc := &models.DBConn{
//		Driver:   "postgres",
//		Host:     "localhost",
//		User:     "postgres",
//		Password: "postgres",
//		DB:       "kp_admin_development",
//		Port:     5432,
//		Site:     site,
//		Name:     "Връзка за delete-ване",
//	}
//
//	//Aко не е част от сайт
//	resJ := as.JSON("/site/%s/db/delete/%s", dbc.Site.ID, dbc.ID).Delete()
//	as.Equal(302, resJ.Code)
//	as.Equal("/site/all/", resJ.Header().Get("Location"))
//	//Ако е част от сайта, но не admin или owner
//	u.JoinSite(&site, as.DB, "rw")
//	resJ = as.JSON("/site/%s/db/delete/%s", dbc.Site.ID, dbc.ID).Delete()
//	as.Equal(302, resJ.Code)
//	u.LeaveSite(&site, as.DB)
//	u.JoinSite(&site, as.DB, "admin")
//
//	//Първо запазва
//	res := as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc)
//	as.Equal(200, res.Code)
//
//	//Запазва още един, да има
//	dbc.Name = "Връзка за delete-ване 2"
//	as.HTML(fmt.Sprintf("/site/%s/db/create", dbc.Site.ID)).Put(dbc)
//	err = models.DB.Where("connection_name = ?", "Връзка за delete-ване").First(dbc)
//	as.NoError(err)
//
//	//бройката на записите преди да изтрия някой това
//	var dbcs models.DBConns
//	n, err := models.DB.Count(&dbcs)
//	as.NoError(err)
//
//	err = models.DB.Where("connection_name = ?", "Връзка за update-ване").First(dbc)
//
//	resJ = as.JSON("/site/%s/db/delete/%s", dbc.Site.ID, dbc.ID).Delete()
//	as.Equal(200, resJ.Code)
//	n2, err := models.DB.Count(&dbcs)
//	as.NoError(err)
//	as.NotEqual(n, n2)
//	as.Equal(n2, n-1)
//
//	//Пробва се да изтрие сайт, който не притежава
//	//имплементирано е чрез middlеwarе-ите
//
//}
//
//func (as *ActionSuite) Test_DbConnsGetConns() {
//
//	as.DB.Destroy(&models.Users{})
//	as.DB.Destroy(&models.Sites{})
//	as.DB.Destroy(&models.DBConns{})
//
//	///site/db/get/connections/{site}
//	u := &models.User{
//		Name:  nulls.NewString("Ivan"),
//		Email: nulls.NewString("test@gmail.test"),
//	}
//	err := as.DB.Create(u)
//	as.NoError(err)
//	as.Session.Set("current_user_id", u.ID)
//
//	//Сайта, към който ще са базите
//	site := models.Site{
//		Name: nulls.NewString("SiteДБС"),
//		Desc: nulls.NewString("Сайт със 5 бази данни"),
//		Link: nulls.NewString("http://manySites.com")}
//	models.DB.Create(&site)
//
//	//Сайтовете, които ще дърпа
//	for i := 1; i <= 5; i++ {
//		dbc := &models.DBConn{
//			Driver:   "postgres",
//			Host:     "http://manySites.com",
//			User:     "nikola",
//			Password: "postgres",
//			DB:       fmt.Sprintf("db%d", i),
//			Port:     5432,
//			Site:     site,
//			Name:     fmt.Sprintf("Връзка %d", i),
//		}
//		models.DB.Create(dbc)
//	}
//
//	//Aко не е част от сайт
//	res := as.HTML(fmt.Sprintf("/site/%s/db/get/connections", site.ID)).Post(nil)
//	as.Equal(302, res.Code)
//	as.Equal("/site/all/", res.Header().Get("Location"))
//	u.JoinSite(&site, as.DB, "admin")
//
//	res = as.HTML(fmt.Sprintf("/site/%s/db/get/connections", site.ID)).Post(nil)
//	as.Equal(200, res.Code)
//
//}

package grifts

import (
	"github.com/gobuffalo/buffalo"
	"github.com/kolioPesho/kp_admin/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}

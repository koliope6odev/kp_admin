package models_test

import (
	"fmt"

	"github.com/gobuffalo/pop/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioPesho/kp_admin/models"
)

//Проверява дали запазва сайт с create метода
func (ms *ModelSuite) Test_Site_Create() {

	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:  nulls.NewString("Name"),
		Desc:  nulls.NewString("Description"),
		Link:  nulls.NewString("Link"),
		Owner: *u,
	}
	_, err := s.Create(models.DB)
	ms.NoError(err)
	ms.Equal(s.OwnerId, u.ID, "Not setting the OwnerId when using Create method")
}

//Провярява GetOwnedBy
func (ms *ModelSuite) Test_Site_GetOwnerSites() {

	u := &models.User{
		Name:  nulls.NewString("NotOwner"),
		Email: nulls.NewString("ivan@example.com"),
	}

	uOwner := &models.User{
		Name:  nulls.NewString("Owner"),
		Email: nulls.NewString("nikola@example.com"),
	}
	models.DB.Save(u)
	models.DB.Save(uOwner)

	s1 := &models.Site{
		Name:    nulls.NewString("Name1"),
		Desc:    nulls.NewString("Description1"),
		Link:    nulls.NewString("Link1"),
		Owner:   *uOwner,
		OwnerId: uOwner.ID,
	}
	//Не е от собственика ни
	s2 := &models.Site{
		Name:    nulls.NewString("Name2"),
		Desc:    nulls.NewString("Description2"),
		Link:    nulls.NewString("Link2"),
		Owner:   *u,
		OwnerId: u.ID,
	}

	s3 := &models.Site{
		Name:    nulls.NewString("Name3"),
		Desc:    nulls.NewString("Description3"),
		Link:    nulls.NewString("Link3"),
		Owner:   *uOwner,
		OwnerId: uOwner.ID,
	}

	s4 := &models.Site{
		Name:    nulls.NewString("Name4"),
		Desc:    nulls.NewString("Description4"),
		Link:    nulls.NewString("Link4"),
		Owner:   *uOwner,
		OwnerId: uOwner.ID,
	}

	models.DB.Save(s1)
	models.DB.Save(s2) //Не е от собственика ни
	models.DB.Save(s3)
	models.DB.Save(s4)

	var owned models.Sites
	ms.NoError(owned.GetOwnedBy(uOwner), "Хвърля грешка при викане не models.Sites.GetOwnedBy()")

	ms.False(len(owned) < 3, fmt.Sprintf("Не е върнало всички сайтове, върнало е само %d сайта", len(owned)))

	ms.False(len(owned) > 3, fmt.Sprintf("Върнало е твърде много сайтове - %d сайта", len(owned)))

	for _, site := range owned {
		ms.NotEqual("Link2", site.Link.String, "Връща сайт, който не е собственост на потребителя")
	}
}

func (ms *ModelSuite) Test_SiteWorkerLink_SaveMethod() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:    nulls.NewString("Name"),
		Desc:    nulls.NewString("Description"),
		Link:    nulls.NewString("Link"),
		OwnerId: u.ID,
	}
	models.DB.Save(s)

	link := &models.SiteWorkerLink{
		SiteId:     s.ID,
		StringLink: nulls.NewString("ABCDEFGH"),
	}

	err := models.DB.Save(link)
	ms.NoError(err)
}

func (ms *ModelSuite) Test_SiteWorkerLink_Create() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:    nulls.NewString("Name"),
		Desc:    nulls.NewString("Description"),
		Link:    nulls.NewString("Link"),
		OwnerId: u.ID,
	}
	models.DB.Save(s)

	link := &models.SiteWorkerLink{
		SiteId:     s.ID,
		StringLink: nulls.NewString("ABCDEFGH"),
	}

	_, err := link.Create(models.DB)
	ms.NoError(err)
}

func (ms *ModelSuite) Test_SiteWorkerLink_GetLinkByString() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:    nulls.NewString("Name"),
		Desc:    nulls.NewString("Description"),
		Link:    nulls.NewString("Link"),
		OwnerId: u.ID,
	}
	models.DB.Save(s)

	link := &models.SiteWorkerLink{
		SiteId:     s.ID,
		StringLink: nulls.NewString("ABCD1234"),
	}
	models.DB.Save(link)

	var links models.SiteWorkerLinks
	ms.NoError(links.GetLinkByString("ABCD1234"), "Хвърля грешка при викане не models.SiteWorkerLinks.GetLinkByString()")

	ms.Equal(1, len(links), "Върнало е твърде много линкове")

	for _, link := range links {
		ms.Equal(nulls.NewString("ABCD1234"), link.StringLink, "Връща грешен линк")
	}
}

func (ms *ModelSuite) TestSite_RemoveUser() {

	s := &models.Site{
		Name: nulls.NewString("Ivan"),
		Users: models.Users{
			models.User{ID: uuid.UUID{1}},
			models.User{ID: uuid.UUID{2}},
			models.User{ID: uuid.UUID{3}},
			models.User{ID: uuid.UUID{4}},
		},
	}

	s.RemoveUser(models.User{ID: uuid.UUID{3}})

	ms.Equal(3, len(s.Users), fmt.Sprintf("Грешен брой потребители в масива на сайта - %d", len(s.Users)))

	for _, u := range s.Users {
		id3 := uuid.UUID{3}
		ms.NotEqual(id3, u.ID, "Не е махнат правилният потребител")
	}

}

func (ms *ModelSuite) TestSite_GetByRights() {
	u1 := &models.User{
		Name:  nulls.NewString("Ivan1"),
		Email: nulls.NewString("ivan1@example.com"),
	}

	u2 := &models.User{
		Name:  nulls.NewString("Ivan2"),
		Email: nulls.NewString("ivan2@example.com"),
	}

	u3 := &models.User{
		Name:  nulls.NewString("Ivan3"),
		Email: nulls.NewString("ivan3@example.com"),
	}

	s1 := &models.Site{
		Name:  nulls.NewString("site1"),
		Desc:  nulls.NewString("desc1"),
		Link:  nulls.NewString("https://link1.bg"),
		Owner: *u1,
	}

	s2 := &models.Site{
		Name:  nulls.NewString("site2"),
		Desc:  nulls.NewString("desc2"),
		Link:  nulls.NewString("https://link2.bg"),
		Owner: *u1,
	}

	s3 := &models.Site{
		Name:  nulls.NewString("site3"),
		Desc:  nulls.NewString("desc3"),
		Link:  nulls.NewString("https://link3.bg"),
		Owner: *u2,
	}

	models.DB.Save(u1)
	models.DB.Save(u2)
	models.DB.Save(u3)
	models.DB.Save(s1)
	models.DB.Save(s2)
	models.DB.Save(s3)

	su1 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u1.ID,
		Rights: "owner",
	}

	su2 := &models.SitesUsers{
		SiteId: s2.ID,
		UserId: u1.ID,
		Rights: "owner",
	}

	su3 := &models.SitesUsers{
		SiteId: s3.ID,
		UserId: u1.ID,
		Rights: "rw",
	}

	su4 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u2.ID,
		Rights: "admin",
	}

	su5 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u3.ID,
		Rights: "admin",
	}

	models.DB.Save(su1)
	models.DB.Save(su2)
	models.DB.Save(su3)
	models.DB.Save(su4)
	models.DB.Save(su5)

	var sites models.SitesUsersMulti
	ms.NoError(sites.GetByRights(models.DB, u1.ID, "owner"), "Хвърля грешка при викане на models.SitesUsersMulti.GetByRights()")

	
	ms.NotEqual(1, len(sites))


	for _, site := range sites {
		ms.Equal(site.UserId, u1.ID,"Връща резултат за грешен потребител" )
		ms.Equal("owner",site.Rights)
	}
}

func (ms *ModelSuite) Test_Site_DeleteSite() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	s := &models.Site{
		Name:  nulls.NewString("Name"),
		Desc:  nulls.NewString("Description"),
		Link:  nulls.NewString("Link"),
		Owner: *u,
	}

	models.DB.Save(s)

	site_id := s.ID

	ms.NoError(models.DB.Destroy(s), "Хвърля грешка при викане")

	ms.Error(models.DB.Find(s, site_id), "Записът не е изтрит")
}

func (ms *ModelSuite) TestSite_GetBySite() {
	u1 := &models.User{
		Name:  nulls.NewString("Ivan1"),
		Email: nulls.NewString("ivan1@example.com"),
	}

	u2 := &models.User{
		Name:  nulls.NewString("Ivan2"),
		Email: nulls.NewString("ivan2@example.com"),
	}

	u3 := &models.User{
		Name:  nulls.NewString("Ivan3"),
		Email: nulls.NewString("ivan3@example.com"),
	}

	s1 := &models.Site{
		Name:  nulls.NewString("site1"),
		Desc:  nulls.NewString("desc1"),
		Link:  nulls.NewString("https://link1.bg"),
		Owner: *u1,
	}

	s2 := &models.Site{
		Name:  nulls.NewString("site2"),
		Desc:  nulls.NewString("desc2"),
		Link:  nulls.NewString("https://link2.bg"),
		Owner: *u1,
	}

	s3 := &models.Site{
		Name:  nulls.NewString("site3"),
		Desc:  nulls.NewString("desc3"),
		Link:  nulls.NewString("https://link3.bg"),
		Owner: *u2,
	}

	models.DB.Save(u1)
	models.DB.Save(u2)
	models.DB.Save(u3)
	models.DB.Save(s1)
	models.DB.Save(s2)
	models.DB.Save(s3)

	su1 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u1.ID,
		Rights: "owner",
	}

	su2 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u2.ID,
		Rights: "admin",
	}

	su3 := &models.SitesUsers{
		SiteId: s1.ID,
		UserId: u3.ID,
		Rights: "admin",
	}

	su4 := &models.SitesUsers{
		SiteId: s2.ID,
		UserId: u1.ID,
		Rights: "owner",
	}

	su5 := &models.SitesUsers{
		SiteId: s3.ID,
		UserId: u2.ID,
		Rights: "admin",
	}

	models.DB.Save(su1)
	models.DB.Save(su2)
	models.DB.Save(su3)
	models.DB.Save(su4)
	models.DB.Save(su5)

	var sites models.SitesUsersMulti
	ms.NoError(sites.GetBySite(s1.ID, "admin"), "Хвърля грешка при викане на models.SitesUsersMulti.GetBySite()")

	ms.False(len(sites) > 2, "Върнало е твърде много юзъри")

	ms.False(len(sites) > 2, "Върнало е твърде много юзъри")

	for _, site := range sites {
		ms.Equal(s1.ID, site.SiteId, "Връща резултат за грешен сайт")
		ms.Equal("admin", site.Rights, "Връща резултат с грешни права")
	}
}

func (ms *ModelSuite) Test_Sites_Users_DeleteUsersWithSite() {
	u := &models.User{
		Name:  nulls.NewString("Ivan"),
		Email: nulls.NewString("ivan@example.com"),
	}
	models.DB.Save(u)

	u1 := &models.User{
		Name:  nulls.NewString("Ivan1"),
		Email: nulls.NewString("ivan1@example.com"),
	}
	models.DB.Save(u1)

	s := &models.Site{
		Name:  nulls.NewString("Name"),
		Desc:  nulls.NewString("Description"),
		Link:  nulls.NewString("Link"),
		Owner: *u,
	}

	models.DB.Save(s)

	su := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u.ID,
		Rights: "owner",
	}

	su1 := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u1.ID,
		Rights: "read",
	}

	su_id := su.ID
	su1_id := su1.ID

	models.DB.Save(su)
	models.DB.Save(su1)

	var sites models.SitesUsersMulti
	ms.NoError(sites.DeleteUsersWithSite(s.ID, models.DB), "Хвърля грешка при викане на models.SitesUsersMulti.DeleteUsersWithSite()")

	ms.Error(models.DB.Find(su, su_id), "Запис не е изтрит")

	ms.Error(models.DB.Find(su1, su1_id), "Запис не е изтрит")
}

func (ms *ModelSuite) Test_SitesUsers_GetRights() {
	u := &models.User{
		Name:  nulls.NewString("Ivan1"),
		Email: nulls.NewString("ivan1@example.com"),
	}

	s := &models.Site{
		Name:  nulls.NewString("site3"),
		Desc:  nulls.NewString("desc3"),
		Link:  nulls.NewString("https://link3.bg"),
		Owner: *u,
	}

	models.DB.Save(u)
	models.DB.Save(s)

	su := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u.ID,
		Rights: "owner",
	}

	models.DB.Save(su)

	var susu models.SitesUsers
	ms.NoError(susu.GetRights(u.ID, s.ID), "Хвърля грешка при викане на models.SitesUsers.GetRights()")

	ms.Equal("owner", susu.Rights, "Връща запис с грешни права")
}

func (ms *ModelSuite) Test_SitesUsers_RemoveUserFromSite() {
	u := &models.User{
		Name:  nulls.NewString("Ivan1"),
		Email: nulls.NewString("ivan1@example.com"),
	}

	s := &models.Site{
		Name:  nulls.NewString("site3"),
		Desc:  nulls.NewString("desc3"),
		Link:  nulls.NewString("https://link3.bg"),
		Owner: *u,
	}

	models.DB.Save(u)
	models.DB.Save(s)

	su := &models.SitesUsers{
		SiteId: s.ID,
		UserId: u.ID,
		Rights: "owner",
	}

	models.DB.Save(su)

	ms.NoError(models.DB.Destroy(su), "Хвърля грешка при викане")

	var ss models.SitesUsers
	ms.Error(ss.GetRights(u.ID, s.ID), "Не е изтрит потребителят")

}
